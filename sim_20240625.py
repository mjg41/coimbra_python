# Template for doing multiple simulation runs

from hpsignal_complete_binding_method_of_lines import *

# v_sup_values=[1.00000000e-01, 1.58489319e-01, 2.51188643e-01, 3.98107171e-01,
#               6.30957344e-01, 1.00000000e+00, 1.58489319e+00, 2.51188643e+00,
#               3.98107171e+00, 6.30957344e+00, 1.00000000e+01, 1.58489319e+01,
#               2.51188643e+01, 3.98107171e+01, 6.30957344e+01, 1.00000000e+02]
v_sup_values=[1.00000000e+00, 1.58489319e+00, 2.51188643e+00,
              3.98107171e+00, 6.30957344e+00, 1.00000000e+01, 1.58489319e+01,
              2.51188643e+01, 3.98107171e+01, 6.30957344e+01, 1.00000000e+02]
k_aMC_values = [1.0, 10.0]
K_DTSH_values = [0.01, 0.018, 0.032, 0.056, 0.1, 0.178, 0.316,
                 0.562, 1.0, 1.778, 3.162, 5.623, 10.0]
t_end_values = [0.5,1.,5.,10.,15.,20.,25.,30.,35.,40.,45.,50.,55.,60.,70.,80.,
                90.,100.,110.,120.,150.,180.,240.,300.,3600.,21600.,43200.,86400.]

# Loop over parameters

for v_sup in v_sup_values:
    for k_aMC in k_aMC_values:
        for K_DTSH in K_DTSH_values:
            for t_end in t_end_values:

                parameters = Parameters(v_sup = v_sup, k_aMC = k_aMC, k_aS = 0.0,
                                        K_DTSH = K_DTSH, K_DTSS = 10000.0, c_Pb_init_value = 0.001)
                modelsetup = ModelSetup(parameters)
                modelsetup.ic = '(v_sup={:.4f}_k_aMC={:.2f}_K_DTSH={:.4f})'.format(parameters.v_sup, parameters.k_aMC, parameters.K_DTSH)

                _,_ = hpscbml(parameters = parameters, modelsetup = modelsetup, t_end= t_end, saving=True, output_dir = 'output/20240625_K_DTSH_investigation')
