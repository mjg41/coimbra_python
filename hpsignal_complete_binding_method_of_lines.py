# Solve numerically the complete system which builds on the equations 
# described in Eqns (1) & (2) of Griffith et al. (2024).
# The PDEs are discretised spatially and the resulting ODE
# system is solved using the built-in LSODA method in scipy.

import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import diags
from scipy.integrate import simpson, LSODA
from timeit import default_timer as timer
import sys
import os, errno

###############################################################################

# Units of distance are dm throughout

###############################################################################

#############################    SETUP CLASSES    #############################

# Create class to do dynamic printing on one line

class Printer():
    """Print things to stdout on one line dynamically"""
    def __init__(self,data):
        sys.stdout.write("\r\x1b[K"+data.__str__())
        sys.stdout.flush()

# Create class to store various quantities of interest for simulation analysis
# These can then be viewed easily using vars(class)

class Analysis(object):
    '''Class to store various quantities of interest for simulation analysis'''
    def __init__(self,
                 TSS_gen_domain = 0.0,
                 TSS_gen_boundary = 0.0,
                 TSS_to_centre_frac = 0.2,
                 TSS_to_centre_frac_time = 0.0,
                 TSS_mean = 0.0):
        self.TSS_gen_domain = TSS_gen_domain
        self.TSS_gen_boundary = TSS_gen_boundary
        self.TSS_to_centre_frac = TSS_to_centre_frac
        self.TSS_to_centre_frac_time = TSS_to_centre_frac_time
        self.TSS_mean = TSS_mean
    
    def reset(self):
        self.TSS_gen_domain = 0.0
        self.TSS_gen_boundary = 0.0
        self.TSS_to_centre_frac_time = 0.0
        self.TSS_mean = 0.0

# Set up class containing parameters
# These can then be viewed easily using vars(class)

class Parameters:                
    '''Class containing simulation parameters'''
    def __init__(self,
                 v_sup = 15.6,
                 k_Alt = 100.0,
                 k_aM = 0.0,        # possibly: 0.1,1,10,100
                 k_aMP = 0.01,       # possibly: 0.001, 0.01, 0.1, 1, 10
                 k_aMC = 1.0,       # possibly: 0.001, 0.01, 0.1, 1, 10
                 k_aB = 0.0,
                 k_aBP = 0.01,
                 k_aBC = 1.0,
                 K_DB = 0.03,
                 k_aS = 0.00774264,
                 k_C_P2 = 0.5,
                 k_C_P1 = 11.0,
                 k_I = 15.8,
                 K_DTSH = 30.0,
                 K_DTSS = 30.0,
                 k_HC = 0.21,
                 k_R_P2 = 0.61,
                 k_R_P1 = 2.2,
                 k_Se = 100.0,
                 k_Si_P2 = 0.0034,
                 k_Si_P1 = 0.0015,
                 k_Sr = 0.001,
                 k_TDE = 1.0,       # possibly: 0.01,0.1,1,10,100
                 K_TDE = 1.0,
                 k_ITDE = 0.0,
                 K_ITDE = 1.0,
                 k_TR = 100.0,
                 k_TSSR = 0.0225,
                 k_PbOx = 2.5,
                 k_PbR = 2.9e-4,
                 k_MPbOx = 2.5,
                 k_MPbR = 2.9e-4,
                 r_DM = 200.0,
                 V_MaxTrxR = 180.0,
                 K_MTrxR = 1.8,
                 D_1 = 3.7e-7,
                 D_2 = 5.2e-11,
                 D_3 = 9e-11,
                 D_4 = 4.6e-11,
                 D_5 = 3.5e-9,
                 D_6 = 7.9e-11,
                 D_Pb = 2.7e-10,
                 D_B = 4.2e-10,
                 D_B_P = 4.9e-11,
                 D_B_PSS = 7.3e-11,
                 D_B_P_T = 4.5e-11,
                 D_B_PSS_T = 7.2e-11,
                 c_P1_init_value = 110.0,
                 c_P2_init_value = 30.0,
                 c_T_init_value = 0.05,
                 c_Trx_init_value = 20.0,
                 c_Pb_init_value = 5.0,
                 c_B_init_value = 0.15,
                 dx = 1e-6,         # This gives 51 points in total
                 length = 5e-5):
                 
        self.v_sup = v_sup
        self.k_Alt = k_Alt
        self.k_aM = k_aM
        self.k_aMP = k_aMP
        self.k_aMC = k_aMC
        self.k_aB = k_aB
        self.k_aBP = k_aBP
        self.k_aBC = k_aBC
        self.K_DB = K_DB
        self.k_aS = k_aS
        self.k_C_P2 = k_C_P2
        self.k_C_P1 = k_C_P1
        self.k_I = k_I
        self.K_DTSH = K_DTSH
        self.K_DTSS = K_DTSS
        self.k_HC = k_HC
        self.k_R_P2 = k_R_P2
        self.k_R_P1 = k_R_P1
        self.k_Se = k_Se
        self.k_Si_P2 = k_Si_P2
        self.k_Si_P1 = k_Si_P1
        self.k_Sr = k_Sr
        self.k_TDE = k_TDE
        self.K_TDE = K_TDE
        self.k_ITDE = k_ITDE
        self.K_ITDE = K_ITDE
        self.k_TR = k_TR
        self.k_TSSR = k_TSSR
        self.k_PbOx = k_PbOx
        self.k_PbR = k_PbR
        self.k_MPbOx = k_MPbOx
        self.k_MPbR = k_MPbR
        self.r_DM = r_DM

        self.V_MaxTrxR = V_MaxTrxR
        self.K_MTrxR = K_MTrxR

        self.D_1 = D_1
        self.D_2 = D_2
        self.D_3 = D_3
        self.D_4 = D_4
        self.D_5 = D_5
        self.D_6 = D_6
        self.D_Pb = D_Pb
        self.D_B = D_B
        self.D_B_P = D_B_P
        self.D_B_PSS = D_B_PSS
        self.D_B_P_T = D_B_P_T
        self.D_B_PSS_T = D_B_PSS_T

        self.dx = dx
        self.length = length
        self.r_VS = self.length

        self.c_P1_init_value = c_P1_init_value
        self.c_P2_init_value = c_P2_init_value
        self.c_T_init_value = c_T_init_value
        self.c_Trx_init_value = c_Trx_init_value
        self.c_Pb_init_value = c_Pb_init_value
        self.c_B_init_value = c_B_init_value

    def __str__(self):

        out = ('Length units are dm throughout\n' +
               '----------------------------------\n' +
               'v_sup             = {:.5f}\n'.format(self.v_sup) +
               'k_Alt             = {:.5f}\n'.format(self.k_Alt) +
               'k_aM              = {:.5f}\n'.format(self.k_aM) +
               'k_aMP             = {:.5f}\n'.format(self.k_aMP) +
               'k_aMC             = {:.5f}\n'.format(self.k_aMC) +
               'k_aB              = {:.5f}\n'.format(self.k_aB) +
               'k_aBP             = {:.5f}\n'.format(self.k_aBP) +
               'k_aBC             = {:.5f}\n'.format(self.k_aBC) +
               'K_DB              = {:.5f}\n'.format(self.K_DB) +
               'k_aS              = {:.6f}\n'.format(self.k_aS) +
               'k_C_P2            = {:.5f}\n'.format(self.k_C_P2) +
               'k_C_P1            = {:.5f}\n'.format(self.k_C_P1) +
               'k_I               = {:.5f}\n'.format(self.k_I) +
               'K_DTSH            = {:.5f}\n'.format(self.K_DTSH) +
               'K_DTSS            = {:.5f}\n'.format(self.K_DTSS) +
               'k_HC              = {:.5f}\n'.format(self.k_HC) +
               'k_R_P2            = {:.5f}\n'.format(self.k_R_P2) +
               'k_R_P1            = {:.5f}\n'.format(self.k_R_P1) +
               'k_Se              = {:.5f}\n'.format(self.k_Se) +
               'k_Si_P2           = {:.5f}\n'.format(self.k_Si_P2) +
               'k_Si_P1           = {:.5f}\n'.format(self.k_Si_P1) +
               'k_Sr              = {:.5f}\n'.format(self.k_Sr) +
               'k_TDE             = {:.5f}\n'.format(self.k_TDE) +
               'K_TDE             = {:.5f}\n'.format(self.K_TDE) +
               'k_ITDE            = {:.5f}\n'.format(self.k_ITDE) +
               'K_ITDE            = {:.5f}\n'.format(self.K_ITDE) +
               'k_TR              = {:.5f}\n'.format(self.k_TR) +
               'k_TSSR            = {:.5f}\n'.format(self.k_TSSR) +
               'k_PbOx            = {:.5f}\n'.format(self.k_PbOx) +
               'k_PbR             = {:.5f}\n'.format(self.k_PbR) +
               'k_MPbOx           = {:.5f}\n'.format(self.k_MPbOx) +
               'k_MPbR            = {:.5f}\n'.format(self.k_MPbR) +
               'r_DM              = {:.5f}\n'.format(self.r_DM) +
               'V_MaxTrxR         = {:.5f}\n'.format(self.V_MaxTrxR) +
               'K_MTrxR           = {:.5f}\n'.format(self.K_MTrxR) +
               'r_VS              = {:.5e}\n'.format(self.r_VS) +
               'D_1               = {:.5e}\n'.format(self.D_1) +
               'D_2               = {:.5e}\n'.format(self.D_2) +
               'D_3               = {:.5e}\n'.format(self.D_3) +
               'D_4               = {:.5e}\n'.format(self.D_4) +
               'D_5               = {:.5e}\n'.format(self.D_5) +
               'D_6               = {:.5e}\n'.format(self.D_6) +
               'D_Pb              = {:.5e}\n'.format(self.D_Pb) +
               'D_B               = {:.5e}\n'.format(self.D_B) +
               'D_B_P             = {:.5e}\n'.format(self.D_B_P) +
               'D_B_PSS           = {:.5e}\n'.format(self.D_B_PSS) +
               'D_B_P_T           = {:.5e}\n'.format(self.D_B_P_T) +
               'D_B_PSS_T         = {:.5e}\n'.format(self.D_B_PSS_T) +
               'Domain Spacing    = {:.5e}\n'.format(self.dx) +
               'Domain Length     = {:.5e}\n'.format(self.length) +
               'Initial P1 total  = {:.5f}\n'.format(self.c_P1_init_value) +
               'Initial P2 total  = {:.5f}\n'.format(self.c_P2_init_value) +
               'Initial T total   = {:.5f}\n'.format(self.c_T_init_value) +
               'Initial Trx total = {:.5f}\n'.format(self.c_Trx_init_value) +
               'Initial Pb total  = {:.5f}\n'.format(self.c_Pb_init_value) + 
               'Initial B total   = {:.5f}'.format(self.c_B_init_value))
        
        return out

# Set up class containing computed quantities related to model setup using values of
# parameters from Parameters class
# These can then be viewed easily using vars(class)

class ModelSetup(Parameters):
    '''Class containing computed quantities related to model setup using values of parameters from Parameters class'''
    def __init__(self,
                 parameters,
                 n_bulk_species = 39,
                 n_memb_species = 15):

        self.Nx = int(parameters.length/parameters.dx) + 1
        self.x = np.linspace(0, parameters.length, self.Nx)
        self.n_bulk_species = n_bulk_species
        self.n_memb_species = n_memb_species
        self.c_bulk_init, self.c_m_init = self.build_init_conds(parameters)
        # self.ic = '(v_sup=65exp-5e-3t_k_aM={:.1f}_k_aMC={:.2f}_k_TDE={:.2f})'.format(parameters.k_aM, parameters.k_aMC, parameters.k_TDE)
        # self.ic = '(k_aM={:.1f}_k_aMC={:.2f}_k_TDE={:.2f})'.format(parameters.k_aM, parameters.k_aMC, parameters.k_TDE)
        # self.ic = '(k_aS={:.4f}_k_TSSR={:.5f})'.format(parameters.k_aS, parameters.k_TSSR)
        # self.ic = '(v_sup={:.4f}_k_aMC={:.3f}_K_DTSH={:.3f})'.format(parameters.v_sup, parameters.k_aMC, parameters.K_DTSH)
        self.ic = '(v_sup={:.4f})'.format(parameters.v_sup)

    def build_init_conds(self, parameters):

        # Initialise empty conc array

        # The 39 bulk concentrations are in the following order:
        #
        # c_H
        # c_P1SH
        # c_P1SOH
        # c_P1SO2H
        # c_P1SS
        # c_P1SHa
        # c_PSH
        # c_PSOH
        # c_PSO2H
        # c_PSS
        # c_PSST
        # c_TSH
        # c_TSS
        # c_TrxSH
        # c_TrxSS
        # c_PSH_TSH
        # c_PSH_TSS
        # c_PSOH_TSH
        # c_PSOH_TSS
        # c_PSO2H_TSH
        # c_PSO2H_TSS
        # c_PSS_TSH
        # c_PSS_TSS
        # c_PbSH
        # c_PbSS
        # c_B
        # c_B_PSH
        # c_B_PSH_TSH
        # c_B_PSH_TSS
        # c_B_PSO2H
        # c_B_PSO2H_TSH
        # c_B_PSO2H_TSS
        # c_B_PSOH
        # c_B_PSOH_TSH
        # c_B_PSOH_TSS
        # c_B_PSS
        # c_B_PSST
        # c_B_PSS_TSH
        # c_B_PSS_TSS
        #
        # The 15 membrane concentrations are in the following order:
        #
        # c_m_PSH
        # c_m_PSOH
        # c_m_PSO2H
        # c_m_PSS
        # c_m_PSST
        # c_m_PSH_TSH
        # c_m_PSH_TSS
        # c_m_PSOH_TSH
        # c_m_PSOH_TSS
        # c_m_PSO2H_TSH
        # c_m_PSO2H_TSS
        # c_m_PSS_TSH
        # c_m_PSS_TSS
        # c_m_PbSH
        # c_m_PbSS
        

        c_bulk_init = np.zeros((self.Nx,self.n_bulk_species))
        
        c_m_init = np.zeros(self.n_memb_species)

        c_bulk_init[:,1] = parameters.c_P1_init_value        # c_P1SH

        c_bulk_init[:,6] = parameters.c_P2_init_value         # c_PSH

        c_bulk_init[:,11] = parameters.c_T_init_value        # c_TSH

        c_bulk_init[:,13] = parameters.c_Trx_init_value      # c_TrxSH

        c_bulk_init[:,23] = parameters.c_Pb_init_value      # c_PbSH

        c_bulk_init[:,23] = parameters.c_Pb_init_value      # c_PbSH

        c_bulk_init[:,25] = parameters.c_B_init_value      # c_B

        #c_bulk_init[:,1] = 0.99*parameters.c_P1_init_value                                   # c_P1SH
        #c_bulk_init[:,2] = 0.005*parameters.c_P1_init_value                                  # c_P1SOH
        #c_bulk_init[:,4] = 0.005*parameters.c_P1_init_value                                  # c_P1SS

        #c_bulk_init[:,6] = (0.98 - 0.5*parameters.c_T_init_value)*parameters.c_P2_init_value  # c_PSH
        #c_bulk_init[:,6] = 0.98*parameters.c_P2_init_value                                    # c_PSH
        #c_bulk_init[:,7] = 0.01*parameters.c_P2_init_value                                    # c_PSOH
        #c_bulk_init[:,9] = 0.01*parameters.c_P2_init_value                                    # c_PSS

        #c_bulk_init[:,11] = 0.49*parameters.c_T_init_value                                   # c_TSH
        #c_bulk_init[:,15] = (0.49 + 0.5*parameters.c_P2_init_value)*parameters.c_T_init_value # c_PSH_TSH
        #c_bulk_init[:,12] = 0.01*parameters.c_T_init_value                                   # c_TSS
        #c_bulk_init[:,10] = 0.01*parameters.c_T_init_value                                    # c_PSST

        #c_bulk_init[:,13] = 0.99*parameters.c_Trx_init_value                                 # c_TrxSH
        #c_bulk_init[:,14] = 0.01*parameters.c_Trx_init_value                                 # c_TrxSS

        return c_bulk_init, c_m_init

###############################################################################

#############################  PLOTTING FUNCTION  #############################
## c_B values are not included in this plotting function ##

def plot_concs(c_bulk, c_m,
               parameters = Parameters(), modelsetup = ModelSetup(Parameters()),
               t_end=np.nan):
    
    '''Plot concentrations outputted by pde routine.
    
    Positional arguments:
    c_bulk are the bulk concentrations from the pde routine.
    c_m    are the membrane concentrations from the pde routine.
    
    Optional keyword arguments:
    parameters is the class containing all parameters to use for the simulation. Default as in init method.
    modelsetup is the class containing all computed quantities related to model setup. Default as in init method.
    t_end is the end time of the pde simulation. Default is NaN.
    '''
    
    # Initialise perox figure and axes (first 10 concs)
    
    fig_perox, axes_perox = plt.subplots(4,3,figsize=[10,11])

    # Initialise Target figure and axes (next 3, skip 2, then 8 after)
    
    fig_T, axes_T = plt.subplots(4,3,figsize=[10,11])

    # Initialise Trx figure and axes (skipped 2)
    
    fig_Trx, axes_Trx = plt.subplots(1,2,figsize=[10,4])

    # Initialise Probe figure and axes (next 2)
    
    fig_Pb, axes_Pb = plt.subplots(1,2,figsize=[10,4])

    # Initialise Bulk scaffold figure and axes (final 14)

    fig_B, axes_B = plt.subplots(5,3,figsize=[10,14])

    # Loop over figures doing formatting

    for fig in [fig_perox, fig_T, fig_Trx, fig_Pb, fig_B]:

        # Nicely space figures leaving room for title
    
        fig.tight_layout(pad=3.75, h_pad=None, w_pad=None)
    
        # Plot title
            
        fig.suptitle('v_sup = ' +
                     str(parameters.v_sup) + '   Model at ' + 
                     str('{:.2f}'.format(t_end)) + 's.')
    
    # List of subplot titles
    
    title_list = ['$c_{\\mathrm{H}}$',
                  '$c_{\\mathrm{P1SH}}$',
                  '$c_{\\mathrm{P1SOH}}$',
                  '$c_{\\mathrm{P1SO2H}}$',
                  '$c_{\\mathrm{P1SS}}$',
                  '$c_{\\mathrm{P1SHa}}$',
                  '$c_{\\mathrm{PSH}}$',
                  '$c_{\\mathrm{PSOH}}$',
                  '$c_{\\mathrm{PSO2H}}$',
                  '$c_{\\mathrm{PSS}}$',
                  '$c_{\\mathrm{PSST}}$',
                  '$c_{\\mathrm{TSH}}$',
                  '$c_{\\mathrm{TSS}}$',
                  '$c_{\\mathrm{TrxSH}}$',
                  '$c_{\\mathrm{TrxSS}}$',
                  '$c_{\\mathrm{PSH\\$TSH}}$',
                  '$c_{\\mathrm{PSH\\$TSS}}$',
                  '$c_{\\mathrm{PSOH\\$TSH}}$',
                  '$c_{\\mathrm{PSOH\\$TSS}}$',
                  '$c_{\\mathrm{PSO2H\\$TSH}}$',
                  '$c_{\\mathrm{PSO2H\\$TSS}}$',
                  '$c_{\\mathrm{PSS\\$TSH}}$',
                  '$c_{\\mathrm{PSS\\$TSS}}$',
                  '$c_{\\mathrm{PbSH}}$',
                  '$c_{\\mathrm{PbSS}}$',
                  '$c_{\\mathrm{B}}$',
                  '$c_{\\mathrm{B\\$PSH}}$',
                  '$c_{\\mathrm{B\\$PSH\\$TSH}}$',
                  '$c_{\\mathrm{B\\$PSH\\$TSS}}$',
                  '$c_{\\mathrm{B\\$PSO2H}}$',
                  '$c_{\\mathrm{B\\$PSO2H\\$TSH}}$',
                  '$c_{\\mathrm{B\\$PSO2H\\$TSS}}$',
                  '$c_{\\mathrm{B\\$PSOH}}$',
                  '$c_{\\mathrm{B\\$PSOH\\$TSH}}$',
                  '$c_{\\mathrm{B\\$PSOH\\$TSS}}$',
                  '$c_{\\mathrm{B\\$PSS}}$',
                  '$c_{\\mathrm{B\\$PSST}}$',
                  '$c_{\\mathrm{B\\$PSS\\$TSH}}$',
                  '$c_{\\mathrm{B\\$PSS\\$TSS}}$'
                  ]
    
    # Put axes into list in correct order. First 10 concs are perox
    # then 3 Target concs, then 2 Trx concs, then 8 remaining target concs,
    # then 2 Pb concs, then 14 B concs.

    axes = np.concatenate((axes_perox.ravel()[:10],
                           axes_T.ravel()[:3],
                           axes_Trx.ravel(),
                           axes_T.ravel()[3:11],
                           axes_Pb.ravel(),
                           axes_B.ravel()[:15]))

    # Plot bulk concentrations
    
    for conc_init, conc, ax, title in zip(modelsetup.c_bulk_init.T,
                                          c_bulk.T,
                                          axes,
                                          title_list):
        
        ax.plot(modelsetup.x,conc_init, label = 'Initial Concentration')
        ax.plot(modelsetup.x,conc, label = 'Final Concentration')
        ax.set_title(title)

    # Get last axis legend info as it is the same for all

    handles, labels = ax.get_legend_handles_labels()

    # Loop over figures adding legends

    for fig in [fig_perox, fig_T, fig_Trx, fig_Pb, fig_B]:

        fig.legend(handles, labels, loc='lower center')
    
    # Plot membrane concentrations using text

    # Initialise figure and axes
    
    fig_m, axes_m = plt.subplots(1,2)

    # Nicely space figures leaving room for title
    
    fig_m.tight_layout(pad=3.25, h_pad=None, w_pad=None)

    # Remove axis ticks and labels from axes
    
    for ax in axes_m.ravel():
        ax.axis('off')
    
    # List of membrane concentrations
    
    membrane_conc_list = ['$c_{\\mathrm{PSH}}^{m} = $',
                          '$c_{\\mathrm{PSOH}}^{m} = $',
                          '$c_{\\mathrm{PSO2H}}^{m} = $',
                          '$c_{\\mathrm{PSS}}^{m} = $',
                          '$c_{\\mathrm{PSST}}^{m} = $',
                          '$c_{\\mathrm{PSH\\$TSH}}^{m} = $',
                          '$c_{\\mathrm{PSH\\$TSS}}^{m} = $',
                          '$c_{\\mathrm{PSOH\\$TSH}}^{m} = $',
                          '$c_{\\mathrm{PSOH\\$TSS}}^{m} = $',
                          '$c_{\\mathrm{PSO2H\\$TSH}}^{m} = $',
                          '$c_{\\mathrm{PSO2H\\$TSS}}^{m} = $',
                          '$c_{\\mathrm{PSS\\$TSH}}^{m} = $',
                          '$c_{\\mathrm{PSS\\$TSS}}^{m} = $',
                          '$c_{\\mathrm{PbSH}}^{m} = $',
                          '$c_{\\mathrm{PbSS}}^{m} = $'
                          ]

    # Define y coordinate for text position in figure
    # First figure will have 4 Prx membrane concs
    # Second figure will have 9 Target membrane concs
    # Then we add to first figure with 2 Pb membrane concs
    # Then stick them all together

    first_fig_y_coords = np.arange(1.0, 0.69,step=-0.1) # 4 points

    second_fig_y_coords = np.arange(1.0, 0.19,step=-0.1) # 9 points

    first_fig_Pb_y_coords = np.arange(0.5, 0.39,step=-0.1) # 2 points

    y_coords = np.concatenate((first_fig_y_coords, second_fig_y_coords, first_fig_Pb_y_coords))

    # Loop over membrane concs, writing them at y_coord
        
    for ii, [text, y_coord, conc] in enumerate(zip(membrane_conc_list, y_coords, c_m)):

        # For first 4 concs and last 2 concs plot in first axis

        if ii in [0, 1, 2, 3, 13, 14]:

            axes_m.ravel()[0].text(0.5, y_coord, text + 
                                   str('{:.4e}'.format(conc)),
                                   horizontalalignment='center'
                                   )
        
        # For other 9 concs plot in second axis

        else:

            axes_m.ravel()[1].text(0.5, y_coord, text + 
                                   str('{:.4e}'.format(conc)),
                                   horizontalalignment='center'
                                   )

    # Show plots
    
    plt.show()

###############################################################################

############################# SIMULATION FUNCTION #############################


def hpscbml(parameters = Parameters(), modelsetup = ModelSetup(Parameters()),
            analysis = Analysis(), t_init = 0.0, t_end = 86400.0,
            max_step = np.inf, saving = False, output_dir = 'output/test'):
    '''Solve numerically the complete system which builds on the
    equations described in Eqns (3) of Travasso et al. (2017). The PDEs
    are discretised spatially and the resulting ODE system is solved
    using the built-in LSODA method in scipy.
    
    Optional keyword arguments:
    parameters is the class containing all parameters to use for the simulation. Default as in init method.
    modelsetup is the class containing all computed quantities related to model setup. Default as in init method.
    analysis   is the class containing all computed quantities for analysing the model. Default as in init method.
    t_init     is the start time of the simulation. Default is 0.0.
    t_end      is the end time of the simulation. Default is 86400.0 (24 hrs).
    max_step   is the maximum time step to be used in the LSODA solver. Default is inf.
    saving     is the True/False switch to save output of run to csv file. Default is False.
    output_dir is the directory in which output will be saved. Default is 'output/test'
    '''
    ######################### Initial setup ##########################
    
    # Timer
    
    start = timer()
    
    # Calculate intermediate constants
    
    r_1 = parameters.D_1/parameters.dx**2
    r_2 = parameters.D_2/parameters.dx**2
    r_3 = parameters.D_3/parameters.dx**2
    r_4 = parameters.D_4/parameters.dx**2
    r_5 = parameters.D_5/parameters.dx**2
    r_6 = parameters.D_6/parameters.dx**2
    r_Pb = parameters.D_Pb/parameters.dx**2
    r_B = parameters.D_B/parameters.dx**2
    r_B_P = parameters.D_B_P/parameters.dx**2
    r_B_PSS = parameters.D_B_PSS/parameters.dx**2
    r_B_P_T = parameters.D_B_P_T/parameters.dx**2
    r_B_PSS_T = parameters.D_B_PSS_T/parameters.dx**2
    
    # Reset Analysis variables
    
    analysis.reset()
    
    # Initial condition
    
    # Reshape initial concentration array  to a vector by stacking all
    # columns (hence the transpose) and then appending membrane concs
    
    Y_0 = np.append(modelsetup.c_bulk_init.T.reshape(modelsetup.c_bulk_init.size), modelsetup.c_m_init)
    
    # Initial mass - integrate along each row to give a value for each
    # species and then sum all as well as membrane concs
    
    mass_init = (np.sum(simpson(modelsetup.c_bulk_init,x=modelsetup.x,axis=0)) + np.sum(modelsetup.c_m_init))  
                                                        
    
    ##################### Build Diffusion Operator #####################
    
    # Initialise diagonals
    
    L_diag = np.zeros(modelsetup.n_bulk_species*modelsetup.Nx + modelsetup.n_memb_species)
    
    L_updiag = np.zeros(modelsetup.n_bulk_species*modelsetup.Nx + modelsetup.n_memb_species - 1)
    
    L_lowdiag = np.zeros(modelsetup.n_bulk_species*modelsetup.Nx + modelsetup.n_memb_species - 1)

    # List of r_values

    r_values = [r_1,       # c_H
                r_2,       # c_P1SH
                r_2,       # c_P1SOH
                r_2,       # c_P1SO2H
                r_3,       # c_P1SS
                r_3,       # c_P1SHa
                r_2,       # c_PSH
                r_2,       # c_PSOH
                r_2,       # c_PSO2H
                r_3,       # c_PSS
                r_4,       # c_PSST
                r_3,       # c_TSH
                r_3,       # c_TSS
                r_5,       # c_TrxSH
                r_5,       # c_TrxSS
                r_4,       # c_PSH_TSH
                r_4,       # c_PSH_TSS
                r_4,       # c_PSOH_TSH
                r_4,       # c_PSOH_TSS
                r_4,       # c_PSO2H_TSH
                r_4,       # c_PSO2H_TSS
                r_6,       # c_PSS_TSH
                r_6,       # c_PSS_TSS
                r_Pb,      # c_PbSH
                r_Pb,      # c_PbSS
                r_B,       # c_B
                r_B_P,     # c_B_PSH
                r_B_P_T,   # c_B_PSH_TSH
                r_B_P_T,   # c_B_PSH_TSS
                r_B_P,     # c_B_PSO2H
                r_B_P_T,   # c_B_PSO2H_TSH
                r_B_P_T,   # c_B_PSO2H_TSS
                r_B_P,     # c_B_PSOH
                r_B_P_T,   # c_B_PSOH_TSH
                r_B_P_T,   # c_B_PSOH_TSS
                r_B_PSS,   # c_B_PSS
                r_B_P_T, # c_B_PSST
                r_B_PSS_T, # c_B_PSS_TSH
                r_B_PSS_T  # c_B_PSS_TSS
                ]
    
    # Fill in diagonals

    for ii, r_value in enumerate(r_values):

        L_diag[ii*modelsetup.Nx:(ii + 1)*modelsetup.Nx] = -2*r_value*np.ones(modelsetup.Nx)

        L_updiag[ii*modelsetup.Nx:((ii + 1)*modelsetup.Nx - 1)] = r_value*np.append(2, np.ones(modelsetup.Nx - 2))

        L_lowdiag[ii*modelsetup.Nx:((ii + 1)*modelsetup.Nx - 1)] = r_value*np.append(np.ones(modelsetup.Nx - 2), 2)
    
    # Construct matrix
    
    L = diags([L_lowdiag, L_diag, L_updiag],[-1,0,1])
    
    ########################## Build ODE RHS ###########################

    def f(t, Y):
        
        # Extract variables from input
        
        c_H = Y[:modelsetup.Nx]
        c_P1SH = Y[modelsetup.Nx:2*modelsetup.Nx]
        c_P1SOH = Y[2*modelsetup.Nx:3*modelsetup.Nx]
        c_P1SO2H = Y[3*modelsetup.Nx:4*modelsetup.Nx]
        c_P1SS = Y[4*modelsetup.Nx:5*modelsetup.Nx]
        c_P1SHa = Y[5*modelsetup.Nx:6*modelsetup.Nx]
        c_PSH = Y[6*modelsetup.Nx:7*modelsetup.Nx]
        c_PSOH = Y[7*modelsetup.Nx:8*modelsetup.Nx]
        c_PSO2H = Y[8*modelsetup.Nx:9*modelsetup.Nx]
        c_PSS = Y[9*modelsetup.Nx:10*modelsetup.Nx]
        c_PSST = Y[10*modelsetup.Nx:11*modelsetup.Nx]
        c_TSH = Y[11*modelsetup.Nx:12*modelsetup.Nx]
        c_TSS = Y[12*modelsetup.Nx:13*modelsetup.Nx]
        c_TrxSH = Y[13*modelsetup.Nx:14*modelsetup.Nx]
        c_TrxSS = Y[14*modelsetup.Nx:15*modelsetup.Nx]
        c_PSH_TSH = Y[15*modelsetup.Nx:16*modelsetup.Nx]
        c_PSH_TSS = Y[16*modelsetup.Nx:17*modelsetup.Nx]
        c_PSOH_TSH = Y[17*modelsetup.Nx:18*modelsetup.Nx]
        c_PSOH_TSS = Y[18*modelsetup.Nx:19*modelsetup.Nx]
        c_PSO2H_TSH = Y[19*modelsetup.Nx:20*modelsetup.Nx]
        c_PSO2H_TSS = Y[20*modelsetup.Nx:21*modelsetup.Nx]
        c_PSS_TSH = Y[21*modelsetup.Nx:22*modelsetup.Nx]
        c_PSS_TSS = Y[22*modelsetup.Nx:23*modelsetup.Nx]
        c_PbSH = Y[23*modelsetup.Nx:24*modelsetup.Nx]
        c_PbSS = Y[24*modelsetup.Nx:25*modelsetup.Nx]
        c_B = Y[25*modelsetup.Nx:26*modelsetup.Nx]
        c_B_PSH = Y[26*modelsetup.Nx:27*modelsetup.Nx]
        c_B_PSH_TSH = Y[27*modelsetup.Nx:28*modelsetup.Nx]
        c_B_PSH_TSS = Y[28*modelsetup.Nx:29*modelsetup.Nx]
        c_B_PSO2H = Y[29*modelsetup.Nx:30*modelsetup.Nx]
        c_B_PSO2H_TSH = Y[30*modelsetup.Nx:31*modelsetup.Nx]
        c_B_PSO2H_TSS = Y[31*modelsetup.Nx:32*modelsetup.Nx]
        c_B_PSOH = Y[32*modelsetup.Nx:33*modelsetup.Nx]
        c_B_PSOH_TSH = Y[33*modelsetup.Nx:34*modelsetup.Nx]
        c_B_PSOH_TSS = Y[34*modelsetup.Nx:35*modelsetup.Nx]
        c_B_PSS = Y[35*modelsetup.Nx:36*modelsetup.Nx]
        c_B_PSST = Y[36*modelsetup.Nx:37*modelsetup.Nx]
        c_B_PSS_TSH = Y[37*modelsetup.Nx:38*modelsetup.Nx]
        c_B_PSS_TSS = Y[38*modelsetup.Nx:39*modelsetup.Nx]
        
        c_m_PSH = Y[modelsetup.n_bulk_species*modelsetup.Nx]
        c_m_PSOH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 1]
        c_m_PSO2H = Y[modelsetup.n_bulk_species*modelsetup.Nx + 2]
        c_m_PSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 3]
        c_m_PSST = Y[modelsetup.n_bulk_species*modelsetup.Nx + 4]
        c_m_PSH_TSH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 5]
        c_m_PSH_TSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 6]
        c_m_PSOH_TSH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 7]
        c_m_PSOH_TSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 8]
        c_m_PSO2H_TSH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 9]
        c_m_PSO2H_TSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 10]
        c_m_PSS_TSH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 11]
        c_m_PSS_TSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 12]
        c_m_PbSH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 13]
        c_m_PbSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 14]

        # Extract parameters from input

        v_sup = parameters.v_sup
        k_Alt = parameters.k_Alt
        k_aM = parameters.k_aM
        k_aMP = parameters.k_aMP
        k_aMC = parameters.k_aMC
        k_aB = parameters.k_aB
        k_aBP = parameters.k_aBP
        k_aBC = parameters.k_aBC
        K_DB = parameters.K_DB
        k_aS = parameters.k_aS
        k_C_P2 = parameters.k_C_P2
        k_C_P1 = parameters.k_C_P1
        k_I = parameters.k_I
        K_DTSH = parameters.K_DTSH
        K_DTSS = parameters.K_DTSS
        k_HC = parameters.k_HC
        k_R_P2 = parameters.k_R_P2
        k_R_P1 = parameters.k_R_P1
        k_Se = parameters.k_Se
        k_Si_P2 = parameters.k_Si_P2
        k_Si_P1 = parameters.k_Si_P1
        k_Sr = parameters.k_Sr
        k_TDE = parameters.k_TDE
        K_TDE = parameters.K_TDE
        k_ITDE = parameters.k_ITDE
        K_ITDE = parameters.K_ITDE
        k_TR = parameters.k_TR
        k_TSSR = parameters.k_TSSR
        k_PbOx = parameters.k_PbOx
        k_PbR = parameters.k_PbR
        k_MPbOx = parameters.k_MPbOx
        k_MPbR = parameters.k_MPbR
        r_DM = parameters.r_DM
        V_MaxTrxR = parameters.V_MaxTrxR
        K_MTrxR = parameters.K_MTrxR
        r_VS = parameters.r_VS
        
        # Diffusion
        
        dYdt = L*Y
        
        # Reaction terms

        # c_H
        dYdt[:modelsetup.Nx] += -(c_H*k_Alt) - c_B_PSH*c_H*k_Se - c_B_PSH_TSH*c_H*k_Se - c_B_PSH_TSS*c_H*k_Se - c_B_PSOH*c_H*k_Si_P2 - c_B_PSOH_TSH*c_H*k_Si_P2 - c_B_PSOH_TSS*c_H*k_Si_P2 - c_H*k_Se*c_P1SH - c_H*k_Si_P1*c_P1SOH - c_H*k_PbOx*c_PbSH - c_H*k_Se*c_PSH - c_H*k_Se*c_PSH_TSH - c_H*k_Se*c_PSH_TSS - c_H*k_Si_P2*c_PSOH - c_H*k_Si_P2*c_PSOH_TSH - c_H*k_Si_P2*c_PSOH_TSS
        # c_P1SH
        dYdt[modelsetup.Nx:2*modelsetup.Nx] += -(c_H*k_Se*c_P1SH) + k_I*c_P1SHa
        # c_P1SOH
        dYdt[2*modelsetup.Nx:3*modelsetup.Nx] += c_H*k_Se*c_P1SH + k_Sr*c_P1SO2H - k_C_P1*c_P1SOH - c_H*k_Si_P1*c_P1SOH
        # c_P1SO2H
        dYdt[3*modelsetup.Nx:4*modelsetup.Nx] += -(k_Sr*c_P1SO2H) + c_H*k_Si_P1*c_P1SOH
        # c_P1SS
        dYdt[4*modelsetup.Nx:5*modelsetup.Nx] += k_C_P1*c_P1SOH - k_R_P1*c_P1SS*c_TrxSH
        # c_P1SHa
        dYdt[5*modelsetup.Nx:6*modelsetup.Nx] += -(k_I*c_P1SHa) + k_R_P1*c_P1SS*c_TrxSH
        # c_PSH
        dYdt[6*modelsetup.Nx:7*modelsetup.Nx] += c_B_PSH*k_aBP*K_DB - c_B*k_aBP*c_PSH - c_H*k_Se*c_PSH + k_aS*K_DTSH*c_PSH_TSH + k_aS*K_DTSS*c_PSH_TSS + k_R_P2*c_PSS*c_TrxSH - k_aS*c_PSH*c_TSH - k_aS*c_PSH*c_TSS
        # c_PSOH
        dYdt[7*modelsetup.Nx:8*modelsetup.Nx] += c_B_PSOH*k_aBP*K_DB + c_H*k_Se*c_PSH + k_Sr*c_PSO2H - c_B*k_aBP*c_PSOH - k_C_P2*c_PSOH - c_H*k_Si_P2*c_PSOH + k_aS*K_DTSH*c_PSOH_TSH + k_aS*K_DTSS*c_PSOH_TSS - k_aS*c_PSOH*c_TSH - k_aS*c_PSOH*c_TSS
        # c_PSO2H
        dYdt[8*modelsetup.Nx:9*modelsetup.Nx] += c_B_PSO2H*k_aBP*K_DB - c_B*k_aBP*c_PSO2H - k_Sr*c_PSO2H + k_aS*K_DTSH*c_PSO2H_TSH + k_aS*K_DTSS*c_PSO2H_TSS + c_H*k_Si_P2*c_PSOH - k_aS*c_PSO2H*c_TSH - k_aS*c_PSO2H*c_TSS
        # c_PSS
        dYdt[9*modelsetup.Nx:10*modelsetup.Nx] += c_B_PSS*k_aBP*K_DB + k_C_P2*c_PSOH - c_B*k_aBP*c_PSS + k_aS*K_DTSH*c_PSS_TSH + k_aS*K_DTSS*c_PSS_TSS - k_R_P2*c_PSS*c_TrxSH - k_aS*c_PSS*c_TSH - k_aS*c_PSS*c_TSS
        # c_PSST
        dYdt[10*modelsetup.Nx:11*modelsetup.Nx] += c_B_PSST*k_aB*K_DB + (k_TDE*c_PSH_TSS)/K_TDE + k_HC*c_PSOH_TSH - c_B*k_aB*c_PSST - (k_ITDE*c_PSST)/K_ITDE - k_TDE*c_PSST + k_ITDE*c_PSS_TSH
        # c_TSH
        dYdt[11*modelsetup.Nx:12*modelsetup.Nx] += c_B_PSH_TSH*k_aBC*K_DTSH + c_B_PSO2H_TSH*k_aBC*K_DTSH + c_B_PSOH_TSH*k_aBC*K_DTSH + c_B_PSS_TSH*k_aBC*K_DTSH + k_aS*K_DTSH*c_PSH_TSH + k_aS*K_DTSH*c_PSO2H_TSH + k_aS*K_DTSH*c_PSOH_TSH + k_aS*K_DTSH*c_PSS_TSH - c_B_PSH*k_aBC*c_TSH - c_B_PSO2H*k_aBC*c_TSH - c_B_PSOH*k_aBC*c_TSH - c_B_PSS*k_aBC*c_TSH - k_aS*c_PSH*c_TSH - k_aS*c_PSO2H*c_TSH - k_aS*c_PSOH*c_TSH - k_aS*c_PSS*c_TSH + k_TSSR*c_TSS
        # c_TSS
        dYdt[12*modelsetup.Nx:13*modelsetup.Nx] += c_B_PSH_TSS*k_aBC*K_DTSS + c_B_PSO2H_TSS*k_aBC*K_DTSS + c_B_PSOH_TSS*k_aBC*K_DTSS + c_B_PSS_TSS*k_aBC*K_DTSS + k_aS*K_DTSS*c_PSH_TSS + k_aS*K_DTSS*c_PSO2H_TSS + k_aS*K_DTSS*c_PSOH_TSS + k_aS*K_DTSS*c_PSS_TSS - c_B_PSH*k_aBC*c_TSS - c_B_PSO2H*k_aBC*c_TSS - c_B_PSOH*k_aBC*c_TSS - c_B_PSS*k_aBC*c_TSS - k_TSSR*c_TSS - k_aS*c_PSH*c_TSS - k_aS*c_PSO2H*c_TSS - k_aS*c_PSOH*c_TSS - k_aS*c_PSS*c_TSS
        # c_TrxSH
        dYdt[13*modelsetup.Nx:14*modelsetup.Nx] += -(c_B_PSS*k_R_P2*c_TrxSH) - k_R_P1*c_P1SS*c_TrxSH - k_PbR*c_PbSS*c_TrxSH - k_R_P2*c_PSS*c_TrxSH + (c_TrxSS*V_MaxTrxR)/(K_MTrxR + c_TrxSS)
        # c_TrxSS
        dYdt[14*modelsetup.Nx:15*modelsetup.Nx] += c_B_PSS*k_R_P2*c_TrxSH + k_R_P1*c_P1SS*c_TrxSH + k_PbR*c_PbSS*c_TrxSH + k_R_P2*c_PSS*c_TrxSH - (c_TrxSS*V_MaxTrxR)/(K_MTrxR + c_TrxSS)
        # c_PSH_TSH
        dYdt[15*modelsetup.Nx:16*modelsetup.Nx] += c_B_PSH_TSH*k_aB*K_DB - c_B*k_aB*c_PSH_TSH - k_aS*K_DTSH*c_PSH_TSH - c_H*k_Se*c_PSH_TSH + k_aS*c_PSH*c_TSH
        # c_PSH_TSS
        dYdt[16*modelsetup.Nx:17*modelsetup.Nx] += c_B_PSH_TSS*k_aB*K_DB - c_B*k_aB*c_PSH_TSS - k_aS*K_DTSS*c_PSH_TSS - c_H*k_Se*c_PSH_TSS - (k_TDE*c_PSH_TSS)/K_TDE + k_TDE*c_PSST + k_aS*c_PSH*c_TSS
        # c_PSOH_TSH
        dYdt[17*modelsetup.Nx:18*modelsetup.Nx] += c_B_PSOH_TSH*k_aB*K_DB + c_H*k_Se*c_PSH_TSH + k_Sr*c_PSO2H_TSH - c_B*k_aB*c_PSOH_TSH - k_C_P2*c_PSOH_TSH - k_aS*K_DTSH*c_PSOH_TSH - k_HC*c_PSOH_TSH - c_H*k_Si_P2*c_PSOH_TSH + k_aS*c_PSOH*c_TSH
        # c_PSOH_TSS
        dYdt[18*modelsetup.Nx:19*modelsetup.Nx] += c_B_PSOH_TSS*k_aB*K_DB + c_H*k_Se*c_PSH_TSS + k_Sr*c_PSO2H_TSS - c_B*k_aB*c_PSOH_TSS - k_C_P2*c_PSOH_TSS - k_aS*K_DTSS*c_PSOH_TSS - c_H*k_Si_P2*c_PSOH_TSS + k_aS*c_PSOH*c_TSS
        # c_PSO2H_TSH
        dYdt[19*modelsetup.Nx:20*modelsetup.Nx] += c_B_PSO2H_TSH*k_aB*K_DB - c_B*k_aB*c_PSO2H_TSH - k_aS*K_DTSH*c_PSO2H_TSH - k_Sr*c_PSO2H_TSH + c_H*k_Si_P2*c_PSOH_TSH + k_aS*c_PSO2H*c_TSH
        # c_PSO2H_TSS
        dYdt[20*modelsetup.Nx:21*modelsetup.Nx] += c_B_PSO2H_TSS*k_aB*K_DB - c_B*k_aB*c_PSO2H_TSS - k_aS*K_DTSS*c_PSO2H_TSS - k_Sr*c_PSO2H_TSS + c_H*k_Si_P2*c_PSOH_TSS + k_aS*c_PSO2H*c_TSS
        # c_PSS_TSH
        dYdt[21*modelsetup.Nx:22*modelsetup.Nx] += c_B_PSS_TSH*k_aB*K_DB + k_C_P2*c_PSOH_TSH + (k_ITDE*c_PSST)/K_ITDE - c_B*k_aB*c_PSS_TSH - k_aS*K_DTSH*c_PSS_TSH - k_ITDE*c_PSS_TSH + k_aS*c_PSS*c_TSH
        # c_PSS_TSS
        dYdt[22*modelsetup.Nx:23*modelsetup.Nx] += c_B_PSS_TSS*k_aB*K_DB + k_C_P2*c_PSOH_TSS - c_B*k_aB*c_PSS_TSS - k_aS*K_DTSS*c_PSS_TSS + k_aS*c_PSS*c_TSS
        # c_PbSH
        dYdt[23*modelsetup.Nx:24*modelsetup.Nx] += -(c_H*k_PbOx*c_PbSH) + k_PbR*c_PbSS*c_TrxSH
        # c_PbSS
        dYdt[24*modelsetup.Nx:25*modelsetup.Nx] += c_H*k_PbOx*c_PbSH - k_PbR*c_PbSS*c_TrxSH
        # c_B
        dYdt[25*modelsetup.Nx:26*modelsetup.Nx] += c_B_PSH_TSH*k_aB*K_DB + c_B_PSH_TSS*k_aB*K_DB + c_B_PSO2H_TSH*k_aB*K_DB + c_B_PSO2H_TSS*k_aB*K_DB + c_B_PSOH_TSH*k_aB*K_DB + c_B_PSOH_TSS*k_aB*K_DB + c_B_PSST*k_aB*K_DB + c_B_PSS_TSH*k_aB*K_DB + c_B_PSS_TSS*k_aB*K_DB + c_B_PSH*k_aBP*K_DB + c_B_PSO2H*k_aBP*K_DB + c_B_PSOH*k_aBP*K_DB + c_B_PSS*k_aBP*K_DB - c_B*k_aBP*c_PSH - c_B*k_aB*c_PSH_TSH - c_B*k_aB*c_PSH_TSS - c_B*k_aBP*c_PSO2H - c_B*k_aB*c_PSO2H_TSH - c_B*k_aB*c_PSO2H_TSS - c_B*k_aBP*c_PSOH - c_B*k_aB*c_PSOH_TSH - c_B*k_aB*c_PSOH_TSS - c_B*k_aBP*c_PSS - c_B*k_aB*c_PSST - c_B*k_aB*c_PSS_TSH - c_B*k_aB*c_PSS_TSS
        # c_B_PSH
        dYdt[26*modelsetup.Nx:27*modelsetup.Nx] += -(c_B_PSH*k_aBP*K_DB) + c_B_PSH_TSH*k_aBC*K_DTSH + c_B_PSH_TSS*k_aBC*K_DTSS - c_B_PSH*c_H*k_Se + c_B*k_aBP*c_PSH + c_B_PSS*k_R_P2*c_TrxSH - c_B_PSH*k_aBC*c_TSH - c_B_PSH*k_aBC*c_TSS
        # c_B_PSH_TSH
        dYdt[27*modelsetup.Nx:28*modelsetup.Nx] += -(c_B_PSH_TSH*k_aB*K_DB) - c_B_PSH_TSH*k_aBC*K_DTSH - c_B_PSH_TSH*c_H*k_Se + c_B*k_aB*c_PSH_TSH + c_B_PSH*k_aBC*c_TSH
        # c_B_PSH_TSS
        dYdt[28*modelsetup.Nx:29*modelsetup.Nx] += -(c_B_PSH_TSS*k_aB*K_DB) - c_B_PSH_TSS*k_aBC*K_DTSS - c_B_PSH_TSS*c_H*k_Se + c_B_PSST*k_TDE - (c_B_PSH_TSS*k_TDE)/K_TDE + c_B*k_aB*c_PSH_TSS + c_B_PSH*k_aBC*c_TSS
        # c_B_PSO2H
        dYdt[29*modelsetup.Nx:30*modelsetup.Nx] += -(c_B_PSO2H*k_aBP*K_DB) + c_B_PSO2H_TSH*k_aBC*K_DTSH + c_B_PSO2H_TSS*k_aBC*K_DTSS + c_B_PSOH*c_H*k_Si_P2 - c_B_PSO2H*k_Sr + c_B*k_aBP*c_PSO2H - c_B_PSO2H*k_aBC*c_TSH - c_B_PSO2H*k_aBC*c_TSS
        # c_B_PSO2H_TSH
        dYdt[30*modelsetup.Nx:31*modelsetup.Nx] += -(c_B_PSO2H_TSH*k_aB*K_DB) - c_B_PSO2H_TSH*k_aBC*K_DTSH + c_B_PSOH_TSH*c_H*k_Si_P2 - c_B_PSO2H_TSH*k_Sr + c_B*k_aB*c_PSO2H_TSH + c_B_PSO2H*k_aBC*c_TSH
        # c_B_PSO2H_TSS
        dYdt[31*modelsetup.Nx:32*modelsetup.Nx] += -(c_B_PSO2H_TSS*k_aB*K_DB) - c_B_PSO2H_TSS*k_aBC*K_DTSS + c_B_PSOH_TSS*c_H*k_Si_P2 - c_B_PSO2H_TSS*k_Sr + c_B*k_aB*c_PSO2H_TSS + c_B_PSO2H*k_aBC*c_TSS
        # c_B_PSOH
        dYdt[32*modelsetup.Nx:33*modelsetup.Nx] += -(c_B_PSOH*k_C_P2) - c_B_PSOH*k_aBP*K_DB + c_B_PSOH_TSH*k_aBC*K_DTSH + c_B_PSOH_TSS*k_aBC*K_DTSS + c_B_PSH*c_H*k_Se - c_B_PSOH*c_H*k_Si_P2 + c_B_PSO2H*k_Sr + c_B*k_aBP*c_PSOH - c_B_PSOH*k_aBC*c_TSH - c_B_PSOH*k_aBC*c_TSS
        # c_B_PSOH_TSH
        dYdt[33*modelsetup.Nx:34*modelsetup.Nx] += -(c_B_PSOH_TSH*k_C_P2) - c_B_PSOH_TSH*k_aB*K_DB - c_B_PSOH_TSH*k_aBC*K_DTSH - c_B_PSOH_TSH*k_HC + c_B_PSH_TSH*c_H*k_Se - c_B_PSOH_TSH*c_H*k_Si_P2 + c_B_PSO2H_TSH*k_Sr + c_B*k_aB*c_PSOH_TSH + c_B_PSOH*k_aBC*c_TSH
        # c_B_PSOH_TSS
        dYdt[34*modelsetup.Nx:35*modelsetup.Nx] += -(c_B_PSOH_TSS*k_C_P2) - c_B_PSOH_TSS*k_aB*K_DB - c_B_PSOH_TSS*k_aBC*K_DTSS + c_B_PSH_TSS*c_H*k_Se - c_B_PSOH_TSS*c_H*k_Si_P2 + c_B_PSO2H_TSS*k_Sr + c_B*k_aB*c_PSOH_TSS + c_B_PSOH*k_aBC*c_TSS
        # c_B_PSS
        dYdt[35*modelsetup.Nx:36*modelsetup.Nx] += c_B_PSOH*k_C_P2 - c_B_PSS*k_aBP*K_DB + c_B_PSS_TSH*k_aBC*K_DTSH + c_B_PSS_TSS*k_aBC*K_DTSS + c_B*k_aBP*c_PSS - c_B_PSS*k_R_P2*c_TrxSH - c_B_PSS*k_aBC*c_TSH - c_B_PSS*k_aBC*c_TSS
        # c_B_PSST
        dYdt[36*modelsetup.Nx:37*modelsetup.Nx] += -(c_B_PSST*k_aB*K_DB) + c_B_PSOH_TSH*k_HC + c_B_PSS_TSH*k_ITDE - (c_B_PSST*k_ITDE)/K_ITDE - c_B_PSST*k_TDE + (c_B_PSH_TSS*k_TDE)/K_TDE + c_B*k_aB*c_PSST
        # c_B_PSS_TSH
        dYdt[37*modelsetup.Nx:38*modelsetup.Nx] += c_B_PSOH_TSH*k_C_P2 - c_B_PSS_TSH*k_aB*K_DB - c_B_PSS_TSH*k_aBC*K_DTSH - c_B_PSS_TSH*k_ITDE + (c_B_PSST*k_ITDE)/K_ITDE + c_B*k_aB*c_PSS_TSH + c_B_PSS*k_aBC*c_TSH
        # c_B_PSS_TSS
        dYdt[38*modelsetup.Nx:39*modelsetup.Nx] += c_B_PSOH_TSS*k_C_P2 - c_B_PSS_TSS*k_aB*K_DB - c_B_PSS_TSS*k_aBC*K_DTSS + c_B*k_aB*c_PSS_TSS + c_B_PSS*k_aBC*c_TSS

        # Boundary conditions
        
        # c_H
        # dYdt[0] += 2*( -(c_H[0]*k_MPbOx*c_m_PbSH) - c_H[0]*k_Se*c_m_PSH - c_H[0]*k_Se*c_m_PSH_TSH - c_H[0]*k_Se*c_m_PSH_TSS - c_H[0]*k_Si_P2*c_m_PSOH - c_H[0]*k_Si_P2*c_m_PSOH_TSH - c_H[0]*k_Si_P2*c_m_PSOH_TSS + r_VS*v_sup*65.*np.exp(-5e-3*t) )/parameters.dx
        dYdt[0] += 2*( -(c_H[0]*k_MPbOx*c_m_PbSH) - c_H[0]*k_Se*c_m_PSH - c_H[0]*k_Se*c_m_PSH_TSH - c_H[0]*k_Se*c_m_PSH_TSS - c_H[0]*k_Si_P2*c_m_PSOH - c_H[0]*k_Si_P2*c_m_PSOH_TSH - c_H[0]*k_Si_P2*c_m_PSOH_TSS + r_VS*v_sup )/parameters.dx
        # c_P1SH
        dYdt[modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_P1SOH
        dYdt[2*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_P1SO2H
        dYdt[3*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_P1SS
        dYdt[4*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_P1SHa
        dYdt[5*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_PSH
        dYdt[6*modelsetup.Nx] += 2*( k_aMP*c_m_PSH*r_DM - k_aMP*c_PSH[0]*r_VS )/parameters.dx
        # c_PSOH
        dYdt[7*modelsetup.Nx] += 2*( k_aMP*c_m_PSOH*r_DM - k_aMP*c_PSOH[0]*r_VS )/parameters.dx
        # c_PSO2H
        dYdt[8*modelsetup.Nx] += 2*( k_aMP*c_m_PSO2H*r_DM - k_aMP*c_PSO2H[0]*r_VS )/parameters.dx
        # c_PSS
        dYdt[9*modelsetup.Nx] += 2*( k_aMP*c_m_PSS*r_DM - k_aMP*c_PSS[0]*r_VS )/parameters.dx
        # c_PSST
        dYdt[10*modelsetup.Nx] += 2*( k_aM*c_m_PSST*r_DM - k_aM*c_PSST[0]*r_VS )/parameters.dx
        # c_TSH
        dYdt[11*modelsetup.Nx] += 2*( k_aMC*K_DTSH*c_m_PSH_TSH + k_aMC*K_DTSH*c_m_PSO2H_TSH + k_aMC*K_DTSH*c_m_PSOH_TSH + k_aMC*K_DTSH*c_m_PSS_TSH - k_aMC*c_m_PSH*c_TSH[0] - k_aMC*c_m_PSO2H*c_TSH[0] - k_aMC*c_m_PSOH*c_TSH[0] - k_aMC*c_m_PSS*c_TSH[0] )/parameters.dx
        # c_TSS
        dYdt[12*modelsetup.Nx] += 2*( k_aMC*K_DTSS*c_m_PSH_TSS + k_aMC*K_DTSS*c_m_PSO2H_TSS + k_aMC*K_DTSS*c_m_PSOH_TSS + k_aMC*K_DTSS*c_m_PSS_TSS - k_aMC*c_m_PSH*c_TSS[0] - k_aMC*c_m_PSO2H*c_TSS[0] - k_aMC*c_m_PSOH*c_TSS[0] - k_aMC*c_m_PSS*c_TSS[0] )/parameters.dx
        # c_TrxSH
        dYdt[13*modelsetup.Nx] += 2*( -(k_MPbR*c_m_PbSS*c_TrxSH[0]) - k_R_P2*c_m_PSS*c_TrxSH[0] )/parameters.dx
        # c_TrxSS
        dYdt[14*modelsetup.Nx] += 2*( k_MPbR*c_m_PbSS*c_TrxSH[0] + k_R_P2*c_m_PSS*c_TrxSH[0] )/parameters.dx
        # c_PSH_TSH
        dYdt[15*modelsetup.Nx] += 2*( k_aM*c_m_PSH_TSH*r_DM - k_aM*c_PSH_TSH[0]*r_VS )/parameters.dx
        # c_PSH_TSS
        dYdt[16*modelsetup.Nx] += 2*( k_aM*c_m_PSH_TSS*r_DM - k_aM*c_PSH_TSS[0]*r_VS )/parameters.dx
        # c_PSOH_TSH
        dYdt[17*modelsetup.Nx] += 2*( k_aM*c_m_PSOH_TSH*r_DM - k_aM*c_PSOH_TSH[0]*r_VS )/parameters.dx
        # c_PSOH_TSS
        dYdt[18*modelsetup.Nx] += 2*( k_aM*c_m_PSOH_TSS*r_DM - k_aM*c_PSOH_TSS[0]*r_VS )/parameters.dx
        # c_PSO2H_TSH
        dYdt[19*modelsetup.Nx] += 2*( k_aM*c_m_PSO2H_TSH*r_DM - k_aM*c_PSO2H_TSH[0]*r_VS )/parameters.dx
        # c_PSO2H_TSS
        dYdt[20*modelsetup.Nx] += 2*( k_aM*c_m_PSO2H_TSS*r_DM - k_aM*c_PSO2H_TSS[0]*r_VS )/parameters.dx
        # c_PSS_TSH
        dYdt[21*modelsetup.Nx] += 2*( k_aM*c_m_PSS_TSH*r_DM - k_aM*c_PSS_TSH[0]*r_VS )/parameters.dx
        # c_PSS_TSS
        dYdt[22*modelsetup.Nx] += 2*( k_aM*c_m_PSS_TSS*r_DM - k_aM*c_PSS_TSS[0]*r_VS )/parameters.dx
        # c_PbSH
        dYdt[23*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_PbSS
        dYdt[24*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B
        dYdt[25*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSH
        dYdt[26*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSH_TSH
        dYdt[27*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSH_TSS
        dYdt[28*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSO2H
        dYdt[29*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSO2H_TSH
        dYdt[30*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSO2H_TSS
        dYdt[31*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSOH
        dYdt[32*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSOH_TSH
        dYdt[33*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSOH_TSS
        dYdt[34*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSS
        dYdt[35*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSST
        dYdt[36*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSS_TSH
        dYdt[37*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_B_PSS_TSS
        dYdt[38*modelsetup.Nx] += 2*( 0.0 )/parameters.dx

        # Membrane ODEs

        # c_m_PSH
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx] += -(c_H[0]*k_Se*c_m_PSH) + k_aMC*K_DTSH*c_m_PSH_TSH + k_aMC*K_DTSS*c_m_PSH_TSS - k_aMP*c_m_PSH*r_DM + k_aMP*c_PSH[0]*r_VS + k_R_P2*c_m_PSS*c_TrxSH[0] - k_aMC*c_m_PSH*c_TSH[0] - k_aMC*c_m_PSH*c_TSS[0]
        # c_m_PSOH
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 1] += c_H[0]*k_Se*c_m_PSH + k_Sr*c_m_PSO2H - k_C_P2*c_m_PSOH - c_H[0]*k_Si_P2*c_m_PSOH + k_aMC*K_DTSH*c_m_PSOH_TSH + k_aMC*K_DTSS*c_m_PSOH_TSS - k_aMP*c_m_PSOH*r_DM + k_aMP*c_PSOH[0]*r_VS - k_aMC*c_m_PSOH*c_TSH[0] - k_aMC*c_m_PSOH*c_TSS[0]
        # c_m_PSO2H
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 2] += -(k_Sr*c_m_PSO2H) + k_aMC*K_DTSH*c_m_PSO2H_TSH + k_aMC*K_DTSS*c_m_PSO2H_TSS + c_H[0]*k_Si_P2*c_m_PSOH - k_aMP*c_m_PSO2H*r_DM + k_aMP*c_PSO2H[0]*r_VS - k_aMC*c_m_PSO2H*c_TSH[0] - k_aMC*c_m_PSO2H*c_TSS[0]
        # c_m_PSS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 3] += k_C_P2*c_m_PSOH + k_aMC*K_DTSH*c_m_PSS_TSH + k_aMC*K_DTSS*c_m_PSS_TSS - k_aMP*c_m_PSS*r_DM + k_aMP*c_PSS[0]*r_VS - k_R_P2*c_m_PSS*c_TrxSH[0] - k_aMC*c_m_PSS*c_TSH[0] - k_aMC*c_m_PSS*c_TSS[0]
        # c_m_PSST
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 4] += (k_TDE*c_m_PSH_TSS)/K_TDE + k_HC*c_m_PSOH_TSH - (k_ITDE*c_m_PSST)/K_ITDE - k_TDE*c_m_PSST + k_ITDE*c_m_PSS_TSH - k_aM*c_m_PSST*r_DM + k_aM*c_PSST[0]*r_VS
        # c_m_PSH_TSH
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 5] += -(k_aMC*K_DTSH*c_m_PSH_TSH) - c_H[0]*k_Se*c_m_PSH_TSH - k_aM*c_m_PSH_TSH*r_DM + k_aM*c_PSH_TSH[0]*r_VS + k_aMC*c_m_PSH*c_TSH[0]
        # c_m_PSH_TSS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 6] += -(k_aMC*K_DTSS*c_m_PSH_TSS) - c_H[0]*k_Se*c_m_PSH_TSS - (k_TDE*c_m_PSH_TSS)/K_TDE + k_TDE*c_m_PSST - k_aM*c_m_PSH_TSS*r_DM + k_aM*c_PSH_TSS[0]*r_VS + k_aMC*c_m_PSH*c_TSS[0]
        # c_m_PSOH_TSH
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 7] += c_H[0]*k_Se*c_m_PSH_TSH + k_Sr*c_m_PSO2H_TSH - k_C_P2*c_m_PSOH_TSH - k_aMC*K_DTSH*c_m_PSOH_TSH - k_HC*c_m_PSOH_TSH - c_H[0]*k_Si_P2*c_m_PSOH_TSH - k_aM*c_m_PSOH_TSH*r_DM + k_aM*c_PSOH_TSH[0]*r_VS + k_aMC*c_m_PSOH*c_TSH[0]
        # c_m_PSOH_TSS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 8] += c_H[0]*k_Se*c_m_PSH_TSS + k_Sr*c_m_PSO2H_TSS - k_C_P2*c_m_PSOH_TSS - k_aMC*K_DTSS*c_m_PSOH_TSS - c_H[0]*k_Si_P2*c_m_PSOH_TSS - k_aM*c_m_PSOH_TSS*r_DM + k_aM*c_PSOH_TSS[0]*r_VS + k_aMC*c_m_PSOH*c_TSS[0]
        # c_m_PSO2H_TSH
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 9] += -(k_aMC*K_DTSH*c_m_PSO2H_TSH) - k_Sr*c_m_PSO2H_TSH + c_H[0]*k_Si_P2*c_m_PSOH_TSH - k_aM*c_m_PSO2H_TSH*r_DM + k_aM*c_PSO2H_TSH[0]*r_VS + k_aMC*c_m_PSO2H*c_TSH[0]
        # c_m_PSO2H_TSS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 10] += -(k_aMC*K_DTSS*c_m_PSO2H_TSS) - k_Sr*c_m_PSO2H_TSS + c_H[0]*k_Si_P2*c_m_PSOH_TSS - k_aM*c_m_PSO2H_TSS*r_DM + k_aM*c_PSO2H_TSS[0]*r_VS + k_aMC*c_m_PSO2H*c_TSS[0]
        # c_m_PSS_TSH
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 11] += k_C_P2*c_m_PSOH_TSH + (k_ITDE*c_m_PSST)/K_ITDE - k_aMC*K_DTSH*c_m_PSS_TSH - k_ITDE*c_m_PSS_TSH - k_aM*c_m_PSS_TSH*r_DM + k_aM*c_PSS_TSH[0]*r_VS + k_aMC*c_m_PSS*c_TSH[0]
        # c_m_PSS_TSS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 12] += k_C_P2*c_m_PSOH_TSS - k_aMC*K_DTSS*c_m_PSS_TSS - k_aM*c_m_PSS_TSS*r_DM + k_aM*c_PSS_TSS[0]*r_VS + k_aMC*c_m_PSS*c_TSS[0]
        # c_m_PbSH
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 13] += -(c_H[0]*k_MPbOx*c_m_PbSH) + k_MPbR*c_m_PbSS*c_TrxSH[0]
        # c_m_PbSS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 14] += c_H[0]*k_MPbOx*c_m_PbSH - k_MPbR*c_m_PbSS*c_TrxSH[0]

        # Compute other quantities of interest
        
        analysis.TSS_gen_domain += simpson(k_aS*K_DTSS*c_PSH_TSS + k_aS*K_DTSS*c_PSO2H_TSS + k_aS*K_DTSS*c_PSOH_TSS + k_aS*K_DTSS*c_PSS_TSS
                                         + c_B_PSH_TSS*k_aBC*K_DTSS + + c_B_PSO2H_TSS*k_aBC*K_DTSS + c_B_PSOH_TSS*k_aBC*K_DTSS + c_B_PSS_TSS*k_aBC*K_DTSS, x=modelsetup.x)
        
        analysis.TSS_gen_boundary += k_aMC*K_DTSS*c_m_PSH_TSS + k_aMC*K_DTSS*c_m_PSO2H_TSS + k_aMC*K_DTSS*c_m_PSOH_TSS + k_aMC*K_DTSS*c_m_PSS_TSS
        
        return dYdt
    
    ############################# Solve ODE ############################
    
    # Set up ODE integrator
    
    solver = LSODA(f, t_init, Y_0, t_end, max_step=max_step)
    
    # Integrate ODE forward
    
    while solver.t < t_end:
        
        # Step solver forward
        
        solver.step()
        
    ########## Calculate diagnostics and do printing/plotting ##########
        
        # Check if TSS has reached fraction in middle of cell and if
        # so record time
        
        if (solver.y[13*modelsetup.Nx-1] >=
            analysis.TSS_to_centre_frac*parameters.c_T_init_value
            and analysis.TSS_to_centre_frac_time == 0.0):
            
            analysis.TSS_to_centre_frac_time = solver.t
        
        # Calculate TSS mean
        
        analysis.TSS_mean = np.mean(solver.y[12*modelsetup.Nx:13*modelsetup.Nx])
        
        # Output run time to screen
        
        output = 'Run time = {:.6f}s.'.format(solver.t)
        
        # Check conservation
        
        # Initialise mass
        
        mass = 0.0
        
        # Integrate mass of each bulk concentration excluding c_H
        
        for ii in np.arange(1,modelsetup.n_bulk_species):
            
            mass += simpson(solver.y[ii*modelsetup.Nx:(ii+1)*modelsetup.Nx], x=modelsetup.x)
        
        # Add mass of boundary concentrations
        
        mass += np.sum(solver.y[-(modelsetup.n_memb_species):])
        
        # Add to screen output
        
        output += '    ' + 'Conservation: {:.8f}%'.format(100*(mass/mass_init))
    
        Printer(output)
    
    ############################# Timing ###############################

    end = timer()
    print('\nElapsed time: {:.2f} s'.format(end - start))
    
    ############################## Output ##############################
    
    # Save final and initial concentrations in array
    
    if saving:
        
        # The formatting for the saved output is as follows:
        #
        # The first output file will contain the 25 bulk concentrations 
        # in their initial state in the following order:
        #
        # c_H_init
        # c_P1SH_init
        # c_P1SOH_init
        # c_P1SO2H_init
        # c_P1SS_init
        # c_P1SHa_init
        # c_PSH_init
        # c_PSOH_init
        # c_PSO2H_init
        # c_PSS_init
        # c_PSST_init
        # c_TSH_init
        # c_TSS_init
        # c_TrxSH_init
        # c_TrxSS_init
        # c_PSH_TSH_init
        # c_PSH_TSS_init
        # c_PSOH_TSH_init
        # c_PSOH_TSS_init
        # c_PSO2H_TSH_init
        # c_PSO2H_TSS_init
        # c_PSS_TSH_init
        # c_PSS_TSS_init
        # c_PbSH_init
        # c_PbSS_init
        # c_B_init
        # c_B_PSH_init
        # c_B_PSH_TSH_init
        # c_B_PSH_TSS_init
        # c_B_PSO2H_init
        # c_B_PSO2H_TSH_init
        # c_B_PSO2H_TSS_init
        # c_B_PSOH_init
        # c_B_PSOH_TSH_init
        # c_B_PSOH_TSS_init
        # c_B_PSS_init
        # c_B_PSST_init
        # c_B_PSS_TSH_init
        # c_B_PSS_TSS_init
        #
        # These therefore occupy the first 39 columns.
        # The first 15 elements of the final (40th) column store the
        # membrane concentrations in their initial state in the order
        # given below.
        #
        # The second output file will contain the 39 bulk concentrations 
        # in their final state in the same order as above.
        # 
        # These therefore occupy the first 39 columns.
        # The first 15 elements of the final (40th) column store the
        # membrane concentrations.
        #
        # The 15 membrane concentrations are in the following order:
        #
        # c_m_PSH
        # c_m_PSOH
        # c_m_PSO2H
        # c_m_PSS
        # c_m_PSST
        # c_m_PSH_TSH
        # c_m_PSH_TSS
        # c_m_PSOH_TSH
        # c_m_PSOH_TSS
        # c_m_PSO2H_TSH
        # c_m_PSO2H_TSS
        # c_m_PSS_TSH
        # c_m_PSS_TSS
        # c_m_PbSH
        # c_m_PbSS
        
        # Add IC to output directory as a subdirectory

        newdir = output_dir.split('/')[-1]

        output_dir = os.path.join(output_dir, newdir + '_' + modelsetup.ic + '/')

        if not os.path.exists(os.path.dirname(output_dir)):
            try:
                os.makedirs(os.path.dirname(output_dir))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        
        # Initialise output files
        
        concs_file_init = np.zeros((modelsetup.Nx, modelsetup.n_bulk_species + 1))
        concs_file_final = np.zeros((modelsetup.Nx, modelsetup.n_bulk_species + 1))
        
        # Add c_init and c_final for each of the "n_bulk_species" concentrations
        # to concs_files
        
        for ii in np.arange(modelsetup.n_bulk_species):
            
            concs_file_init[:,ii] = Y_0[ii*modelsetup.Nx:(ii+1)*modelsetup.Nx]
            concs_file_final[:,ii] = solver.y[ii*modelsetup.Nx:(ii+1)*modelsetup.Nx]
        
        # Add membrane concentrations
        concs_file_init[:(modelsetup.n_memb_species), modelsetup.n_bulk_species] = Y_0[-(modelsetup.n_memb_species):]
        concs_file_final[:(modelsetup.n_memb_species), modelsetup.n_bulk_species] = solver.y[-(modelsetup.n_memb_species):]
        
        # Save output as csv files
        
        np.savetxt(output_dir +
                   'concs_at_T={:.4f}s.csv'.format(0.0),
                   concs_file_init, delimiter = ',')
        
        np.savetxt(output_dir +
                   'concs_at_T={:.4f}s.csv'.format(t_end),
                   concs_file_final, delimiter = ',')
        
        # Save simulation paremeters to text file within output directory
        
        with open(output_dir + 'simulation_parameters.txt', 'w') as f:

            print(parameters, end="", file=f)
                    
        # Save TSS Mean to text file within output directory
        
        with open(output_dir + 'tss_properties.txt', 'a+') as f:

            f.write('-------------------------------\n' +
                    'T = {:.4f}s\n'.format(t_end) +
                    '-------------------------------\n' +
                    'TSS mean         = {:.5e}\n'.format(analysis.TSS_mean) +
                    'TSS gen domain   = {:.5e}\n'.format(analysis.TSS_gen_domain) +
                    'TSS gen boundary = {:.5e}\n'.format(analysis.TSS_gen_boundary))

    # Prepare interactive output
    
    # Initialise arrays
    
    c_bulk_out = np.zeros((modelsetup.Nx, modelsetup.n_bulk_species))
    c_m_out = np.zeros(modelsetup.n_memb_species)
    
    # Get bulk concentrations from solver
    
    for ii in np.arange(modelsetup.n_bulk_species):
        c_bulk_out[:,ii] = solver.y[ii*modelsetup.Nx:(ii+1)*modelsetup.Nx]
    
    # Get membrane concentrations from solver
    
    c_m_out = solver.y[-(modelsetup.n_memb_species):]
    
    # Print diagnostic values to screen
    
    print('TSS Generation at boundary:   ',
          '{:.5e}'.format(analysis.TSS_gen_boundary))
    
    print('TSS Generation in domain:     ',
          '{:.5e}'.format(analysis.TSS_gen_domain))
    
    print('Time for {:d}'.format(int(analysis.TSS_to_centre_frac*100)) +
          '% TSS accumulation: {:.5f}s'.format(analysis.TSS_to_centre_frac_time))
          
    print('TSS Mean in domain:            {:.5e}'.format(analysis.TSS_mean))
    
    return c_bulk_out, c_m_out

if __name__ == '__main__':

    c_bulk, c_m = hpscbml()