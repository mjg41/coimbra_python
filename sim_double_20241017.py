# Template for doing multiple simulation runs

from hpsignal_complete_doublebinding_method_of_lines import *

v_sup_m1_values = [1.00000000e-02, 1.00000000e-01, 1.00000000e+00, 1.00000000e+01,
                   1.00000000e+02]
v_sup_m2_values = [1.00000000e-02, 1.00000000e-01, 1.00000000e+00, 1.00000000e+01,
                   1.00000000e+02]

# Loop over parameters

for v_sup_m1 in v_sup_m1_values:
    for v_sup_m2 in v_sup_m2_values:

        parameters = Parameters(v_sup_m1 = v_sup_m1, v_sup_m2 = v_sup_m2, k_aS = 1.0,
                                K_DT1SH = 0.1, K_DT1SS = 10000.0, k_aMC_T1_m1 = 10.0,
                                k_aMC_T2_m2 = 10.0, k_aMC_T2_m1 = 0.0, k_aMC_T1_m2 = 0.0)
        modelsetup = ModelSetup(parameters)
        modelsetup.ic = '(v_sup_m1={:.4f}_v_sup_m2={:.4f})'.format(parameters.v_sup_m1, parameters.v_sup_m2)

        _,_ = hpscdbml(parameters = parameters, modelsetup = modelsetup, t_end=100000.0, saving=True, output_dir = 'output/20241017_two_membrane_test')
