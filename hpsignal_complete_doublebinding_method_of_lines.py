# Solve numerically the complete system which builds on the equations 
# described in Eqns (1) & (2) of Griffith et al. (2024).
# Here the domain is not assumed to be symmetric and so each
# membrane is considered distinctly.
# The PDEs are discretised spatially and the resulting ODE
# system is solved using the built-in LSODA method in scipy.

import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
from scipy.sparse import diags
from scipy.integrate import simps, LSODA
from timeit import default_timer as timer
import sys
import os, errno

###############################################################################

# Units of distance are dm throughout

###############################################################################

#############################    SETUP CLASSES    #############################

# Create class to do dynamic printing on one line

class Printer():
    """Print things to stdout on one line dynamically"""
    def __init__(self,data):
        sys.stdout.write("\r\x1b[K"+data.__str__())
        sys.stdout.flush()

# Create class to store various quantities of interest for simulation analysis
# These can then be viewed easily using vars(class)

class Analysis(object):
    '''Class to store various quantities of interest for simulation analysis'''
    def __init__(self,
                 T1SS_gen_domain = 0.0,
                 T1SS_gen_boundary = 0.0,
                 T1SS_to_centre_frac = 0.2,
                 T1SS_to_centre_frac_time = 0.0,
                 T1SS_mean = 0.0,
                 T2SS_gen_domain = 0.0,
                 T2SS_gen_boundary = 0.0,
                 T2SS_to_centre_frac = 0.2,
                 T2SS_to_centre_frac_time = 0.0,
                 T2SS_mean = 0.0):
        self.T1SS_gen_domain = T1SS_gen_domain
        self.T1SS_gen_boundary = T1SS_gen_boundary
        self.T1SS_to_centre_frac = T1SS_to_centre_frac
        self.T1SS_to_centre_frac_time = T1SS_to_centre_frac_time
        self.T1SS_mean = T1SS_mean
        self.T2SS_gen_domain = T2SS_gen_domain
        self.T2SS_gen_boundary = T2SS_gen_boundary
        self.T2SS_to_centre_frac = T2SS_to_centre_frac
        self.T2SS_to_centre_frac_time = T2SS_to_centre_frac_time
        self.T2SS_mean = T2SS_mean
    
    def reset(self):
        self.T1SS_gen_domain = 0.0
        self.T1SS_gen_boundary = 0.0
        self.T1SS_to_centre_frac_time = 0.0
        self.T1SS_mean = 0.0
        self.T2SS_gen_domain = 0.0
        self.T2SS_gen_boundary = 0.0
        self.T2SS_to_centre_frac_time = 0.0
        self.T2SS_mean = 0.0

# Set up class containing parameters
# These can then be viewed easily using vars(class)

class Parameters: 
    '''Class containing simulation parameters'''
    def __init__(self,
                 v_sup_m1 = 15.6,
                 v_sup_m2 = None,
                 k_Alt = 100.0,
                 k_aM = 0.0,               # possibly: 0.1,1,10,100            <----- Zeroed for experiment
                 k_aMP_m1 = 0.01,          # possibly: 0.001, 0.01, 0.1, 1, 10 <----- Zeroed for experiment
                 k_aMP_m2 = None,          # possibly: 0.001, 0.01, 0.1, 1, 10 <----- Zeroed for experiment
                 k_aMC_T1_m1 = 1.0,        # possibly: 0.001, 0.01, 0.1, 1, 10 <----- Zeroed for experiment
                 k_aMC_T2_m1 = 1.0,        # possibly: 0.001, 0.01, 0.1, 1, 10 <----- Zeroed for experiment
                 k_aMC_T1_m2 = None,       # possibly: 0.001, 0.01, 0.1, 1, 10 <----- Zeroed for experiment
                 k_aMC_T2_m2 = None,       # possibly: 0.001, 0.01, 0.1, 1, 10 <----- Zeroed for experiment
                 k_aS = 0.00774264,
                 k_C_P2 = 0.5,
                 k_C_P1 = 11.0,
                 k_I = 15.8,
                 K_DT1SH = 30.0,
                 K_DT1SS = 30.0,
                 K_DT2SH = None,
                 K_DT2SS = None,
                 k_HC_T1 = 0.21,
                 k_HC_T2 = None,
                 k_R_P2 = 0.61,
                 k_R_P1 = 2.2,
                 k_Se = 100.0,
                 k_Si_P2 = 0.0034,
                 k_Si_P1 = 0.0015,
                 k_Sr = 0.001,
                 k_TDE = 1.0,       # possibly: 0.01,0.1,1,10,100
                 K_TDE = 1.0,
                 k_ITDE = 0.0,
                 K_ITDE = 1.0,
                 k_TR = 100.0,
                 k_TSSR = 0.0225,
                 k_PbOx = 2.5,
                 k_PbR = 2.9e-4,
                 k_MPbOx = 2.5,
                 k_MPbR = 2.9e-4,
                 r_DM = 200.0,
                 V_MaxTrxR = 180.0,
                 K_MTrxR = 1.8,
                 D_1 = 3.7e-7,
                 D_2 = 5.2e-11,
                 D_3 = 9e-11,
                 D_4 = 4.6e-11,
                 D_5 = 3.5e-9,
                 D_6 = 7.9e-11,
                 D_Pb = 2.7e-10,
                 c_P1_init_value = 110.0,
                 c_P2_init_value = 30.0,
                 c_T1_init_value = 0.05,
                 c_T2_init_value = None,
                 c_Trx_init_value = 20.0,
                 c_Pb_init_value = 5.0,
                 dx = 2e-6,         # This gives 51 points in total
                 length = 1e-4):
                 
        self.v_sup_m1 = v_sup_m1
        if v_sup_m2 is None:
            self.v_sup_m2 = v_sup_m1
        else:
            self.v_sup_m2 = v_sup_m2
        self.k_Alt = k_Alt
        self.k_aM = k_aM
        self.k_aMP_m1 = k_aMP_m1
        if k_aMP_m2 is None:
            self.k_aMP_m2 = k_aMP_m1
        else:
            self.k_aMP_m2 = k_aMP_m2
        self.k_aMC_T1_m1 = k_aMC_T1_m1
        self.k_aMC_T2_m1 = k_aMC_T2_m1
        if k_aMC_T1_m2 is None:
            self.k_aMC_T1_m2 = k_aMC_T1_m1
        else:
            self.k_aMC_T1_m2 = k_aMC_T1_m2
        if k_aMC_T2_m2 is None:
            self.k_aMC_T2_m2 = k_aMC_T2_m1
        else:
            self.k_aMC_T2_m2 = k_aMC_T2_m2
        self.k_aS = k_aS
        self.k_C_P2 = k_C_P2
        self.k_C_P1 = k_C_P1
        self.k_I = k_I
        self.K_DT1SH = K_DT1SH
        self.K_DT1SS = K_DT1SS
        if K_DT2SH is None:
            self.K_DT2SH = K_DT1SH
        else:
            self.K_DT2SH = K_DT2SH
        if K_DT2SS is None:
            self.K_DT2SS = K_DT1SS
        else:
            self.K_DT2SS = K_DT2SS        
        self.k_HC_T1 = k_HC_T1
        if k_HC_T2 is None:
            self.k_HC_T2 = k_HC_T1
        else:
            self.k_HC_T2 = k_HC_T2
        self.k_R_P2 = k_R_P2
        self.k_R_P1 = k_R_P1
        self.k_Se = k_Se
        self.k_Si_P2 = k_Si_P2
        self.k_Si_P1 = k_Si_P1
        self.k_Sr = k_Sr
        self.k_TDE = k_TDE
        self.K_TDE = K_TDE
        self.k_ITDE = k_ITDE
        self.K_ITDE = K_ITDE
        self.k_TR = k_TR
        self.k_TSSR = k_TSSR
        self.k_PbOx = k_PbOx
        self.k_PbR = k_PbR
        self.k_MPbOx = k_MPbOx
        self.k_MPbR = k_MPbR
        self.r_DM = r_DM

        self.V_MaxTrxR = V_MaxTrxR
        self.K_MTrxR = K_MTrxR

        self.D_1 = D_1
        self.D_2 = D_2
        self.D_3 = D_3
        self.D_4 = D_4
        self.D_5 = D_5
        self.D_6 = D_6
        self.D_Pb = D_Pb

        self.dx = dx
        self.length = length
        self.r_VS = self.length/2

        self.c_P1_init_value = c_P1_init_value
        self.c_P2_init_value = c_P2_init_value
        self.c_T1_init_value = c_T1_init_value
        if not c_T2_init_value:
            self.c_T2_init_value = c_T1_init_value
        else:
            self.c_T2_init_value = c_T2_init_value
        self.c_Trx_init_value = c_Trx_init_value
        self.c_Pb_init_value = c_Pb_init_value

    def __str__(self):

        out = ('Length units are dm throughout\n' +
               '----------------------------------\n' +
               'v_sup_m1          = {:.5f}\n'.format(self.v_sup_m1) +
               'v_sup_m2          = {:.5f}\n'.format(self.v_sup_m2) +
               'k_Alt             = {:.5f}\n'.format(self.k_Alt) +
               'k_aM              = {:.5f}\n'.format(self.k_aM) +
               'k_aMP_m1          = {:.5f}\n'.format(self.k_aMP_m1) +
               'k_aMP_m2          = {:.5f}\n'.format(self.k_aMP_m2) +
               'k_aMC_T1_m1       = {:.5f}\n'.format(self.k_aMC_T1_m1) +
               'k_aMC_T2_m1       = {:.5f}\n'.format(self.k_aMC_T2_m1) +
               'k_aMC_T1_m2       = {:.5f}\n'.format(self.k_aMC_T1_m2) +
               'k_aMC_T2_m2       = {:.5f}\n'.format(self.k_aMC_T2_m2) +
               'k_aS              = {:.6f}\n'.format(self.k_aS) +
               'k_C_P2            = {:.5f}\n'.format(self.k_C_P2) +
               'k_C_P1            = {:.5f}\n'.format(self.k_C_P1) +
               'k_I               = {:.5f}\n'.format(self.k_I) +
               'K_DT1SH           = {:.5f}\n'.format(self.K_DT1SH) +
               'K_DT1SS           = {:.5f}\n'.format(self.K_DT1SS) +
               'K_DT2SH           = {:.5f}\n'.format(self.K_DT2SH) +
               'K_DT2SS           = {:.5f}\n'.format(self.K_DT2SS) +
               'k_HC_T1           = {:.5f}\n'.format(self.k_HC_T1) +
               'k_HC_T2           = {:.5f}\n'.format(self.k_HC_T2) +
               'k_R_P2            = {:.5f}\n'.format(self.k_R_P2) +
               'k_R_P1            = {:.5f}\n'.format(self.k_R_P1) +
               'k_Se              = {:.5f}\n'.format(self.k_Se) +
               'k_Si_P2           = {:.5f}\n'.format(self.k_Si_P2) +
               'k_Si_P1           = {:.5f}\n'.format(self.k_Si_P1) +
               'k_Sr              = {:.5f}\n'.format(self.k_Sr) +
               'k_TDE             = {:.5f}\n'.format(self.k_TDE) +
               'K_TDE             = {:.5f}\n'.format(self.K_TDE) +
               'k_ITDE            = {:.5f}\n'.format(self.k_ITDE) +
               'K_ITDE            = {:.5f}\n'.format(self.K_ITDE) +
               'k_TR              = {:.5f}\n'.format(self.k_TR) +
               'k_TSSR            = {:.5f}\n'.format(self.k_TSSR) +
               'k_PbOx            = {:.5f}\n'.format(self.k_PbOx) +
               'k_PbR             = {:.5f}\n'.format(self.k_PbR) +
               'k_MPbOx           = {:.5f}\n'.format(self.k_MPbOx) +
               'k_MPbR            = {:.5f}\n'.format(self.k_MPbR) +
               'r_DM              = {:.5f}\n'.format(self.r_DM) +
               'V_MaxTrxR         = {:.5f}\n'.format(self.V_MaxTrxR) +
               'K_MTrxR           = {:.5f}\n'.format(self.K_MTrxR) +
               'r_VS              = {:.5e}\n'.format(self.r_VS) +
               'D_1               = {:.5e}\n'.format(self.D_1) +
               'D_2               = {:.5e}\n'.format(self.D_2) +
               'D_3               = {:.5e}\n'.format(self.D_3) +
               'D_4               = {:.5e}\n'.format(self.D_4) +
               'D_5               = {:.5e}\n'.format(self.D_5) +
               'D_6               = {:.5e}\n'.format(self.D_6) +
               'D_Pb              = {:.5e}\n'.format(self.D_Pb) +
               'Domain Length     = {:.5e}\n'.format(self.length) +
               'Initial P1 total  = {:.5f}\n'.format(self.c_P1_init_value) +
               'Initial P2 total  = {:.5f}\n'.format(self.c_P2_init_value) +
               'Initial T1 total  = {:.5f}\n'.format(self.c_T1_init_value) +
               'Initial T2 total  = {:.5f}\n'.format(self.c_T2_init_value) +
               'Initial Trx total = {:.5f}\n'.format(self.c_Trx_init_value) +
               'Initial Pb total  = {:.5f}'.format(self.c_Pb_init_value))
        
        return out

# Set up class containing computed quantities related to model setup using values of
# parameters from Parameters class
# These can then be viewed easily using vars(class)

class ModelSetup(Parameters):
    '''Class containing computed quantities related to model setup using values of parameters from Parameters class'''
    def __init__(self,
                 parameters,
                 n_bulk_species = 36,
                 n_memb_species = 48):

        self.Nx = int(parameters.length/parameters.dx) + 1
        self.x = np.linspace(0, parameters.length, self.Nx)
        self.n_bulk_species = n_bulk_species
        self.n_memb_species = n_memb_species
        self.c_bulk_init, self.c_m_init = self.build_init_conds(parameters)
        # self.ic = '(v_sup=65exp-5e-3t_k_aM={:.1f}_k_aMC={:.2f}_k_TDE={:.2f})'.format(parameters.k_aM, parameters.k_aMC_T1_m1, parameters.k_TDE)
        # self.ic = '(k_aM={:.1f}_k_aMC={:.2f}_k_TDE={:.2f})'.format(parameters.k_aM, parameters.k_aMC_T1_m1, parameters.k_TDE)
        # self.ic = '(k_aS={:.4f}_k_TSSR={:.5f})'.format(parameters.k_aS, parameters.k_TSSR)
        # self.ic = '(v_sup={:.4f}_k_aMC={:.3f}_K_DT1SH={:.3f})'.format(parameters.v_sup, parameters.k_aMC_T1_m1, parameters.K_DT1SH)
        self.ic = '(v_sup_m1={:.4f})'.format(parameters.v_sup_m1)

    def build_init_conds(self, parameters):

        # Initialise empty conc array

        # The 36 bulk concentrations are in the following order:
        #
        # c_H
        # c_P1SH
        # c_P1SOH
        # c_P1SO2H
        # c_P1SS
        # c_P1SHa
        # c_PSH
        # c_PSOH
        # c_PSO2H
        # c_PSS
        # c_PSST1
        # c_T1SH
        # c_T1SS
        # c_TrxSH
        # c_TrxSS
        # c_PSH_T1SH
        # c_PSH_T1SS
        # c_PSOH_T1SH
        # c_PSOH_T1SS
        # c_PSO2H_T1SH
        # c_PSO2H_T1SS
        # c_PSS_T1SH
        # c_PSS_T1SS
        # c_PSST2
        # c_T2SH
        # c_T2SS
        # c_PSH_T2SH
        # c_PSH_T2SS
        # c_PSOH_T2SH
        # c_PSOH_T2SS
        # c_PSO2H_T2SH
        # c_PSO2H_T2SS
        # c_PSS_T2SH
        # c_PSS_T2SS
        # c_PbSH
        # c_PbSS
        #
        # The 48 membrane concentrations (24 for membrane 1 (left) and
        # 24 for membrane 2 (right)) are in the following order:
        #
        # c_m1_PSH
        # c_m1_PSOH
        # c_m1_PSO2H
        # c_m1_PSS
        # c_m1_PSST1
        # c_m1_PSH_T1SH
        # c_m1_PSH_T1SS
        # c_m1_PSOH_T1SH
        # c_m1_PSOH_T1SS
        # c_m1_PSO2H_T1SH
        # c_m1_PSO2H_T1SS
        # c_m1_PSS_T1SH
        # c_m1_PSS_T1SS
        # c_m1_PSST2
        # c_m1_PSH_T2SH
        # c_m1_PSH_T2SS
        # c_m1_PSOH_T2SH
        # c_m1_PSOH_T2SS
        # c_m1_PSO2H_T2SH
        # c_m1_PSO2H_T2SS
        # c_m1_PSS_T2SH
        # c_m1_PSS_T2SS
        # c_m1_PbSH
        # c_m1_PbSS

        # c_m2_PSH
        # c_m2_PSOH
        # c_m2_PSO2H
        # c_m2_PSS
        # c_m2_PSST1
        # c_m2_PSH_T1SH
        # c_m2_PSH_T1SS
        # c_m2_PSOH_T1SH
        # c_m2_PSOH_T1SS
        # c_m2_PSO2H_T1SH
        # c_m2_PSO2H_T1SS
        # c_m2_PSS_T1SH
        # c_m2_PSS_T1SS
        # c_m2_PSST2
        # c_m2_PSH_T2SH
        # c_m2_PSH_T2SS
        # c_m2_PSOH_T2SH
        # c_m2_PSOH_T2SS
        # c_m2_PSO2H_T2SH
        # c_m2_PSO2H_T2SS
        # c_m2_PSS_T2SH
        # c_m2_PSS_T2SS
        # c_m2_PbSH
        # c_m2_PbSS

        c_bulk_init = np.zeros((self.Nx,self.n_bulk_species))
        
        c_m_init = np.zeros(self.n_memb_species)

        c_bulk_init[:,1] = parameters.c_P1_init_value        # c_P1SH

        c_bulk_init[:,6] = parameters.c_P2_init_value         # c_PSH

        c_bulk_init[:,11] = parameters.c_T1_init_value       # c_T1SH

        c_bulk_init[:,13] = parameters.c_Trx_init_value      # c_TrxSH

        c_bulk_init[:,24] = parameters.c_T2_init_value       # c_T2SH

        c_bulk_init[:,34] = parameters.c_Pb_init_value      # c_PbSH

        #c_bulk_init[:,1] = 0.99*parameters.c_P1_init_value                                   # c_P1SH
        #c_bulk_init[:,2] = 0.005*parameters.c_P1_init_value                                  # c_P1SOH
        #c_bulk_init[:,5] = 0.005*parameters.c_P1_init_value                                  # c_P1SS

        #c_bulk_init[:,6] = (0.98 - 0.5*parameters.c_T1_init_value)*parameters.c_P2_init_value  # c_PSH
        #c_bulk_init[:,6] = 0.98*parameters.c_P2_init_value                                    # c_PSH
        #c_bulk_init[:,7] = 0.01*parameters.c_P2_init_value                                    # c_PSOH
        #c_bulk_init[:,9] = 0.01*parameters.c_P2_init_value                                    # c_PSS

        #c_bulk_init[:,11] = 0.49*parameters.c_T1_init_value                                   # c_T1SH
        #c_bulk_init[:,15] = (0.49 + 0.5*parameters.c_P2_init_value)*parameters.c_T1_init_value # c_PSH_T1SH
        #c_bulk_init[:,12] = 0.01*parameters.c_T1_init_value                                   # c_T1SS
        #c_bulk_init[:,10] = 0.01*parameters.c_T1_init_value                                    # c_PSST1

        #c_bulk_init[:,13] = 0.99*parameters.c_Trx_init_value                                 # c_TrxSH
        #c_bulk_init[:,14] = 0.01*parameters.c_Trx_init_value                                 # c_TrxSS

        return c_bulk_init, c_m_init

###############################################################################

#############################  PLOTTING FUNCTION  #############################

def plot_concs(c_bulk, c_m,
               parameters = Parameters(), modelsetup = ModelSetup(Parameters()),
               t_end=np.nan):
    
    '''Plot concentrations outputted by pde routine.
    
    Positional arguments:
    c_bulk are the bulk concentrations from the pde routine.
    c_m    are the membrane concentrations from the pde routine.
    
    Optional keyword arguments:
    parameters is the class containing all parameters to use for the simulation. Default as in init method.
    modelsetup is the class containing all computed quantities related to model setup. Default as in init method.
    t_end is the end time of the pde simulation. Default is NaN.
    '''

    # Initialise perox figure and axes (first 10 concs)
    
    fig_perox, axes_perox = plt.subplots(4,3,figsize=[10,11])

    # Initialise T1 figure and axes (next 3, skip 2, then 8 after)
    
    fig_T1, axes_T1 = plt.subplots(4,3,figsize=[10,11])

    # Initialise Trx figure and axes (skipped 2)
    
    fig_Trx, axes_Trx = plt.subplots(1,2,figsize=[10,4])

    # Initialise T2 figure and axes (next 11)
    
    fig_T2, axes_T2 = plt.subplots(4,3,figsize=[10,11])

    # Initialise Probe figure and axes (final 2)
    
    fig_Pb, axes_Pb = plt.subplots(1,2,figsize=[10,4])

    # Loop over figures doing formatting

    for fig in [fig_perox, fig_T1, fig_Trx, fig_T2, fig_Pb]:

        # Nicely space figures leaving room for title
    
        fig.tight_layout(pad=3.75, h_pad=None, w_pad=None)
    
        # Plot title
            
        fig.suptitle('v_sup_m1 = ' +
                     str(parameters.v_sup_m1) +
                     '   v_sup_m2 = ' +
                     str(parameters.v_sup_m2) +
                     '   Model at ' + 
                     str('{:.2f}'.format(t_end)) + 's.')
    
    # List of subplot titles
    
    title_list = ['$c_{\mathrm{H}}$',
                  '$c_{\mathrm{P1SH}}$',
                  '$c_{\mathrm{P1SOH}}$',
                  '$c_{\mathrm{P1SO2H}}$',
                  '$c_{\mathrm{P1SS}}$',
                  '$c_{\mathrm{P1SHa}}$',
                  '$c_{\mathrm{PSH}}$',
                  '$c_{\mathrm{PSOH}}$',
                  '$c_{\mathrm{PSO2H}}$',
                  '$c_{\mathrm{PSS}}$',
                  '$c_{\mathrm{PSST1}}$',
                  '$c_{\mathrm{T1SH}}$',
                  '$c_{\mathrm{T1SS}}$',
                  '$c_{\mathrm{TrxSH}}$',
                  '$c_{\mathrm{TrxSS}}$',
                  '$c_{\mathrm{PSH\$T1SH}}$',
                  '$c_{\mathrm{PSH\$T1SS}}$',
                  '$c_{\mathrm{PSOH\$T1SH}}$',
                  '$c_{\mathrm{PSOH\$T1SS}}$',
                  '$c_{\mathrm{PSO2H\$T1SH}}$',
                  '$c_{\mathrm{PSO2H\$T1SS}}$',
                  '$c_{\mathrm{PSS\$T1SH}}$',
                  '$c_{\mathrm{PSS\$T1SS}}$',
                  '$c_{\mathrm{PSST2}}$',
                  '$c_{\mathrm{T2SH}}$',
                  '$c_{\mathrm{T2SS}}$',
                  '$c_{\mathrm{PSH\$T2SH}}$',
                  '$c_{\mathrm{PSH\$T2SS}}$',
                  '$c_{\mathrm{PSOH\$T2SH}}$',
                  '$c_{\mathrm{PSOH\$T2SS}}$',
                  '$c_{\mathrm{PSO2H\$T2SH}}$',
                  '$c_{\mathrm{PSO2H\$T2SS}}$',
                  '$c_{\mathrm{PSS\$T2SH}}$',
                  '$c_{\mathrm{PSS\$T2SS}}$',
                  '$c_{\mathrm{PbSH}}$',
                  '$c_{\mathrm{PbSS}}$'
                  ]
    
    # Put axes into list in correct order. First 10 concs are perox
    # then 3 T1 concs, then 2 Trx concs, then remaining 8 T1 concs,
    # then 11 T2 concs and finally 2 Probe concs

    axes = np.concatenate((axes_perox.ravel()[:10],
                           axes_T1.ravel()[:3],
                           axes_Trx.ravel(),
                           axes_T1.ravel()[3:11],
                           axes_T2.ravel()[:11],
                           axes_Pb.ravel()))

    # Plot bulk concentrations (initial, final, title, use scientific axis formatting)
    
    for conc_init, conc, ax, title in zip(modelsetup.c_bulk_init.T,
                                          c_bulk.T,
                                          axes,
                                          title_list):
        
        ax.plot(modelsetup.x,conc_init, label = 'Initial Concentration')
        ax.plot(modelsetup.x,conc, label = 'Final Concentration')
        ax.set_title(title)
        ax.ticklabel_format(axis='x', style='sci', scilimits=(0,0))

    # Get last axis legend info as it is the same for all

    handles, labels = ax.get_legend_handles_labels()

    # Loop over figures adding legends

    for fig in [fig_perox, fig_T1, fig_Trx, fig_T2, fig_Pb]:

        fig.legend(handles, labels, loc='lower center')
    
    # Plot membrane concentrations using text

    # Initialise m1 figure and axes
    
    fig_m1, axes_m1 = plt.subplots(1,3)

    # Initialise m2 figure and axes
    
    fig_m2, axes_m2 = plt.subplots(1,3)

    # Loop over figures doing formatting

    for fig in [fig_m1, fig_m2]:

        # Nicely space figures leaving room for title
    
        fig.tight_layout(pad=3.25, h_pad=None, w_pad=None)

    # Remove axis ticks and labels from axes
    
    for ax in np.concatenate((axes_m1.ravel(), axes_m2.ravel())):
        ax.axis('off')
    
    # List of membrane concentrations for m1 and m2
    
    m1_conc_list = ['$c_{\mathrm{PSH}}^{m_1} = $',
                    '$c_{\mathrm{PSOH}}^{m_1} = $',
                    '$c_{\mathrm{PSO2H}}^{m_1} = $',
                    '$c_{\mathrm{PSS}}^{m_1} = $',
                    '$c_{\mathrm{PSST1}}^{m_1} = $',
                    '$c_{\mathrm{PSH\$T1SH}}^{m_1} = $',
                    '$c_{\mathrm{PSH\$T1SS}}^{m_1} = $',
                    '$c_{\mathrm{PSOH\$T1SH}}^{m_1} = $',
                    '$c_{\mathrm{PSOH\$T1SS}}^{m_1} = $',
                    '$c_{\mathrm{PSO2H\$T1SH}}^{m_1} = $',
                    '$c_{\mathrm{PSO2H\$T1SS}}^{m_1} = $',
                    '$c_{\mathrm{PSS\$T1SH}}^{m_1} = $',
                    '$c_{\mathrm{PSS\$T1SS}}^{m_1} = $',
                    '$c_{\mathrm{PSST2}}^{m_1} = $',
                    '$c_{\mathrm{PSH\$T2SH}}^{m_1} = $',
                    '$c_{\mathrm{PSH\$T2SS}}^{m_1} = $',
                    '$c_{\mathrm{PSOH\$T2SH}}^{m_1} = $',
                    '$c_{\mathrm{PSOH\$T2SS}}^{m_1} = $',
                    '$c_{\mathrm{PSO2H\$T2SH}}^{m_1} = $',
                    '$c_{\mathrm{PSO2H\$T2SS}}^{m_1} = $',
                    '$c_{\mathrm{PSS\$T2SH}}^{m_1} = $',
                    '$c_{\mathrm{PSS\$T2SS}}^{m_1} = $',
                    '$c_{\mathrm{PbSH}}^{m1} = $',
                    '$c_{\mathrm{PbSS}}^{m1} = $'
                    ]
                          
    m2_conc_list = ['$c_{\mathrm{PSH}}^{m_2} = $',
                    '$c_{\mathrm{PSOH}}^{m_2} = $',
                    '$c_{\mathrm{PSO2H}}^{m_2} = $',
                    '$c_{\mathrm{PSS}}^{m_2} = $',
                    '$c_{\mathrm{PSST1}}^{m_2} = $',
                    '$c_{\mathrm{PSH\$T1SH}}^{m_2} = $',
                    '$c_{\mathrm{PSH\$T1SS}}^{m_2} = $',
                    '$c_{\mathrm{PSOH\$T1SH}}^{m_2} = $',
                    '$c_{\mathrm{PSOH\$T1SS}}^{m_2} = $',
                    '$c_{\mathrm{PSO2H\$T1SH}}^{m_2} = $',
                    '$c_{\mathrm{PSO2H\$T1SS}}^{m_2} = $',
                    '$c_{\mathrm{PSS\$T1SH}}^{m_2} = $',
                    '$c_{\mathrm{PSS\$T1SS}}^{m_2} = $',
                    '$c_{\mathrm{PSST2}}^{m_2} = $',
                    '$c_{\mathrm{PSH\$T2SH}}^{m_2} = $',
                    '$c_{\mathrm{PSH\$T2SS}}^{m_2} = $',
                    '$c_{\mathrm{PSOH\$T2SH}}^{m_2} = $',
                    '$c_{\mathrm{PSOH\$T2SS}}^{m_2} = $',
                    '$c_{\mathrm{PSO2H\$T2SH}}^{m_2} = $',
                    '$c_{\mathrm{PSO2H\$T2SS}}^{m_2} = $',
                    '$c_{\mathrm{PSS\$T2SH}}^{m_2} = $',
                    '$c_{\mathrm{PSS\$T2SS}}^{m_2} = $',
                    '$c_{\mathrm{PbSH}}^{m2} = $',
                    '$c_{\mathrm{PbSS}}^{m2} = $'
                    ]

    # Define y coordinate for text position in figure
    # First figure will have 4 Prx membrane concs
    # Second figure will have 9 T1 membrane concs
    # Third figure will have 9 T2 membrane concs
    # Then we add to first figure with 2 Pb membrane concs
    # Then stick them all together

    first_fig_y_coords = np.arange(1.0, 0.69,step=-0.1) # 4 points

    second_fig_y_coords = np.arange(1.0, 0.19,step=-0.1) # 9 points

    third_fig_y_coords = second_fig_y_coords # 9 points

    first_fig_Pb_y_coords = np.arange(0.5, 0.39,step=-0.1) # 2 points

    y_coords = np.concatenate((first_fig_y_coords, second_fig_y_coords, third_fig_y_coords, first_fig_Pb_y_coords))

    # Loop over m1 and m2 with different set of axes and conc_lists for each

    for axes, membrane_conc_list in zip([axes_m1, axes_m2], [m1_conc_list, m2_conc_list]):

        # Loop over membrane concs for each membrane, writing them at y_coord
    
        for ii, [text, y_coord, conc] in enumerate(zip(membrane_conc_list, y_coords, c_m)):

            # For first 4 concs and last 2 concs plot in first axis

            if ii in [0, 1, 2, 3, 22, 23]:

                axes.ravel()[0].text(0.5, y_coord, text + 
                                     str('{:.4e}'.format(conc)),
                                     horizontalalignment='center'
                                     )
        
            # For next 9 concs plot in second axis

            elif ii < 13:

                axes.ravel()[1].text(0.5, y_coord, text + 
                                     str('{:.4e}'.format(conc)),
                                     horizontalalignment='center'
                                     )

            # For last 9 concs plot in third axis

            else:

                axes.ravel()[2].text(0.5, y_coord, text + 
                                     str('{:.4e}'.format(conc)),
                                     horizontalalignment='center'
                                     )

    # Show plots
    
    plt.show()

###############################################################################

############################# SIMULATION FUNCTION #############################


def hpscdbml(parameters = Parameters(), modelsetup = ModelSetup(Parameters()),
             analysis = Analysis(), t_init = 0.0, t_end = 86400.0,
             max_step = np.inf, saving = False, output_dir = 'output/test'):
    '''Solve numerically the complete system which builds on the
    equations described in Eqns (3) of Travasso et al. (2017). The PDEs
    are discretised spatially and the resulting ODE system is solved
    using the built-in LSODA method in scipy.
    
    Optional keyword arguments:
    parameters is the class containing all parameters to use for the simulation. Default as in init method.
    modelsetup is the class containing all computed quantities related to model setup. Default as in init method.
    analysis   is the class containing all computed quantities for analysing the model. Default as in init method.
    t_init     is the start time of the simulation. Default is 0.0.
    t_end      is the end time of the simulation. Default is 86400.0 (24 hrs).
    max_step   is the maximum time step to be used in the LSODA solver. Default is inf.
    saving     is the True/False switch to save output of run to csv file. Default is False.
    output_dir is the directory in which output will be saved. Default is 'output/test'
    '''
    ######################### Initial setup ##########################
    
    # Timer
    
    start = timer()
    
    # Calculate intermediate constants
    
    r_1 = parameters.D_1/parameters.dx**2
    r_2 = parameters.D_2/parameters.dx**2
    r_3 = parameters.D_3/parameters.dx**2
    r_4 = parameters.D_4/parameters.dx**2
    r_5 = parameters.D_5/parameters.dx**2
    r_6 = parameters.D_6/parameters.dx**2
    r_Pb = parameters.D_Pb/parameters.dx**2
    
    # Reset Analysis variables
    
    analysis.reset()
    
    # Initial condition
    
    # Reshape initial concentration array  to a vector by stacking all
    # columns (hence the transpose) and then appending membrane concs
    
    Y_0 = np.append(modelsetup.c_bulk_init.T.reshape(modelsetup.c_bulk_init.size), modelsetup.c_m_init)
    
    # Initial mass - integrate along each row to give a value for each
    # species and then sum all as well as membrane concs
    
    mass_init = (np.sum(simps(modelsetup.c_bulk_init,modelsetup.x,axis=0)) + np.sum(modelsetup.c_m_init))  
                                                        
    
    ##################### Build Diffusion Operator #####################
    
    # Initialise diagonals
    
    L_diag = np.zeros(modelsetup.n_bulk_species*modelsetup.Nx + modelsetup.n_memb_species)
    
    L_updiag = np.zeros(modelsetup.n_bulk_species*modelsetup.Nx + modelsetup.n_memb_species - 1)
    
    L_lowdiag = np.zeros(modelsetup.n_bulk_species*modelsetup.Nx + modelsetup.n_memb_species - 1)

    # List of r_values

    r_values = [r_1,  # c_H
                r_2,  # c_P1SH
                r_2,  # c_P1SOH
                r_2,  # c_P1SO2H
                r_3,  # c_P1SS
                r_3,  # c_P1SHa
                r_2,  # c_PSH
                r_2,  # c_PSOH
                r_2,  # c_PSO2H
                r_3,  # c_PSS
                r_4,  # c_PSST1
                r_3,  # c_T1SH
                r_3,  # c_T1SS
                r_5,  # c_TrxSH
                r_5,  # c_TrxSS
                r_4,  # c_PSH_T1SH
                r_4,  # c_PSH_T1SS
                r_4,  # c_PSOH_T1SH
                r_4,  # c_PSOH_T1SS
                r_4,  # c_PSO2H_T1SH
                r_4,  # c_PSO2H_T1SS
                r_6,  # c_PSS_T1SH
                r_6,  # c_PSS_T1SS
                r_4,  # c_PSST2
                r_3,  # c_T2SH
                r_3,  # c_T2SS
                r_4,  # c_PSH_T2SH
                r_4,  # c_PSH_T2SS
                r_4,  # c_PSOH_T2SH
                r_4,  # c_PSOH_T2SS
                r_4,  # c_PSO2H_T2SH
                r_4,  # c_PSO2H_T2SS
                r_6,  # c_PSS_T2SH
                r_6,  # c_PSS_T2SS
                r_Pb, # c_PbSH
                r_Pb  # c_PbSS
                ]

    # Fill in diagonals

    for ii, r_value in enumerate(r_values):

        L_diag[ii*modelsetup.Nx:(ii + 1)*modelsetup.Nx] = -2*r_value*np.ones(modelsetup.Nx)

        L_updiag[ii*modelsetup.Nx:((ii + 1)*modelsetup.Nx - 1)] = r_value*np.append(2, np.ones(modelsetup.Nx - 2))

        L_lowdiag[ii*modelsetup.Nx:((ii + 1)*modelsetup.Nx - 1)] = r_value*np.append(np.ones(modelsetup.Nx - 2), 2)
    

    # Construct matrix
    
    L = diags([L_lowdiag, L_diag, L_updiag],[-1,0,1])
    
    ########################## Build ODE RHS ###########################

    def f(t, Y):
        
        # Extract variables from input
        
        c_H = Y[:modelsetup.Nx]
        c_P1SH = Y[modelsetup.Nx:2*modelsetup.Nx]
        c_P1SOH = Y[2*modelsetup.Nx:3*modelsetup.Nx]
        c_P1SO2H = Y[3*modelsetup.Nx:4*modelsetup.Nx]
        c_P1SS = Y[4*modelsetup.Nx:5*modelsetup.Nx]
        c_P1SHa = Y[5*modelsetup.Nx:6*modelsetup.Nx]
        c_PSH = Y[6*modelsetup.Nx:7*modelsetup.Nx]
        c_PSOH = Y[7*modelsetup.Nx:8*modelsetup.Nx]
        c_PSO2H = Y[8*modelsetup.Nx:9*modelsetup.Nx]
        c_PSS = Y[9*modelsetup.Nx:10*modelsetup.Nx]
        c_PSST1 = Y[10*modelsetup.Nx:11*modelsetup.Nx]
        c_T1SH = Y[11*modelsetup.Nx:12*modelsetup.Nx]
        c_T1SS = Y[12*modelsetup.Nx:13*modelsetup.Nx]
        c_TrxSH = Y[13*modelsetup.Nx:14*modelsetup.Nx]
        c_TrxSS = Y[14*modelsetup.Nx:15*modelsetup.Nx]
        c_PSH_T1SH = Y[15*modelsetup.Nx:16*modelsetup.Nx]
        c_PSH_T1SS = Y[16*modelsetup.Nx:17*modelsetup.Nx]
        c_PSOH_T1SH = Y[17*modelsetup.Nx:18*modelsetup.Nx]
        c_PSOH_T1SS = Y[18*modelsetup.Nx:19*modelsetup.Nx]
        c_PSO2H_T1SH = Y[19*modelsetup.Nx:20*modelsetup.Nx]
        c_PSO2H_T1SS = Y[20*modelsetup.Nx:21*modelsetup.Nx]
        c_PSS_T1SH = Y[21*modelsetup.Nx:22*modelsetup.Nx]
        c_PSS_T1SS = Y[22*modelsetup.Nx:23*modelsetup.Nx]
        c_PSST2 = Y[23*modelsetup.Nx:24*modelsetup.Nx]
        c_T2SH = Y[24*modelsetup.Nx:25*modelsetup.Nx]
        c_T2SS = Y[25*modelsetup.Nx:26*modelsetup.Nx]
        c_PSH_T2SH = Y[26*modelsetup.Nx:27*modelsetup.Nx]
        c_PSH_T2SS = Y[27*modelsetup.Nx:28*modelsetup.Nx]
        c_PSOH_T2SH = Y[28*modelsetup.Nx:29*modelsetup.Nx]
        c_PSOH_T2SS = Y[29*modelsetup.Nx:30*modelsetup.Nx]
        c_PSO2H_T2SH = Y[30*modelsetup.Nx:31*modelsetup.Nx]
        c_PSO2H_T2SS = Y[31*modelsetup.Nx:32*modelsetup.Nx]
        c_PSS_T2SH = Y[32*modelsetup.Nx:33*modelsetup.Nx]
        c_PSS_T2SS = Y[33*modelsetup.Nx:34*modelsetup.Nx]
        c_PbSH = Y[34*modelsetup.Nx:35*modelsetup.Nx]
        c_PbSS = Y[35*modelsetup.Nx:36*modelsetup.Nx]
        
        c_m1_PSH = Y[modelsetup.n_bulk_species*modelsetup.Nx]
        c_m1_PSOH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 1]
        c_m1_PSO2H = Y[modelsetup.n_bulk_species*modelsetup.Nx + 2]
        c_m1_PSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 3]
        c_m1_PSST1 = Y[modelsetup.n_bulk_species*modelsetup.Nx + 4]
        c_m1_PSH_T1SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 5]
        c_m1_PSH_T1SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 6]
        c_m1_PSOH_T1SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 7]
        c_m1_PSOH_T1SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 8]
        c_m1_PSO2H_T1SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 9]
        c_m1_PSO2H_T1SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 10]
        c_m1_PSS_T1SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 11]
        c_m1_PSS_T1SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 12]
        c_m1_PSST2 = Y[modelsetup.n_bulk_species*modelsetup.Nx + 13]
        c_m1_PSH_T2SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 14]
        c_m1_PSH_T2SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 15]
        c_m1_PSOH_T2SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 16]
        c_m1_PSOH_T2SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 17]
        c_m1_PSO2H_T2SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 18]
        c_m1_PSO2H_T2SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 19]
        c_m1_PSS_T2SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 20]
        c_m1_PSS_T2SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 21]
        c_m1_PbSH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 22]
        c_m1_PbSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 23]

        c_m2_PSH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 24]
        c_m2_PSOH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 25]
        c_m2_PSO2H = Y[modelsetup.n_bulk_species*modelsetup.Nx + 26]
        c_m2_PSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 27]
        c_m2_PSST1 = Y[modelsetup.n_bulk_species*modelsetup.Nx + 28]
        c_m2_PSH_T1SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 29]
        c_m2_PSH_T1SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 30]
        c_m2_PSOH_T1SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 31]
        c_m2_PSOH_T1SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 32]
        c_m2_PSO2H_T1SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 33]
        c_m2_PSO2H_T1SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 34]
        c_m2_PSS_T1SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 35]
        c_m2_PSS_T1SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 36]
        c_m2_PSST2 = Y[modelsetup.n_bulk_species*modelsetup.Nx + 37]
        c_m2_PSH_T2SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 38]
        c_m2_PSH_T2SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 39]
        c_m2_PSOH_T2SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 40]
        c_m2_PSOH_T2SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 41]
        c_m2_PSO2H_T2SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 42]
        c_m2_PSO2H_T2SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 43]
        c_m2_PSS_T2SH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 44]
        c_m2_PSS_T2SS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 45]
        c_m2_PbSH = Y[modelsetup.n_bulk_species*modelsetup.Nx + 46]
        c_m2_PbSS = Y[modelsetup.n_bulk_species*modelsetup.Nx + 47]

        # Extract parameters from input

        v_sup_m1 = parameters.v_sup_m1
        v_sup_m2 = parameters.v_sup_m2
        k_Alt = parameters.k_Alt
        k_aM = parameters.k_aM
        k_aMP_m1 = parameters.k_aMP_m1
        k_aMP_m2 = parameters.k_aMP_m2
        k_aMC_T1_m1 = parameters.k_aMC_T1_m1
        k_aMC_T2_m1 = parameters.k_aMC_T2_m1
        k_aMC_T1_m2 = parameters.k_aMC_T1_m2
        k_aMC_T2_m2 = parameters.k_aMC_T2_m2
        k_aS = parameters.k_aS
        k_C_P2 = parameters.k_C_P2
        k_C_P1 = parameters.k_C_P1
        k_I = parameters.k_I
        K_DT1SH = parameters.K_DT1SH
        K_DT1SS = parameters.K_DT1SS
        K_DT2SH = parameters.K_DT2SH
        K_DT2SS = parameters.K_DT2SS
        k_HC_T1 = parameters.k_HC_T1
        k_HC_T2 = parameters.k_HC_T2
        k_R_P2 = parameters.k_R_P2
        k_R_P1 = parameters.k_R_P1
        k_Se = parameters.k_Se
        k_Si_P2 = parameters.k_Si_P2
        k_Si_P1 = parameters.k_Si_P1
        k_Sr = parameters.k_Sr
        k_TDE = parameters.k_TDE
        K_TDE = parameters.K_TDE
        k_ITDE = parameters.k_ITDE
        K_ITDE = parameters.K_ITDE
        k_TR = parameters.k_TR
        k_TSSR = parameters.k_TSSR
        k_PbOx = parameters.k_PbOx
        k_PbR = parameters.k_PbR
        k_MPbOx = parameters.k_MPbOx
        k_MPbR = parameters.k_MPbR
        r_DM = parameters.r_DM
        V_MaxTrxR = parameters.V_MaxTrxR
        K_MTrxR = parameters.K_MTrxR
        r_VS = parameters.r_VS
        
        # Diffusion
        
        dYdt = L*Y
        
        # Reaction terms
        
        # c_H
        dYdt[:modelsetup.Nx] += -(c_H*k_Alt) - c_H*k_Se*c_P1SH - c_H*k_Si_P1*c_P1SOH - c_H*k_PbOx*c_PbSH - c_H*k_Se*c_PSH - c_H*k_Se*c_PSH_T1SH - c_H*k_Se*c_PSH_T1SS - c_H*k_Si_P2*c_PSOH - c_H*k_Si_P2*c_PSOH_T1SH - c_H*k_Si_P2*c_PSOH_T1SS \
                                - c_H*k_Se*c_PSH_T2SH - c_H*k_Se*c_PSH_T2SS - c_H*k_Si_P2*c_PSOH_T2SH - c_H*k_Si_P2*c_PSOH_T2SS
        # c_P1SH
        dYdt[modelsetup.Nx:2*modelsetup.Nx] += -(c_H*k_Se*c_P1SH) + k_I*c_P1SHa
        # c_P1SOH
        dYdt[2*modelsetup.Nx:3*modelsetup.Nx] += c_H*k_Se*c_P1SH + k_Sr*c_P1SO2H - k_C_P1*c_P1SOH - c_H*k_Si_P1*c_P1SOH
        # c_P1SO2H
        dYdt[3*modelsetup.Nx:4*modelsetup.Nx] += -(k_Sr*c_P1SO2H) + c_H*k_Si_P1*c_P1SOH
        # c_P1SS
        dYdt[4*modelsetup.Nx:5*modelsetup.Nx] += k_C_P1*c_P1SOH - k_R_P1*c_P1SS*c_TrxSH
        # c_P1SHa
        dYdt[5*modelsetup.Nx:6*modelsetup.Nx] += -(k_I*c_P1SHa) + k_R_P1*c_P1SS*c_TrxSH
        # c_PSH
        dYdt[6*modelsetup.Nx:7*modelsetup.Nx] += -(c_H*k_Se*c_PSH) + k_aS*K_DT1SH*c_PSH_T1SH + k_aS*K_DT1SS*c_PSH_T1SS + k_R_P2*c_PSS*c_TrxSH - k_aS*c_PSH*c_T1SH - k_aS*c_PSH*c_T1SS \
                                                 + k_aS*K_DT2SH*c_PSH_T2SH + k_aS*K_DT2SS*c_PSH_T2SS - k_aS*c_PSH*c_T2SH - k_aS*c_PSH*c_T2SS
        # c_PSOH
        dYdt[7*modelsetup.Nx:8*modelsetup.Nx] += c_H*k_Se*c_PSH + k_Sr*c_PSO2H - k_C_P2*c_PSOH - c_H*k_Si_P2*c_PSOH + k_aS*K_DT1SH*c_PSOH_T1SH + k_aS*K_DT1SS*c_PSOH_T1SS - k_aS*c_PSOH*c_T1SH - k_aS*c_PSOH*c_T1SS \
                                                 + k_aS*K_DT2SH*c_PSOH_T2SH + k_aS*K_DT2SS*c_PSOH_T2SS - k_aS*c_PSOH*c_T2SH - k_aS*c_PSOH*c_T2SS
        # c_PSO2H
        dYdt[8*modelsetup.Nx:9*modelsetup.Nx] += -(k_Sr*c_PSO2H) + k_aS*K_DT1SH*c_PSO2H_T1SH + k_aS*K_DT1SS*c_PSO2H_T1SS + c_H*k_Si_P2*c_PSOH - k_aS*c_PSO2H*c_T1SH - k_aS*c_PSO2H*c_T1SS \
                                                 + k_aS*K_DT2SH*c_PSO2H_T2SH + k_aS*K_DT2SS*c_PSO2H_T2SS - k_aS*c_PSO2H*c_T2SH - k_aS*c_PSO2H*c_T2SS
        # c_PSS
        dYdt[9*modelsetup.Nx:10*modelsetup.Nx] += k_C_P2*c_PSOH + k_aS*K_DT1SH*c_PSS_T1SH + k_aS*K_DT1SS*c_PSS_T1SS - k_R_P2*c_PSS*c_TrxSH - k_aS*c_PSS*c_T1SH - k_aS*c_PSS*c_T1SS \
                                                  + k_aS*K_DT2SH*c_PSS_T2SH + k_aS*K_DT2SS*c_PSS_T2SS - k_aS*c_PSS*c_T2SH - k_aS*c_PSS*c_T2SS
        # c_PSST1
        dYdt[10*modelsetup.Nx:11*modelsetup.Nx] += (k_TDE*c_PSH_T1SS)/K_TDE + k_HC_T1*c_PSOH_T1SH - (k_ITDE*c_PSST1)/K_ITDE - k_TDE*c_PSST1 + k_ITDE*c_PSS_T1SH
        # c_T1SH
        dYdt[11*modelsetup.Nx:12*modelsetup.Nx] += k_aS*K_DT1SH*c_PSH_T1SH + k_aS*K_DT1SH*c_PSO2H_T1SH + k_aS*K_DT1SH*c_PSOH_T1SH + k_aS*K_DT1SH*c_PSS_T1SH - k_aS*c_PSH*c_T1SH - k_aS*c_PSO2H*c_T1SH - k_aS*c_PSOH*c_T1SH - k_aS*c_PSS*c_T1SH + k_TSSR*c_T1SS
        # c_T1SS
        dYdt[12*modelsetup.Nx:13*modelsetup.Nx] += k_aS*K_DT1SS*c_PSH_T1SS + k_aS*K_DT1SS*c_PSO2H_T1SS + k_aS*K_DT1SS*c_PSOH_T1SS + k_aS*K_DT1SS*c_PSS_T1SS - k_TSSR*c_T1SS - k_aS*c_PSH*c_T1SS - k_aS*c_PSO2H*c_T1SS - k_aS*c_PSOH*c_T1SS - k_aS*c_PSS*c_T1SS
        # c_TrxSH
        dYdt[13*modelsetup.Nx:14*modelsetup.Nx] += -(k_R_P1*c_P1SS*c_TrxSH) - k_PbR*c_PbSS*c_TrxSH - k_R_P2*c_PSS*c_TrxSH + (c_TrxSS*V_MaxTrxR)/(K_MTrxR + c_TrxSS)
        # c_TrxSS
        dYdt[14*modelsetup.Nx:15*modelsetup.Nx] += k_R_P1*c_P1SS*c_TrxSH + k_PbR*c_PbSS*c_TrxSH + k_R_P2*c_PSS*c_TrxSH - (c_TrxSS*V_MaxTrxR)/(K_MTrxR + c_TrxSS)
        # c_PSH_T1SH
        dYdt[15*modelsetup.Nx:16*modelsetup.Nx] += -(k_aS*K_DT1SH*c_PSH_T1SH) - c_H*k_Se*c_PSH_T1SH + k_aS*c_PSH*c_T1SH
        # c_PSH_T1SS
        dYdt[16*modelsetup.Nx:17*modelsetup.Nx] += -(k_aS*K_DT1SS*c_PSH_T1SS) - c_H*k_Se*c_PSH_T1SS - (k_TDE*c_PSH_T1SS)/K_TDE + k_TDE*c_PSST1 + k_aS*c_PSH*c_T1SS
        # c_PSOH_T1SH
        dYdt[17*modelsetup.Nx:18*modelsetup.Nx] += c_H*k_Se*c_PSH_T1SH + k_Sr*c_PSO2H_T1SH - k_C_P2*c_PSOH_T1SH - k_aS*K_DT1SH*c_PSOH_T1SH - k_HC_T1*c_PSOH_T1SH - c_H*k_Si_P2*c_PSOH_T1SH + k_aS*c_PSOH*c_T1SH
        # c_PSOH_T1SS
        dYdt[18*modelsetup.Nx:19*modelsetup.Nx] += c_H*k_Se*c_PSH_T1SS + k_Sr*c_PSO2H_T1SS - k_C_P2*c_PSOH_T1SS - k_aS*K_DT1SS*c_PSOH_T1SS - c_H*k_Si_P2*c_PSOH_T1SS + k_aS*c_PSOH*c_T1SS
        # c_PSO2H_T1SH
        dYdt[19*modelsetup.Nx:20*modelsetup.Nx] += -(k_aS*K_DT1SH*c_PSO2H_T1SH) - k_Sr*c_PSO2H_T1SH + c_H*k_Si_P2*c_PSOH_T1SH + k_aS*c_PSO2H*c_T1SH
        # c_PSO2H_T1SS
        dYdt[20*modelsetup.Nx:21*modelsetup.Nx] += -(k_aS*K_DT1SS*c_PSO2H_T1SS) - k_Sr*c_PSO2H_T1SS + c_H*k_Si_P2*c_PSOH_T1SS + k_aS*c_PSO2H*c_T1SS
        # c_PSS_T1SH
        dYdt[21*modelsetup.Nx:22*modelsetup.Nx] += k_C_P2*c_PSOH_T1SH + (k_ITDE*c_PSST1)/K_ITDE - k_aS*K_DT1SH*c_PSS_T1SH - k_ITDE*c_PSS_T1SH + k_aS*c_PSS*c_T1SH
        # c_PSS_T1SS
        dYdt[22*modelsetup.Nx:23*modelsetup.Nx] += k_C_P2*c_PSOH_T1SS - k_aS*K_DT1SS*c_PSS_T1SS + k_aS*c_PSS*c_T1SS
        # c_PSST2
        dYdt[23*modelsetup.Nx:24*modelsetup.Nx] += (k_TDE*c_PSH_T2SS)/K_TDE + k_HC_T2*c_PSOH_T2SH - (k_ITDE*c_PSST2)/K_ITDE - k_TDE*c_PSST2 + k_ITDE*c_PSS_T2SH
        # c_T2SH
        dYdt[24*modelsetup.Nx:25*modelsetup.Nx] += k_aS*K_DT2SH*c_PSH_T2SH + k_aS*K_DT2SH*c_PSO2H_T2SH + k_aS*K_DT2SH*c_PSOH_T2SH + k_aS*K_DT2SH*c_PSS_T2SH - k_aS*c_PSH*c_T2SH - k_aS*c_PSO2H*c_T2SH - k_aS*c_PSOH*c_T2SH - k_aS*c_PSS*c_T2SH + k_TSSR*c_T2SS
        # c_T2SS
        dYdt[25*modelsetup.Nx:26*modelsetup.Nx] += k_aS*K_DT2SS*c_PSH_T2SS + k_aS*K_DT2SS*c_PSO2H_T2SS + k_aS*K_DT2SS*c_PSOH_T2SS + k_aS*K_DT2SS*c_PSS_T2SS - k_TSSR*c_T2SS - k_aS*c_PSH*c_T2SS - k_aS*c_PSO2H*c_T2SS - k_aS*c_PSOH*c_T2SS - k_aS*c_PSS*c_T2SS
        # c_PSH_T2SH
        dYdt[26*modelsetup.Nx:27*modelsetup.Nx] += -(k_aS*K_DT2SH*c_PSH_T2SH) - c_H*k_Se*c_PSH_T2SH + k_aS*c_PSH*c_T2SH
        # c_PSH_T2SS
        dYdt[27*modelsetup.Nx:28*modelsetup.Nx] += -(k_aS*K_DT2SS*c_PSH_T2SS) - c_H*k_Se*c_PSH_T2SS - (k_TDE*c_PSH_T2SS)/K_TDE + k_TDE*c_PSST2 + k_aS*c_PSH*c_T2SS
        # c_PSOH_T2SH
        dYdt[28*modelsetup.Nx:29*modelsetup.Nx] += c_H*k_Se*c_PSH_T2SH + k_Sr*c_PSO2H_T2SH - k_C_P2*c_PSOH_T2SH - k_aS*K_DT2SH*c_PSOH_T2SH - k_HC_T2*c_PSOH_T2SH - c_H*k_Si_P2*c_PSOH_T2SH + k_aS*c_PSOH*c_T2SH
        # c_PSOH_T2SS
        dYdt[29*modelsetup.Nx:30*modelsetup.Nx] += c_H*k_Se*c_PSH_T2SS + k_Sr*c_PSO2H_T2SS - k_C_P2*c_PSOH_T2SS - k_aS*K_DT2SS*c_PSOH_T2SS - c_H*k_Si_P2*c_PSOH_T2SS + k_aS*c_PSOH*c_T2SS
        # c_PSO2H_T2SH
        dYdt[30*modelsetup.Nx:31*modelsetup.Nx] += -(k_aS*K_DT2SH*c_PSO2H_T2SH) - k_Sr*c_PSO2H_T2SH + c_H*k_Si_P2*c_PSOH_T2SH + k_aS*c_PSO2H*c_T2SH
        # c_PSO2H_T2SS
        dYdt[31*modelsetup.Nx:32*modelsetup.Nx] += -(k_aS*K_DT2SS*c_PSO2H_T2SS) - k_Sr*c_PSO2H_T2SS + c_H*k_Si_P2*c_PSOH_T2SS + k_aS*c_PSO2H*c_T2SS
        # c_PSS_T2SH
        dYdt[32*modelsetup.Nx:33*modelsetup.Nx] += k_C_P2*c_PSOH_T2SH + (k_ITDE*c_PSST2)/K_ITDE - k_aS*K_DT2SH*c_PSS_T2SH - k_ITDE*c_PSS_T2SH + k_aS*c_PSS*c_T2SH
        # c_PSS_T2SS
        dYdt[33*modelsetup.Nx:34*modelsetup.Nx] += k_C_P2*c_PSOH_T2SS - k_aS*K_DT2SS*c_PSS_T2SS + k_aS*c_PSS*c_T2SS
        # c_PbSH
        dYdt[34*modelsetup.Nx:35*modelsetup.Nx] += -(c_H*k_PbOx*c_PbSH) + k_PbR*c_PbSS*c_TrxSH
        # c_PbSS
        dYdt[35*modelsetup.Nx:36*modelsetup.Nx] += c_H*k_PbOx*c_PbSH - k_PbR*c_PbSS*c_TrxSH
        
        # Left hand (membrane 1) boundary conditions

        # c_H
        # dYdt[0] += 2*( -(c_H[0]*k_Se*c_m1_PSH) - c_H[0]*k_Se*c_m1_PSH_T1SH - c_H[0]*k_Se*c_m1_PSH_T1SS - c_H[0]*k_Si_P2*c_m1_PSOH - c_H[0]*k_Si_P2*c_m1_PSOH_T1SH - c_H[0]*k_Si_P2*c_m1_PSOH_T1SS + r_VS*v_sup_m1*65.*np.exp(-5e-3*t) )/parameters.dx
        dYdt[0] += 2*( -(c_H[0]*k_MPbOx*c_m1_PbSH) - c_H[0]*k_Se*c_m1_PSH - c_H[0]*k_Se*c_m1_PSH_T1SH - c_H[0]*k_Se*c_m1_PSH_T1SS - c_H[0]*k_Si_P2*c_m1_PSOH - c_H[0]*k_Si_P2*c_m1_PSOH_T1SH - c_H[0]*k_Si_P2*c_m1_PSOH_T1SS - c_H[0]*k_Se*c_m1_PSH_T2SH - c_H[0]*k_Se*c_m1_PSH_T2SS - c_H[0]*k_Si_P2*c_m1_PSOH_T2SH - c_H[0]*k_Si_P2*c_m1_PSOH_T2SS + r_VS*v_sup_m1 )/parameters.dx
        # c_P1SH        
        dYdt[modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_P1SOH        
        dYdt[2*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_P1SO2H        
        dYdt[3*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_P1SS
        dYdt[4*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_P1SHa
        dYdt[5*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_PSH        
        dYdt[6*modelsetup.Nx] += 2*( k_aMP_m1*c_m1_PSH*r_DM - k_aMP_m1*c_PSH[0]*r_VS )/parameters.dx
        # c_PSOH        
        dYdt[7*modelsetup.Nx] += 2*( k_aMP_m1*c_m1_PSOH*r_DM - k_aMP_m1*c_PSOH[0]*r_VS )/parameters.dx
        # c_PSO2H        
        dYdt[8*modelsetup.Nx] += 2*( k_aMP_m1*c_m1_PSO2H*r_DM - k_aMP_m1*c_PSO2H[0]*r_VS )/parameters.dx
        # c_PSS        
        dYdt[9*modelsetup.Nx] += 2*( k_aMP_m1*c_m1_PSS*r_DM - k_aMP_m1*c_PSS[0]*r_VS )/parameters.dx
        # c_PSST1        
        dYdt[10*modelsetup.Nx] += 2*( k_aM*c_m1_PSST1*r_DM - k_aM*c_PSST1[0]*r_VS )/parameters.dx
        # c_T1SH        
        dYdt[11*modelsetup.Nx] += 2*( k_aMC_T1_m1*K_DT1SH*c_m1_PSH_T1SH + k_aMC_T1_m1*K_DT1SH*c_m1_PSO2H_T1SH + k_aMC_T1_m1*K_DT1SH*c_m1_PSOH_T1SH + k_aMC_T1_m1*K_DT1SH*c_m1_PSS_T1SH - k_aMC_T1_m1*c_m1_PSH*c_T1SH[0] - k_aMC_T1_m1*c_m1_PSO2H*c_T1SH[0] - k_aMC_T1_m1*c_m1_PSOH*c_T1SH[0] - k_aMC_T1_m1*c_m1_PSS*c_T1SH[0] )/parameters.dx
        # c_T1SS        
        dYdt[12*modelsetup.Nx] += 2*( k_aMC_T1_m1*K_DT1SS*c_m1_PSH_T1SS + k_aMC_T1_m1*K_DT1SS*c_m1_PSO2H_T1SS + k_aMC_T1_m1*K_DT1SS*c_m1_PSOH_T1SS + k_aMC_T1_m1*K_DT1SS*c_m1_PSS_T1SS - k_aMC_T1_m1*c_m1_PSH*c_T1SS[0] - k_aMC_T1_m1*c_m1_PSO2H*c_T1SS[0] - k_aMC_T1_m1*c_m1_PSOH*c_T1SS[0] - k_aMC_T1_m1*c_m1_PSS*c_T1SS[0] )/parameters.dx
        # c_TrxSH        
        dYdt[13*modelsetup.Nx] += 2*( -(k_MPbR*c_m1_PbSS*c_TrxSH[0]) - k_R_P2*c_m1_PSS*c_TrxSH[0] )/parameters.dx
        # c_TrxSS        
        dYdt[14*modelsetup.Nx] += 2*( k_MPbR*c_m1_PbSS*c_TrxSH[0] + k_R_P2*c_m1_PSS*c_TrxSH[0] )/parameters.dx
        # c_PSH_T1SH        
        dYdt[15*modelsetup.Nx] += 2*( k_aM*c_m1_PSH_T1SH*r_DM - k_aM*c_PSH_T1SH[0]*r_VS )/parameters.dx
        # c_PSH_T1SS        
        dYdt[16*modelsetup.Nx] += 2*( k_aM*c_m1_PSH_T1SS*r_DM - k_aM*c_PSH_T1SS[0]*r_VS )/parameters.dx
        # c_PSOH_T1SH        
        dYdt[17*modelsetup.Nx] += 2*( k_aM*c_m1_PSOH_T1SH*r_DM - k_aM*c_PSOH_T1SH[0]*r_VS )/parameters.dx
        # c_PSOH_T1SS        
        dYdt[18*modelsetup.Nx] += 2*( k_aM*c_m1_PSOH_T1SS*r_DM - k_aM*c_PSOH_T1SS[0]*r_VS )/parameters.dx
        # c_PSO2H_T1SH        
        dYdt[19*modelsetup.Nx] += 2*( k_aM*c_m1_PSO2H_T1SH*r_DM - k_aM*c_PSO2H_T1SH[0]*r_VS )/parameters.dx
        # c_PSO2H_T1SS        
        dYdt[20*modelsetup.Nx] += 2*( k_aM*c_m1_PSO2H_T1SS*r_DM - k_aM*c_PSO2H_T1SS[0]*r_VS )/parameters.dx
        # c_PSS_T1SH        
        dYdt[21*modelsetup.Nx] += 2*( k_aM*c_m1_PSS_T1SH*r_DM - k_aM*c_PSS_T1SH[0]*r_VS )/parameters.dx
        # c_PSS_T1SS        
        dYdt[22*modelsetup.Nx] += 2*( k_aM*c_m1_PSS_T1SS*r_DM - k_aM*c_PSS_T1SS[0]*r_VS )/parameters.dx
        # c_PSST2        
        dYdt[23*modelsetup.Nx] += 2*( k_aM*c_m1_PSST2*r_DM - k_aM*c_PSST2[0]*r_VS )/parameters.dx
        # c_T2SH        
        dYdt[24*modelsetup.Nx] += 2*( k_aMC_T2_m1*K_DT2SH*c_m1_PSH_T2SH + k_aMC_T2_m1*K_DT2SH*c_m1_PSO2H_T2SH + k_aMC_T2_m1*K_DT2SH*c_m1_PSOH_T2SH + k_aMC_T2_m1*K_DT2SH*c_m1_PSS_T2SH - k_aMC_T2_m1*c_m1_PSH*c_T2SH[0] - k_aMC_T2_m1*c_m1_PSO2H*c_T2SH[0] - k_aMC_T2_m1*c_m1_PSOH*c_T2SH[0] - k_aMC_T2_m1*c_m1_PSS*c_T2SH[0] )/parameters.dx
        # c_T2SS        
        dYdt[25*modelsetup.Nx] += 2*( k_aMC_T2_m1*K_DT2SS*c_m1_PSH_T2SS + k_aMC_T2_m1*K_DT2SS*c_m1_PSO2H_T2SS + k_aMC_T2_m1*K_DT2SS*c_m1_PSOH_T2SS + k_aMC_T2_m1*K_DT2SS*c_m1_PSS_T2SS - k_aMC_T2_m1*c_m1_PSH*c_T2SS[0] - k_aMC_T2_m1*c_m1_PSO2H*c_T2SS[0] - k_aMC_T2_m1*c_m1_PSOH*c_T2SS[0] - k_aMC_T2_m1*c_m1_PSS*c_T2SS[0] )/parameters.dx
        # c_PSH_T2SH        
        dYdt[26*modelsetup.Nx] += 2*( k_aM*c_m1_PSH_T2SH*r_DM - k_aM*c_PSH_T2SH[0]*r_VS )/parameters.dx
        # c_PSH_T2SS        
        dYdt[27*modelsetup.Nx] += 2*( k_aM*c_m1_PSH_T2SS*r_DM - k_aM*c_PSH_T2SS[0]*r_VS )/parameters.dx
        # c_PSOH_T2SH        
        dYdt[28*modelsetup.Nx] += 2*( k_aM*c_m1_PSOH_T2SH*r_DM - k_aM*c_PSOH_T2SH[0]*r_VS )/parameters.dx
        # c_PSOH_T2SS        
        dYdt[29*modelsetup.Nx] += 2*( k_aM*c_m1_PSOH_T2SS*r_DM - k_aM*c_PSOH_T2SS[0]*r_VS )/parameters.dx
        # c_PSO2H_T2SH        
        dYdt[30*modelsetup.Nx] += 2*( k_aM*c_m1_PSO2H_T2SH*r_DM - k_aM*c_PSO2H_T2SH[0]*r_VS )/parameters.dx
        # c_PSO2H_T2SS        
        dYdt[31*modelsetup.Nx] += 2*( k_aM*c_m1_PSO2H_T2SS*r_DM - k_aM*c_PSO2H_T2SS[0]*r_VS )/parameters.dx
        # c_PSS_T2SH        
        dYdt[32*modelsetup.Nx] += 2*( k_aM*c_m1_PSS_T2SH*r_DM - k_aM*c_PSS_T2SH[0]*r_VS )/parameters.dx
        # c_PSS_T2SS
        dYdt[33*modelsetup.Nx] += 2*( k_aM*c_m1_PSS_T2SS*r_DM - k_aM*c_PSS_T2SS[0]*r_VS )/parameters.dx
        # c_PbSH
        dYdt[34*modelsetup.Nx] += 2*( 0.0 )/parameters.dx
        # c_PbSS
        dYdt[35*modelsetup.Nx] += 2*( 0.0 )/parameters.dx

       # Right hand (membrane 2) boundary conditions

        # c_H        
        # dYdt[modelsetup.Nx-1] += 2*( -(c_H[-1]*k_Se*c_m2_PSH) - c_H[-1]*k_Se*c_m2_PSH_T1SH - c_H[-1]*k_Se*c_m2_PSH_T1SS - c_H[-1]*k_Si_P2*c_m2_PSOH - c_H[-1]*k_Si_P2*c_m2_PSOH_T1SH - c_H[-1]*k_Si_P2*c_m2_PSOH_T1SS + r_VS*v_sup_m2*65.*np.exp(-5e-3*t) )/parameters.dx
        dYdt[modelsetup.Nx-1] += 2*( -(c_H[-1]*k_MPbOx*c_m2_PbSH) - c_H[-1]*k_Se*c_m2_PSH - c_H[-1]*k_Se*c_m2_PSH_T1SH - c_H[-1]*k_Se*c_m2_PSH_T1SS - c_H[-1]*k_Si_P2*c_m2_PSOH - c_H[-1]*k_Si_P2*c_m2_PSOH_T1SH - c_H[-1]*k_Si_P2*c_m2_PSOH_T1SS - c_H[-1]*k_Se*c_m2_PSH_T2SH - c_H[-1]*k_Se*c_m2_PSH_T2SS - c_H[-1]*k_Si_P2*c_m2_PSOH_T2SH - c_H[-1]*k_Si_P2*c_m2_PSOH_T2SS + r_VS*v_sup_m2 )/parameters.dx
        # c_P1SH        
        dYdt[2*modelsetup.Nx-1] += 2*( 0.0 )/parameters.dx
        # c_P1SOH        
        dYdt[3*modelsetup.Nx-1] += 2*( 0.0 )/parameters.dx
        # c_P1SO2H        
        dYdt[4*modelsetup.Nx-1] += 2*( 0.0 )/parameters.dx
        # c_P1SS        
        dYdt[5*modelsetup.Nx-1] += 2*( 0.0 )/parameters.dx
        # c_P1SHa        
        dYdt[6*modelsetup.Nx-1] += 2*( 0.0 )/parameters.dx
        # c_PSH        
        dYdt[7*modelsetup.Nx-1] += 2*( k_aMP_m2*c_m2_PSH*r_DM - k_aMP_m2*c_PSH[-1]*r_VS )/parameters.dx
        # c_PSOH        
        dYdt[8*modelsetup.Nx-1] += 2*( k_aMP_m2*c_m2_PSOH*r_DM - k_aMP_m2*c_PSOH[-1]*r_VS )/parameters.dx
        # c_PSO2H        
        dYdt[9*modelsetup.Nx-1] += 2*( k_aMP_m2*c_m2_PSO2H*r_DM - k_aMP_m2*c_PSO2H[-1]*r_VS )/parameters.dx
        # c_PSS        
        dYdt[10*modelsetup.Nx-1] += 2*( k_aMP_m2*c_m2_PSS*r_DM - k_aMP_m2*c_PSS[-1]*r_VS )/parameters.dx
        # c_PSST1        
        dYdt[11*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSST1*r_DM - k_aM*c_PSST1[-1]*r_VS )/parameters.dx
        # c_T1SH        
        dYdt[12*modelsetup.Nx-1] += 2*( k_aMC_T1_m2*K_DT1SH*c_m2_PSH_T1SH + k_aMC_T1_m2*K_DT1SH*c_m2_PSO2H_T1SH + k_aMC_T1_m2*K_DT1SH*c_m2_PSOH_T1SH + k_aMC_T1_m2*K_DT1SH*c_m2_PSS_T1SH - k_aMC_T1_m2*c_m2_PSH*c_T1SH[-1] - k_aMC_T1_m2*c_m2_PSO2H*c_T1SH[-1] - k_aMC_T1_m2*c_m2_PSOH*c_T1SH[-1] - k_aMC_T1_m2*c_m2_PSS*c_T1SH[-1] )/parameters.dx
        # c_T1SS        
        dYdt[13*modelsetup.Nx-1] += 2*( k_aMC_T1_m2*K_DT1SS*c_m2_PSH_T1SS + k_aMC_T1_m2*K_DT1SS*c_m2_PSO2H_T1SS + k_aMC_T1_m2*K_DT1SS*c_m2_PSOH_T1SS + k_aMC_T1_m2*K_DT1SS*c_m2_PSS_T1SS - k_aMC_T1_m2*c_m2_PSH*c_T1SS[-1] - k_aMC_T1_m2*c_m2_PSO2H*c_T1SS[-1] - k_aMC_T1_m2*c_m2_PSOH*c_T1SS[-1] - k_aMC_T1_m2*c_m2_PSS*c_T1SS[-1] )/parameters.dx
        # c_TrxSH        
        dYdt[14*modelsetup.Nx-1] += 2*( -(k_MPbR*c_m2_PbSS*c_TrxSH[-1]) - k_R_P2*c_m2_PSS*c_TrxSH[-1] )/parameters.dx
        # c_TrxSS        
        dYdt[15*modelsetup.Nx-1] += 2*( k_MPbR*c_m2_PbSS*c_TrxSH[-1] + k_R_P2*c_m2_PSS*c_TrxSH[-1] )/parameters.dx
        # c_PSH_T1SH
        dYdt[16*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSH_T1SH*r_DM - k_aM*c_PSH_T1SH[-1]*r_VS )/parameters.dx
        # c_PSH_T1SS        
        dYdt[17*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSH_T1SS*r_DM - k_aM*c_PSH_T1SS[-1]*r_VS )/parameters.dx
        # c_PSOH_T1SH        
        dYdt[18*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSOH_T1SH*r_DM - k_aM*c_PSOH_T1SH[-1]*r_VS )/parameters.dx
        # c_PSOH_T1SS        
        dYdt[19*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSOH_T1SS*r_DM - k_aM*c_PSOH_T1SS[-1]*r_VS )/parameters.dx
        # c_PSO2H_T1SH        
        dYdt[20*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSO2H_T1SH*r_DM - k_aM*c_PSO2H_T1SH[-1]*r_VS )/parameters.dx
        # c_PSO2H_T1SS        
        dYdt[21*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSO2H_T1SS*r_DM - k_aM*c_PSO2H_T1SS[-1]*r_VS )/parameters.dx
        # c_PSS_T1SH        
        dYdt[22*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSS_T1SH*r_DM - k_aM*c_PSS_T1SH[-1]*r_VS )/parameters.dx
        # c_PSS_T1SS        
        dYdt[23*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSS_T1SS*r_DM - k_aM*c_PSS_T1SS[-1]*r_VS )/parameters.dx
        # c_PSST2        
        dYdt[24*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSST2*r_DM - k_aM*c_PSST2[-1]*r_VS )/parameters.dx
        # c_T2SH        
        dYdt[25*modelsetup.Nx-1] += 2*( k_aMC_T2_m2*K_DT2SH*c_m2_PSH_T2SH + k_aMC_T2_m2*K_DT2SH*c_m2_PSO2H_T2SH + k_aMC_T2_m2*K_DT2SH*c_m2_PSOH_T2SH + k_aMC_T2_m2*K_DT2SH*c_m2_PSS_T2SH - k_aMC_T2_m2*c_m2_PSH*c_T2SH[-1] - k_aMC_T2_m2*c_m2_PSO2H*c_T2SH[-1] - k_aMC_T2_m2*c_m2_PSOH*c_T2SH[-1] - k_aMC_T2_m2*c_m2_PSS*c_T2SH[-1] )/parameters.dx
        # c_T2SS        
        dYdt[26*modelsetup.Nx-1] += 2*( k_aMC_T2_m2*K_DT2SS*c_m2_PSH_T2SS + k_aMC_T2_m2*K_DT2SS*c_m2_PSO2H_T2SS + k_aMC_T2_m2*K_DT2SS*c_m2_PSOH_T2SS + k_aMC_T2_m2*K_DT2SS*c_m2_PSS_T2SS - k_aMC_T2_m2*c_m2_PSH*c_T2SS[-1] - k_aMC_T2_m2*c_m2_PSO2H*c_T2SS[-1] - k_aMC_T2_m2*c_m2_PSOH*c_T2SS[-1] - k_aMC_T2_m2*c_m2_PSS*c_T2SS[-1] )/parameters.dx
        # c_PSH_T2SH        
        dYdt[27*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSH_T2SH*r_DM - k_aM*c_PSH_T2SH[-1]*r_VS )/parameters.dx
        # c_PSH_T2SS        
        dYdt[28*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSH_T2SS*r_DM - k_aM*c_PSH_T2SS[-1]*r_VS )/parameters.dx
        # c_PSOH_T2SH        
        dYdt[29*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSOH_T2SH*r_DM - k_aM*c_PSOH_T2SH[-1]*r_VS )/parameters.dx
        # c_PSOH_T2SS        
        dYdt[30*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSOH_T2SS*r_DM - k_aM*c_PSOH_T2SS[-1]*r_VS )/parameters.dx
        # c_PSO2H_T2SH        
        dYdt[31*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSO2H_T2SH*r_DM - k_aM*c_PSO2H_T2SH[-1]*r_VS )/parameters.dx
        # c_PSO2H_T2SS        
        dYdt[32*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSO2H_T2SS*r_DM - k_aM*c_PSO2H_T2SS[-1]*r_VS )/parameters.dx
        # c_PSS_T2SH        
        dYdt[33*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSS_T2SH*r_DM - k_aM*c_PSS_T2SH[-1]*r_VS )/parameters.dx
        # c_PSS_T2SS
        dYdt[34*modelsetup.Nx-1] += 2*( k_aM*c_m2_PSS_T2SS*r_DM - k_aM*c_PSS_T2SS[-1]*r_VS )/parameters.dx
        # c_PbSH
        dYdt[35*modelsetup.Nx-1] += 2*( 0.0 )/parameters.dx
        # c_PbSS
        dYdt[36*modelsetup.Nx-1] += 2*( 0.0 )/parameters.dx

        # Left hand (membrane 1) ODEs

        # c_m1_PSH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx] += -(c_H[0]*k_Se*c_m1_PSH) + k_aMC_T1_m1*K_DT1SH*c_m1_PSH_T1SH + k_aMC_T1_m1*K_DT1SS*c_m1_PSH_T1SS - k_aMP_m1*c_m1_PSH*r_DM + k_aMP_m1*c_PSH[0]*r_VS + k_R_P2*c_m1_PSS*c_TrxSH[0] - k_aMC_T1_m1*c_m1_PSH*c_T1SH[0] - k_aMC_T1_m1*c_m1_PSH*c_T1SS[0] \
                                                         + k_aMC_T2_m1*K_DT2SH*c_m1_PSH_T2SH + k_aMC_T2_m1*K_DT2SS*c_m1_PSH_T2SS - k_aMC_T2_m1*c_m1_PSH*c_T2SH[0] - k_aMC_T2_m1*c_m1_PSH*c_T2SS[0]
        # c_m1_PSOH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 1] += c_H[0]*k_Se*c_m1_PSH + k_Sr*c_m1_PSO2H - k_C_P2*c_m1_PSOH - c_H[0]*k_Si_P2*c_m1_PSOH + k_aMC_T1_m1*K_DT1SH*c_m1_PSOH_T1SH + k_aMC_T1_m1*K_DT1SS*c_m1_PSOH_T1SS - k_aMP_m1*c_m1_PSOH*r_DM + k_aMP_m1*c_PSOH[0]*r_VS - k_aMC_T1_m1*c_m1_PSOH*c_T1SH[0] - k_aMC_T1_m1*c_m1_PSOH*c_T1SS[0] \
                                                             + k_aMC_T2_m1*K_DT2SH*c_m1_PSOH_T2SH + k_aMC_T2_m1*K_DT2SS*c_m1_PSOH_T2SS - k_aMC_T2_m1*c_m1_PSOH*c_T2SH[0] - k_aMC_T2_m1*c_m1_PSOH*c_T2SS[0]
        # c_m1_PSO2H        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 2] += -(k_Sr*c_m1_PSO2H) + k_aMC_T1_m1*K_DT1SH*c_m1_PSO2H_T1SH + k_aMC_T1_m1*K_DT1SS*c_m1_PSO2H_T1SS + c_H[0]*k_Si_P2*c_m1_PSOH - k_aMP_m1*c_m1_PSO2H*r_DM + k_aMP_m1*c_PSO2H[0]*r_VS - k_aMC_T1_m1*c_m1_PSO2H*c_T1SH[0] - k_aMC_T1_m1*c_m1_PSO2H*c_T1SS[0] \
                                                             + k_aMC_T2_m1*K_DT2SH*c_m1_PSO2H_T2SH + k_aMC_T2_m1*K_DT2SS*c_m1_PSO2H_T2SS - k_aMC_T2_m1*c_m1_PSO2H*c_T2SH[0] - k_aMC_T2_m1*c_m1_PSO2H*c_T2SS[0]
        # c_m1_PSS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 3] += k_C_P2*c_m1_PSOH + k_aMC_T1_m1*K_DT1SH*c_m1_PSS_T1SH + k_aMC_T1_m1*K_DT1SS*c_m1_PSS_T1SS - k_aMP_m1*c_m1_PSS*r_DM + k_aMP_m1*c_PSS[0]*r_VS - k_R_P2*c_m1_PSS*c_TrxSH[0] - k_aMC_T1_m1*c_m1_PSS*c_T1SH[0] - k_aMC_T1_m1*c_m1_PSS*c_T1SS[0] \
                                                             + k_aMC_T2_m1*K_DT2SH*c_m1_PSS_T2SH + k_aMC_T2_m1*K_DT2SS*c_m1_PSS_T2SS- k_aMC_T2_m1*c_m1_PSS*c_T2SH[0] - k_aMC_T2_m1*c_m1_PSS*c_T2SS[0]
        # c_m1_PSST1        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 4] += (k_TDE*c_m1_PSH_T1SS)/K_TDE + k_HC_T1*c_m1_PSOH_T1SH - (k_ITDE*c_m1_PSST1)/K_ITDE - k_TDE*c_m1_PSST1 + k_ITDE*c_m1_PSS_T1SH - k_aM*c_m1_PSST1*r_DM + k_aM*c_PSST1[0]*r_VS
        # c_m1_PSH_T1SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 5] += -(k_aMC_T1_m1*K_DT1SH*c_m1_PSH_T1SH) - c_H[0]*k_Se*c_m1_PSH_T1SH - k_aM*c_m1_PSH_T1SH*r_DM + k_aM*c_PSH_T1SH[0]*r_VS + k_aMC_T1_m1*c_m1_PSH*c_T1SH[0]
        # c_m1_PSH_T1SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 6] += -(k_aMC_T1_m1*K_DT1SS*c_m1_PSH_T1SS) - c_H[0]*k_Se*c_m1_PSH_T1SS - (k_TDE*c_m1_PSH_T1SS)/K_TDE + k_TDE*c_m1_PSST1 - k_aM*c_m1_PSH_T1SS*r_DM + k_aM*c_PSH_T1SS[0]*r_VS + k_aMC_T1_m1*c_m1_PSH*c_T1SS[0]
        # c_m1_PSOH_T1SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 7] += c_H[0]*k_Se*c_m1_PSH_T1SH + k_Sr*c_m1_PSO2H_T1SH - k_C_P2*c_m1_PSOH_T1SH - k_aMC_T1_m1*K_DT1SH*c_m1_PSOH_T1SH - k_HC_T1*c_m1_PSOH_T1SH - c_H[0]*k_Si_P2*c_m1_PSOH_T1SH - k_aM*c_m1_PSOH_T1SH*r_DM + k_aM*c_PSOH_T1SH[0]*r_VS + k_aMC_T1_m1*c_m1_PSOH*c_T1SH[0]
        # c_m1_PSOH_T1SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 8] += c_H[0]*k_Se*c_m1_PSH_T1SS + k_Sr*c_m1_PSO2H_T1SS - k_C_P2*c_m1_PSOH_T1SS - k_aMC_T1_m1*K_DT1SS*c_m1_PSOH_T1SS - c_H[0]*k_Si_P2*c_m1_PSOH_T1SS - k_aM*c_m1_PSOH_T1SS*r_DM + k_aM*c_PSOH_T1SS[0]*r_VS + k_aMC_T1_m1*c_m1_PSOH*c_T1SS[0]
        # c_m1_PSO2H_T1SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 9] += -(k_aMC_T1_m1*K_DT1SH*c_m1_PSO2H_T1SH) - k_Sr*c_m1_PSO2H_T1SH + c_H[0]*k_Si_P2*c_m1_PSOH_T1SH - k_aM*c_m1_PSO2H_T1SH*r_DM + k_aM*c_PSO2H_T1SH[0]*r_VS + k_aMC_T1_m1*c_m1_PSO2H*c_T1SH[0]
        # c_m1_PSO2H_T1SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 10] += -(k_aMC_T1_m1*K_DT1SS*c_m1_PSO2H_T1SS) - k_Sr*c_m1_PSO2H_T1SS + c_H[0]*k_Si_P2*c_m1_PSOH_T1SS - k_aM*c_m1_PSO2H_T1SS*r_DM + k_aM*c_PSO2H_T1SS[0]*r_VS + k_aMC_T1_m1*c_m1_PSO2H*c_T1SS[0]
        # c_m1_PSS_T1SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 11] += k_C_P2*c_m1_PSOH_T1SH + (k_ITDE*c_m1_PSST1)/K_ITDE - k_aMC_T1_m1*K_DT1SH*c_m1_PSS_T1SH - k_ITDE*c_m1_PSS_T1SH - k_aM*c_m1_PSS_T1SH*r_DM + k_aM*c_PSS_T1SH[0]*r_VS + k_aMC_T1_m1*c_m1_PSS*c_T1SH[0]
        # c_m1_PSS_T1SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 12] += k_C_P2*c_m1_PSOH_T1SS - k_aMC_T1_m1*K_DT1SS*c_m1_PSS_T1SS - k_aM*c_m1_PSS_T1SS*r_DM + k_aM*c_PSS_T1SS[0]*r_VS + k_aMC_T1_m1*c_m1_PSS*c_T1SS[0]
        # c_m1_PSST2        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 13] += (k_TDE*c_m1_PSH_T2SS)/K_TDE + k_HC_T2*c_m1_PSOH_T2SH - (k_ITDE*c_m1_PSST2)/K_ITDE - k_TDE*c_m1_PSST2 + k_ITDE*c_m1_PSS_T2SH - k_aM*c_m1_PSST2*r_DM + k_aM*c_PSST2[0]*r_VS
        # c_m1_PSH_T2SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 14] += -(k_aMC_T2_m1*K_DT2SH*c_m1_PSH_T2SH) - c_H[0]*k_Se*c_m1_PSH_T2SH - k_aM*c_m1_PSH_T2SH*r_DM + k_aM*c_PSH_T2SH[0]*r_VS + k_aMC_T2_m1*c_m1_PSH*c_T2SH[0]
        # c_m1_PSH_T2SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 15] += -(k_aMC_T2_m1*K_DT2SS*c_m1_PSH_T2SS) - c_H[0]*k_Se*c_m1_PSH_T2SS - (k_TDE*c_m1_PSH_T2SS)/K_TDE + k_TDE*c_m1_PSST2 - k_aM*c_m1_PSH_T2SS*r_DM + k_aM*c_PSH_T2SS[0]*r_VS + k_aMC_T2_m1*c_m1_PSH*c_T2SS[0]
        # c_m1_PSOH_T2SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 16] += c_H[0]*k_Se*c_m1_PSH_T2SH + k_Sr*c_m1_PSO2H_T2SH - k_C_P2*c_m1_PSOH_T2SH - k_aMC_T2_m1*K_DT2SH*c_m1_PSOH_T2SH - k_HC_T2*c_m1_PSOH_T2SH - c_H[0]*k_Si_P2*c_m1_PSOH_T2SH - k_aM*c_m1_PSOH_T2SH*r_DM + k_aM*c_PSOH_T2SH[0]*r_VS + k_aMC_T2_m1*c_m1_PSOH*c_T2SH[0]
        # c_m1_PSOH_T2SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 17] += c_H[0]*k_Se*c_m1_PSH_T2SS + k_Sr*c_m1_PSO2H_T2SS - k_C_P2*c_m1_PSOH_T2SS - k_aMC_T2_m1*K_DT2SS*c_m1_PSOH_T2SS - c_H[0]*k_Si_P2*c_m1_PSOH_T2SS - k_aM*c_m1_PSOH_T2SS*r_DM + k_aM*c_PSOH_T2SS[0]*r_VS + k_aMC_T2_m1*c_m1_PSOH*c_T2SS[0]
        # c_m1_PSO2H_T2SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 18] += -(k_aMC_T2_m1*K_DT2SH*c_m1_PSO2H_T2SH) - k_Sr*c_m1_PSO2H_T2SH + c_H[0]*k_Si_P2*c_m1_PSOH_T2SH - k_aM*c_m1_PSO2H_T2SH*r_DM + k_aM*c_PSO2H_T2SH[0]*r_VS + k_aMC_T2_m1*c_m1_PSO2H*c_T2SH[0]
        # c_m1_PSO2H_T2SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 19] += -(k_aMC_T2_m1*K_DT2SS*c_m1_PSO2H_T2SS) - k_Sr*c_m1_PSO2H_T2SS + c_H[0]*k_Si_P2*c_m1_PSOH_T2SS - k_aM*c_m1_PSO2H_T2SS*r_DM + k_aM*c_PSO2H_T2SS[0]*r_VS + k_aMC_T2_m1*c_m1_PSO2H*c_T2SS[0]
        # c_m1_PSS_T2SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 20] += k_C_P2*c_m1_PSOH_T2SH + (k_ITDE*c_m1_PSST2)/K_ITDE - k_aMC_T2_m1*K_DT2SH*c_m1_PSS_T2SH - k_ITDE*c_m1_PSS_T2SH - k_aM*c_m1_PSS_T2SH*r_DM + k_aM*c_PSS_T2SH[0]*r_VS + k_aMC_T2_m1*c_m1_PSS*c_T2SH[0]
        # c_m1_PSS_T2SS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 21] += k_C_P2*c_m1_PSOH_T2SS - k_aMC_T2_m1*K_DT2SS*c_m1_PSS_T2SS - k_aM*c_m1_PSS_T2SS*r_DM + k_aM*c_PSS_T2SS[0]*r_VS + k_aMC_T2_m1*c_m1_PSS*c_T2SS[0]
        # c_m1_PbSH
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 22] += -(c_H[0]*k_MPbOx*c_m1_PbSH) + k_MPbR*c_m1_PbSS*c_TrxSH[0]
        # c_m1_PbSS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 23] += c_H[0]*k_MPbOx*c_m1_PbSH - k_MPbR*c_m1_PbSS*c_TrxSH[0]

        # Right hand (membrane 2) ODEs

        # c_m2_PSH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 24] += -(c_H[-1]*k_Se*c_m2_PSH) + k_aMC_T1_m2*K_DT1SH*c_m2_PSH_T1SH + k_aMC_T1_m2*K_DT1SS*c_m2_PSH_T1SS - k_aMP_m2*c_m2_PSH*r_DM + k_aMP_m2*c_PSH[-1]*r_VS + k_R_P2*c_m2_PSS*c_TrxSH[-1] - k_aMC_T1_m2*c_m2_PSH*c_T1SH[-1] - k_aMC_T1_m2*c_m2_PSH*c_T1SS[-1] \
                                                              + k_aMC_T2_m2*K_DT2SH*c_m2_PSH_T2SH + k_aMC_T2_m2*K_DT2SS*c_m2_PSH_T2SS - k_aMC_T2_m2*c_m2_PSH*c_T2SH[-1] - k_aMC_T2_m2*c_m2_PSH*c_T2SS[-1]
        # c_m2_PSOH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 25] += c_H[-1]*k_Se*c_m2_PSH + k_Sr*c_m2_PSO2H - k_C_P2*c_m2_PSOH - c_H[-1]*k_Si_P2*c_m2_PSOH + k_aMC_T1_m2*K_DT1SH*c_m2_PSOH_T1SH + k_aMC_T1_m2*K_DT1SS*c_m2_PSOH_T1SS - k_aMP_m2*c_m2_PSOH*r_DM + k_aMP_m2*c_PSOH[-1]*r_VS - k_aMC_T1_m2*c_m2_PSOH*c_T1SH[-1] - k_aMC_T1_m2*c_m2_PSOH*c_T1SS[-1] \
                                                              + k_aMC_T2_m2*K_DT2SH*c_m2_PSOH_T2SH + k_aMC_T2_m2*K_DT2SS*c_m2_PSOH_T2SS - k_aMC_T2_m2*c_m2_PSOH*c_T2SH[-1] - k_aMC_T2_m2*c_m2_PSOH*c_T2SS[-1]
        # c_m2_PSO2H        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 26] += -(k_Sr*c_m2_PSO2H) + k_aMC_T1_m2*K_DT1SH*c_m2_PSO2H_T1SH + k_aMC_T1_m2*K_DT1SS*c_m2_PSO2H_T1SS + c_H[-1]*k_Si_P2*c_m2_PSOH - k_aMP_m2*c_m2_PSO2H*r_DM + k_aMP_m2*c_PSO2H[-1]*r_VS - k_aMC_T1_m2*c_m2_PSO2H*c_T1SH[-1] - k_aMC_T1_m2*c_m2_PSO2H*c_T1SS[-1] \
                                                              + k_aMC_T2_m2*K_DT2SH*c_m2_PSO2H_T2SH + k_aMC_T2_m2*K_DT2SS*c_m2_PSO2H_T2SS - k_aMC_T2_m2*c_m2_PSO2H*c_T2SH[-1] - k_aMC_T2_m2*c_m2_PSO2H*c_T2SS[-1]
        # c_m2_PSS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 27] += k_C_P2*c_m2_PSOH + k_aMC_T1_m2*K_DT1SH*c_m2_PSS_T1SH + k_aMC_T1_m2*K_DT1SS*c_m2_PSS_T1SS - k_aMP_m2*c_m2_PSS*r_DM + k_aMP_m2*c_PSS[-1]*r_VS - k_R_P2*c_m2_PSS*c_TrxSH[-1] - k_aMC_T1_m2*c_m2_PSS*c_T1SH[-1] - k_aMC_T1_m2*c_m2_PSS*c_T1SS[-1] \
                                                              + k_aMC_T2_m2*K_DT2SH*c_m2_PSS_T2SH + k_aMC_T2_m2*K_DT2SS*c_m2_PSS_T2SS - k_aMC_T2_m2*c_m2_PSS*c_T2SH[-1] - k_aMC_T2_m2*c_m2_PSS*c_T2SS[-1]
        # c_m2_PSST1        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 28] += (k_TDE*c_m2_PSH_T1SS)/K_TDE + k_HC_T1*c_m2_PSOH_T1SH - (k_ITDE*c_m2_PSST1)/K_ITDE - k_TDE*c_m2_PSST1 + k_ITDE*c_m2_PSS_T1SH - k_aM*c_m2_PSST1*r_DM + k_aM*c_PSST1[-1]*r_VS
        # c_m2_PSH_T1SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 29] += -(k_aMC_T1_m2*K_DT1SH*c_m2_PSH_T1SH) - c_H[-1]*k_Se*c_m2_PSH_T1SH - k_aM*c_m2_PSH_T1SH*r_DM + k_aM*c_PSH_T1SH[-1]*r_VS + k_aMC_T1_m2*c_m2_PSH*c_T1SH[-1]
        # c_m2_PSH_T1SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 30] += -(k_aMC_T1_m2*K_DT1SS*c_m2_PSH_T1SS) - c_H[-1]*k_Se*c_m2_PSH_T1SS - (k_TDE*c_m2_PSH_T1SS)/K_TDE + k_TDE*c_m2_PSST1 - k_aM*c_m2_PSH_T1SS*r_DM + k_aM*c_PSH_T1SS[-1]*r_VS + k_aMC_T1_m2*c_m2_PSH*c_T1SS[-1]
        # c_m2_PSOH_T1SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 31] += c_H[-1]*k_Se*c_m2_PSH_T1SH + k_Sr*c_m2_PSO2H_T1SH - k_C_P2*c_m2_PSOH_T1SH - k_aMC_T1_m2*K_DT1SH*c_m2_PSOH_T1SH - k_HC_T1*c_m2_PSOH_T1SH - c_H[-1]*k_Si_P2*c_m2_PSOH_T1SH - k_aM*c_m2_PSOH_T1SH*r_DM + k_aM*c_PSOH_T1SH[-1]*r_VS + k_aMC_T1_m2*c_m2_PSOH*c_T1SH[-1]
        # c_m2_PSOH_T1SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 32] += c_H[-1]*k_Se*c_m2_PSH_T1SS + k_Sr*c_m2_PSO2H_T1SS - k_C_P2*c_m2_PSOH_T1SS - k_aMC_T1_m2*K_DT1SS*c_m2_PSOH_T1SS - c_H[-1]*k_Si_P2*c_m2_PSOH_T1SS - k_aM*c_m2_PSOH_T1SS*r_DM + k_aM*c_PSOH_T1SS[-1]*r_VS + k_aMC_T1_m2*c_m2_PSOH*c_T1SS[-1]
        # c_m2_PSO2H_T1SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 33] += -(k_aMC_T1_m2*K_DT1SH*c_m2_PSO2H_T1SH) - k_Sr*c_m2_PSO2H_T1SH + c_H[-1]*k_Si_P2*c_m2_PSOH_T1SH - k_aM*c_m2_PSO2H_T1SH*r_DM + k_aM*c_PSO2H_T1SH[-1]*r_VS + k_aMC_T1_m2*c_m2_PSO2H*c_T1SH[-1]
        # c_m2_PSO2H_T1SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 34] += -(k_aMC_T1_m2*K_DT1SS*c_m2_PSO2H_T1SS) - k_Sr*c_m2_PSO2H_T1SS + c_H[-1]*k_Si_P2*c_m2_PSOH_T1SS - k_aM*c_m2_PSO2H_T1SS*r_DM + k_aM*c_PSO2H_T1SS[-1]*r_VS + k_aMC_T1_m2*c_m2_PSO2H*c_T1SS[-1]
        # c_m2_PSS_T1SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 35] += k_C_P2*c_m2_PSOH_T1SH + (k_ITDE*c_m2_PSST1)/K_ITDE - k_aMC_T1_m2*K_DT1SH*c_m2_PSS_T1SH - k_ITDE*c_m2_PSS_T1SH - k_aM*c_m2_PSS_T1SH*r_DM + k_aM*c_PSS_T1SH[-1]*r_VS + k_aMC_T1_m2*c_m2_PSS*c_T1SH[-1]
        # c_m2_PSS_T1SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 36] += k_C_P2*c_m2_PSOH_T1SS - k_aMC_T1_m2*K_DT1SS*c_m2_PSS_T1SS - k_aM*c_m2_PSS_T1SS*r_DM + k_aM*c_PSS_T1SS[-1]*r_VS + k_aMC_T1_m2*c_m2_PSS*c_T1SS[-1]
        # c_m2_PSST2        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 37] += (k_TDE*c_m2_PSH_T2SS)/K_TDE + k_HC_T2*c_m2_PSOH_T2SH - (k_ITDE*c_m2_PSST2)/K_ITDE - k_TDE*c_m2_PSST2 + k_ITDE*c_m2_PSS_T2SH - k_aM*c_m2_PSST2*r_DM + k_aM*c_PSST2[-1]*r_VS
        # c_m2_PSH_T2SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 38] += -(k_aMC_T2_m2*K_DT2SH*c_m2_PSH_T2SH) - c_H[-1]*k_Se*c_m2_PSH_T2SH - k_aM*c_m2_PSH_T2SH*r_DM + k_aM*c_PSH_T2SH[-1]*r_VS + k_aMC_T2_m2*c_m2_PSH*c_T2SH[-1]
        # c_m2_PSH_T2SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 39] += -(k_aMC_T2_m2*K_DT2SS*c_m2_PSH_T2SS) - c_H[-1]*k_Se*c_m2_PSH_T2SS - (k_TDE*c_m2_PSH_T2SS)/K_TDE + k_TDE*c_m2_PSST2 - k_aM*c_m2_PSH_T2SS*r_DM + k_aM*c_PSH_T2SS[-1]*r_VS + k_aMC_T2_m2*c_m2_PSH*c_T2SS[-1]
        # c_m2_PSOH_T2SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 40] += c_H[-1]*k_Se*c_m2_PSH_T2SH + k_Sr*c_m2_PSO2H_T2SH - k_C_P2*c_m2_PSOH_T2SH - k_aMC_T2_m2*K_DT2SH*c_m2_PSOH_T2SH - k_HC_T2*c_m2_PSOH_T2SH - c_H[-1]*k_Si_P2*c_m2_PSOH_T2SH - k_aM*c_m2_PSOH_T2SH*r_DM + k_aM*c_PSOH_T2SH[-1]*r_VS + k_aMC_T2_m2*c_m2_PSOH*c_T2SH[-1]
        # c_m2_PSOH_T2SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 41] += c_H[-1]*k_Se*c_m2_PSH_T2SS + k_Sr*c_m2_PSO2H_T2SS - k_C_P2*c_m2_PSOH_T2SS - k_aMC_T2_m2*K_DT2SS*c_m2_PSOH_T2SS - c_H[-1]*k_Si_P2*c_m2_PSOH_T2SS - k_aM*c_m2_PSOH_T2SS*r_DM + k_aM*c_PSOH_T2SS[-1]*r_VS + k_aMC_T2_m2*c_m2_PSOH*c_T2SS[-1]
        # c_m2_PSO2H_T2SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 42] += -(k_aMC_T2_m2*K_DT2SH*c_m2_PSO2H_T2SH) - k_Sr*c_m2_PSO2H_T2SH + c_H[-1]*k_Si_P2*c_m2_PSOH_T2SH - k_aM*c_m2_PSO2H_T2SH*r_DM + k_aM*c_PSO2H_T2SH[-1]*r_VS + k_aMC_T2_m2*c_m2_PSO2H*c_T2SH[-1]
        # c_m2_PSO2H_T2SS        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 43] += -(k_aMC_T2_m2*K_DT2SS*c_m2_PSO2H_T2SS) - k_Sr*c_m2_PSO2H_T2SS + c_H[-1]*k_Si_P2*c_m2_PSOH_T2SS - k_aM*c_m2_PSO2H_T2SS*r_DM + k_aM*c_PSO2H_T2SS[-1]*r_VS + k_aMC_T2_m2*c_m2_PSO2H*c_T2SS[-1]
        # c_m2_PSS_T2SH        
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 44] += k_C_P2*c_m2_PSOH_T2SH + (k_ITDE*c_m2_PSST2)/K_ITDE - k_aMC_T2_m2*K_DT2SH*c_m2_PSS_T2SH - k_ITDE*c_m2_PSS_T2SH - k_aM*c_m2_PSS_T2SH*r_DM + k_aM*c_PSS_T2SH[-1]*r_VS + k_aMC_T2_m2*c_m2_PSS*c_T2SH[-1]
        # c_m2_PSS_T2SS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 45] += k_C_P2*c_m2_PSOH_T2SS - k_aMC_T2_m2*K_DT2SS*c_m2_PSS_T2SS - k_aM*c_m2_PSS_T2SS*r_DM + k_aM*c_PSS_T2SS[-1]*r_VS + k_aMC_T2_m2*c_m2_PSS*c_T2SS[-1]
        # c_m2_PbSH
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 46] += -(c_H[-1]*k_MPbOx*c_m2_PbSH) + k_MPbR*c_m2_PbSS*c_TrxSH[-1]
        # c_m2_PbSS
        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + 47] += c_H[-1]*k_MPbOx*c_m2_PbSH - k_MPbR*c_m2_PbSS*c_TrxSH[-1]

        # Compute other quantities of interest
        
        analysis.T1SS_gen_domain += simps(k_aS*K_DT1SS*c_PSH_T1SS + k_aS*K_DT1SS*c_PSO2H_T1SS + k_aS*K_DT1SS*c_PSOH_T1SS + k_aS*K_DT1SS*c_PSS_T1SS, modelsetup.x)
        
        analysis.T1SS_gen_boundary += k_aMC_T1_m1*K_DT1SS*c_m1_PSH_T1SS + k_aMC_T1_m1*K_DT1SS*c_m1_PSO2H_T1SS + k_aMC_T1_m1*K_DT1SS*c_m1_PSOH_T1SS + k_aMC_T1_m1*K_DT1SS*c_m1_PSS_T1SS + k_aMC_T1_m2*K_DT1SS*c_m2_PSH_T1SS + k_aMC_T1_m2*K_DT1SS*c_m2_PSO2H_T1SS + k_aMC_T1_m2*K_DT1SS*c_m2_PSOH_T1SS + k_aMC_T1_m2*K_DT1SS*c_m2_PSS_T1SS

        analysis.T2SS_gen_domain += simps(k_aS*K_DT2SS*c_PSH_T2SS + k_aS*K_DT2SS*c_PSO2H_T2SS + k_aS*K_DT2SS*c_PSOH_T2SS + k_aS*K_DT2SS*c_PSS_T2SS, modelsetup.x)
        
        analysis.T2SS_gen_boundary += k_aMC_T2_m1*K_DT2SS*c_m1_PSH_T2SS + k_aMC_T2_m1*K_DT2SS*c_m1_PSO2H_T2SS + k_aMC_T2_m1*K_DT2SS*c_m1_PSOH_T2SS + k_aMC_T2_m1*K_DT2SS*c_m1_PSS_T2SS + k_aMC_T2_m2*K_DT2SS*c_m2_PSH_T2SS + k_aMC_T2_m2*K_DT2SS*c_m2_PSO2H_T2SS + k_aMC_T2_m2*K_DT2SS*c_m2_PSOH_T2SS + k_aMC_T2_m2*K_DT2SS*c_m2_PSS_T2SS

        return dYdt
    
    ############################# Solve ODE ############################
    
    # Set up ODE integrator
    
    solver = LSODA(f, t_init, Y_0, t_end, max_step=max_step)
    
    # Integrate ODE forward
    
    while solver.t < t_end:
        
        # Step solver forward
        
        solver.step()
        
    ########## Calculate diagnostics and do printing/plotting ##########
        
        # Check if T1SS has reached fraction in middle of cell and if
        # so record time. Need to integer divide Nx by 2 now that we are
        # considering whole cell.
        
        if (solver.y[13*modelsetup.Nx-modelsetup.Nx//2-1] >=
            analysis.T1SS_to_centre_frac*parameters.c_T1_init_value
            and analysis.T1SS_to_centre_frac_time == 0.0):
            
            analysis.T1SS_to_centre_frac_time = solver.t
        
        # Calculate T1SS mean
        
        analysis.T1SS_mean = np.mean(solver.y[12*modelsetup.Nx:13*modelsetup.Nx])

        # Check if T2SS has reached fraction in middle of cell and if
        # so record time. Need to integer divide Nx by 2 now that we are
        # considering whole cell.
        
        if (solver.y[26*modelsetup.Nx-modelsetup.Nx//2-1] >=
            analysis.T2SS_to_centre_frac*parameters.c_T2_init_value
            and analysis.T2SS_to_centre_frac_time == 0.0):
            
            analysis.T2SS_to_centre_frac_time = solver.t
        
        # Calculate T2SS mean
        
        analysis.T2SS_mean = np.mean(solver.y[25*modelsetup.Nx:26*modelsetup.Nx])
        
        # Output run time to screen
        
        output = 'Run time = {:.6f}s.'.format(solver.t)
        
        # Check conservation
        
        # Initialise mass
        
        mass = 0.0
        
        # Integrate mass of each bulk concentration excluding c_H
        
        for ii in np.arange(1,modelsetup.n_bulk_species):
            
            mass += simps(solver.y[ii*modelsetup.Nx:(ii+1)*modelsetup.Nx], modelsetup.x)
        
        # Add mass of boundary concentrations
        
        mass += np.sum(solver.y[-(modelsetup.n_memb_species):])
        
        # Add to screen output
        
        output += '    ' + 'Conservation: {:.8f}%'.format(100*(mass/mass_init))
    
        Printer(output)        
    
    ############################# Timing ###############################

    end = timer()
    print('\nElapsed time: {:.2f} s'.format(end - start))
    
    ############################## Output ##############################
    
    # Save final and initial concentrations in array
    
    if saving:
        
        # The formatting for the saved output is as follows:
        #
        # The first output file will contain the 34 bulk concentrations 
        # in their initial state in the following order:
        #
        # c_H_init
        # c_P1SH_init
        # c_P1SOH_init
        # c_P1SO2H_init
        # c_P1SS_init
        # c_P1SHa_init
        # c_PSH_init
        # c_PSOH_init
        # c_PSO2H_init
        # c_PSS_init
        # c_PSST1_init
        # c_T1SH_init
        # c_T1SS_init
        # c_TrxSH_init
        # c_TrxSS_init
        # c_PSH_T1SH_init
        # c_PSH_T1SS_init
        # c_PSOH_T1SH_init
        # c_PSOH_T1SS_init
        # c_PSO2H_T1SH_init
        # c_PSO2H_T1SS_init
        # c_PSS_T1SH_init
        # c_PSS_T1SS_init
        # c_PSST2_init
        # c_T2SH_init
        # c_T2SS_init
        # c_PSH_T2SH_init
        # c_PSH_T2SS_init
        # c_PSOH_T2SH_init
        # c_PSOH_T2SS_init
        # c_PSO2H_T2SH_init
        # c_PSO2H_T2SS_init
        # c_PSS_T2SH_init
        # c_PSS_T2SS_init
        # c_PbSH_init
        # c_PbSS_init
        #
        # The first output file does not contain an additional final
        # column for the membrane concentrations as these are always
        # zero at the start of the simulation.
        #
        # The second output file will contain the 36 bulk concentrations 
        # in their final state in the same order as above.

        # These therefore occupy the first 36 columns.
        # The first 48 elements of the final (37th) column store the
        # membrane concentrations.
        # 
        # The 48 membrane concentrations (24 for membrane 1 (left) and
        # 24 for membrane 2 (right)) are in the following order:
        #
        # c_m1_PSH
        # c_m1_PSOH
        # c_m1_PSO2H
        # c_m1_PSS
        # c_m1_PSST1
        # c_m1_PSH_T1SH
        # c_m1_PSH_T1SS
        # c_m1_PSOH_T1SH
        # c_m1_PSOH_T1SS
        # c_m1_PSO2H_T1SH
        # c_m1_PSO2H_T1SS
        # c_m1_PSS_T1SH
        # c_m1_PSS_T1SS
        # c_m1_PSST2
        # c_m1_PSH_T2SH
        # c_m1_PSH_T2SS
        # c_m1_PSOH_T2SH
        # c_m1_PSOH_T2SS
        # c_m1_PSO2H_T2SH
        # c_m1_PSO2H_T2SS
        # c_m1_PSS_T2SH
        # c_m1_PSS_T2SS
        # c_m1_PbSH
        # c_m1_PbSS

        # c_m2_PSH
        # c_m2_PSOH
        # c_m2_PSO2H
        # c_m2_PSS
        # c_m2_PSST1
        # c_m2_PSH_T1SH
        # c_m2_PSH_T1SS
        # c_m2_PSOH_T1SH
        # c_m2_PSOH_T1SS
        # c_m2_PSO2H_T1SH
        # c_m2_PSO2H_T1SS
        # c_m2_PSS_T1SH
        # c_m2_PSS_T1SS
        # c_m2_PSST2
        # c_m2_PSH_T2SH
        # c_m2_PSH_T2SS
        # c_m2_PSOH_T2SH
        # c_m2_PSOH_T2SS
        # c_m2_PSO2H_T2SH
        # c_m2_PSO2H_T2SS
        # c_m2_PSS_T2SH
        # c_m2_PSS_T2SS
        # c_m2_PbSH
        # c_m2_PbSS
        
        # Add IC to output directory as a subdirectory

        newdir = output_dir.split('/')[-1]

        output_dir = os.path.join(output_dir, newdir + '_' + modelsetup.ic + '/')

        if not os.path.exists(os.path.dirname(output_dir)):
            try:
                os.makedirs(os.path.dirname(output_dir))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        
        # Initialise output files
        
        concs_file_init = np.zeros((modelsetup.Nx, modelsetup.n_bulk_species))
        concs_file_final = np.zeros((modelsetup.Nx, modelsetup.n_bulk_species + 1))
        
        # Add c_init and c_final for each of the "n_bulk_species" concentrations
        # to concs_files
        
        for ii in np.arange(modelsetup.n_bulk_species):
            
            concs_file_init[:,ii] = Y_0[ii*modelsetup.Nx:(ii+1)*modelsetup.Nx]
            concs_file_final[:,ii] = solver.y[ii*modelsetup.Nx:(ii+1)*modelsetup.Nx]
        
        # Add membrane concentrations
        
        concs_file_final[:(modelsetup.n_memb_species), modelsetup.n_bulk_species] = solver.y[-(modelsetup.n_memb_species):]
        
        # Save output as csv file
        
        np.savetxt(output_dir +
                   'concs_at_T={:.4f}s.csv'.format(0.0),
                   concs_file_init, delimiter = ',')
        
        np.savetxt(output_dir +
                   'concs_at_T={:.4f}s.csv'.format(t_end),
                   concs_file_final, delimiter = ',')
        
        # Save simulation paremeters to text file within output directory
        
        with open(output_dir + 'simulation_parameters.txt', 'w') as f:

            print(parameters, end="", file=f)
                    
        # Save T1SS Mean & T2SS Mean to text file within output directory
        
        with open(output_dir + 'tss_properties.txt', 'a+') as f:

            f.write('-------------------------------\n' +
                    'T = {:.4f}s\n'.format(t_end) +
                    '-------------------------------\n' +
                    'T1SS mean         = {:.5e}\n'.format(analysis.T1SS_mean) +
                    'T1SS gen domain   = {:.5e}\n'.format(analysis.T1SS_gen_domain) +
                    'T1SS gen boundary = {:.5e}\n'.format(analysis.T1SS_gen_boundary) +
                    'T2SS mean         = {:.5e}\n'.format(analysis.T2SS_mean) +
                    'T2SS gen domain   = {:.5e}\n'.format(analysis.T2SS_gen_domain) +
                    'T2SS gen boundary = {:.5e}\n'.format(analysis.T2SS_gen_boundary))

    # Prepare interactive output
    
    # Initialise arrays
    
    c_bulk_out = np.zeros((modelsetup.Nx, modelsetup.n_bulk_species))
    c_m_out = np.zeros(modelsetup.n_memb_species)
    
    # Get bulk concentrations from solver
    
    for ii in np.arange(modelsetup.n_bulk_species):
        c_bulk_out[:,ii] = solver.y[ii*modelsetup.Nx:(ii+1)*modelsetup.Nx]
    
    # Get membrane concentrations from solver
    
    c_m_out = solver.y[-(modelsetup.n_memb_species):]
    
    # Print diagnostic values to screen
    
    print('T1SS Generation at boundary:   ',
          '{:.5e}'.format(analysis.T1SS_gen_boundary))
    
    print('T1SS Generation in domain:     ',
          '{:.5e}'.format(analysis.T1SS_gen_domain))
    
    print('Time for {:d}'.format(int(analysis.T1SS_to_centre_frac*100)) +
          '% T1SS accumulation: {:.5f}s'.format(analysis.T1SS_to_centre_frac_time))
          
    print('T1SS Mean in domain:            {:.5e}'.format(analysis.T1SS_mean))

    print('T2SS Generation at boundary:   ',
          '{:.5e}'.format(analysis.T2SS_gen_boundary))
    
    print('T2SS Generation in domain:     ',
          '{:.5e}'.format(analysis.T2SS_gen_domain))
    
    print('Time for {:d}'.format(int(analysis.T2SS_to_centre_frac*100)) +
          '% T2SS accumulation: {:.5f}s'.format(analysis.T2SS_to_centre_frac_time))
          
    print('T2SS Mean in domain:            {:.5e}'.format(analysis.T2SS_mean))
    
    return c_bulk_out, c_m_out

if __name__ == '__main__':

    c_bulk, c_m = hpscdbml()
