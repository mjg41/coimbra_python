-(c_H*k_Alt) - c_H*k_Se*c_P1SH - c_H*k_Se*c_PSH - c_H*k_Se*c_PSH_TSH - c_H*k_Se*c_PSH_TSS - c_H*k_Si*c_PSOH - c_H*k_Si*c_PSOH_TSH - c_H*k_Si*c_PSOH_TSS
-(c_H*k_Se*c_PSH) + k_aS*K_DT*c_PSH_TSH + k_aS*K_DT*c_PSH_TSS + k_R*c_PSS*c_TrxSH - k_aS*c_PSH*c_TSH - k_aS*c_PSH*c_TSS
c_H*k_Se*c_PSH + k_Sr*c_PSO2H - k_C*c_PSOH - c_H*k_Si*c_PSOH + k_aS*K_DT*c_PSOH_TSH + k_aS*K_DT*c_PSOH_TSS - k_aS*c_PSOH*c_TSH - k_aS*c_PSOH*c_TSS
-(k_Sr*c_PSO2H) + k_aS*K_DT*c_PSO2H_TSH + k_aS*K_DT*c_PSO2H_TSS + c_H*k_Si*c_PSOH - k_aS*c_PSO2H*c_TSH - k_aS*c_PSO2H*c_TSS
k_C*c_PSOH + k_aS*K_DT*c_PSS_TSH + k_aS*K_DT*c_PSS_TSS - k_R*c_PSS*c_TrxSH - k_aS*c_PSS*c_TSH - k_aS*c_PSS*c_TSS
(k_TDE*c_PSH_TSS)/K_TDE + k_HC*c_PSOH_TSH - k_TDE*c_PSST
k_aS*K_DT*c_PSH_TSH + k_aS*K_DT*c_PSO2H_TSH + k_aS*K_DT*c_PSOH_TSH + k_aS*K_DT*c_PSS_TSH - k_aS*c_PSH*c_TSH - k_aS*c_PSO2H*c_TSH - k_aS*c_PSOH*c_TSH - k_aS*c_PSS*c_TSH + k_TSSR*c_TSS
k_aS*K_DT*c_PSH_TSS + k_aS*K_DT*c_PSO2H_TSS + k_aS*K_DT*c_PSOH_TSS + k_aS*K_DT*c_PSS_TSS - k_TSSR*c_TSS - k_aS*c_PSH*c_TSS - k_aS*c_PSO2H*c_TSS - k_aS*c_PSOH*c_TSS - k_aS*c_PSS*c_TSS
-(k_R1*c_P1SS*c_TrxSH) - k_R*c_PSS*c_TrxSH + k_TR*c_TrxSS
k_R1*c_P1SS*c_TrxSH + k_R*c_PSS*c_TrxSH - k_TR*c_TrxSS
-(k_aS*K_DT*c_PSH_TSH) - c_H*k_Se*c_PSH_TSH + k_aS*c_PSH*c_TSH
-(k_aS*K_DT*c_PSH_TSS) - (k_ITDE*c_PSH_TSS)/K_ITDE - c_H*k_Se*c_PSH_TSS - (k_TDE*c_PSH_TSS)/K_TDE + k_TDE*c_PSST + k_ITDE*c_PSS_TSH + k_aS*c_PSH*c_TSS
c_H*k_Se*c_PSH_TSH + k_Sr*c_PSO2H_TSH - k_C*c_PSOH_TSH - k_aS*K_DT*c_PSOH_TSH - k_HC*c_PSOH_TSH - c_H*k_Si*c_PSOH_TSH + k_aS*c_PSOH*c_TSH
c_H*k_Se*c_PSH_TSS + k_Sr*c_PSO2H_TSS - k_C*c_PSOH_TSS - k_aS*K_DT*c_PSOH_TSS - c_H*k_Si*c_PSOH_TSS + k_aS*c_PSOH*c_TSS
-(k_aS*K_DT*c_PSO2H_TSH) - k_Sr*c_PSO2H_TSH + c_H*k_Si*c_PSOH_TSH + k_aS*c_PSO2H*c_TSH
-(k_aS*K_DT*c_PSO2H_TSS) - k_Sr*c_PSO2H_TSS + c_H*k_Si*c_PSOH_TSS + k_aS*c_PSO2H*c_TSS
(k_ITDE*c_PSH_TSS)/K_ITDE + k_C*c_PSOH_TSH - k_aS*K_DT*c_PSS_TSH - k_ITDE*c_PSS_TSH + k_aS*c_PSS*c_TSH
k_C*c_PSOH_TSS - k_aS*K_DT*c_PSS_TSS + k_aS*c_PSS*c_TSS