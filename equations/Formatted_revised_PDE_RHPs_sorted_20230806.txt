-(c_H*k_Alt) - c_H*k_Se*c_P1SH - c_H*k_Si1*c_P1SOH - c_H*k_Se*c_PSH - c_H*k_Se*c_PSH_TSH - c_H*k_Se*c_PSH_TSS - c_H*k_Si*c_PSOH - c_H*k_Si*c_PSOH_TSH - c_H*k_Si*c_PSOH_TSS
-(c_H*k_Se*c_P1SH) + k_I*c_P1SHa
c_H*k_Se*c_P1SH + k_Sr*c_P1SO2H - k_C1*c_P1SOH - c_H*k_Si1*c_P1SOH
-(k_Sr*c_P1SO2H) + c_H*k_Si1*c_P1SOH
k_C1*c_P1SOH - k_R1*c_P1SS*c_TrxSH
-(k_I*c_P1SHa) + k_R1*c_P1SS*c_TrxSH
-(c_H*k_Se*c_PSH) + k_aS*K_DTSH*c_PSH_TSH + k_aS*K_DTSS*c_PSH_TSS + k_R*c_PSS*c_TrxSH - k_aS*c_PSH*c_TSH - k_aS*c_PSH*c_TSS
c_H*k_Se*c_PSH + k_Sr*c_PSO2H - k_C*c_PSOH - c_H*k_Si*c_PSOH + k_aS*K_DTSH*c_PSOH_TSH + k_aS*K_DTSS*c_PSOH_TSS - k_aS*c_PSOH*c_TSH - k_aS*c_PSOH*c_TSS
-(k_Sr*c_PSO2H) + k_aS*K_DTSH*c_PSO2H_TSH + k_aS*K_DTSS*c_PSO2H_TSS + c_H*k_Si*c_PSOH - k_aS*c_PSO2H*c_TSH - k_aS*c_PSO2H*c_TSS
k_C*c_PSOH + k_aS*K_DTSH*c_PSS_TSH + k_aS*K_DTSS*c_PSS_TSS - k_R*c_PSS*c_TrxSH - k_aS*c_PSS*c_TSH - k_aS*c_PSS*c_TSS
(k_TDE*c_PSH_TSS)/K_TDE + k_HC*c_PSOH_TSH - k_TDE*c_PSST
k_aS*K_DTSH*c_PSH_TSH + k_aS*K_DTSH*c_PSO2H_TSH + k_aS*K_DTSH*c_PSOH_TSH + k_aS*K_DTSH*c_PSS_TSH - k_aS*c_PSH*c_TSH - k_aS*c_PSO2H*c_TSH - k_aS*c_PSOH*c_TSH - k_aS*c_PSS*c_TSH + k_TSSR*c_TSS
k_aS*K_DTSS*c_PSH_TSS + k_aS*K_DTSS*c_PSO2H_TSS + k_aS*K_DTSS*c_PSOH_TSS + k_aS*K_DTSS*c_PSS_TSS - k_TSSR*c_TSS - k_aS*c_PSH*c_TSS - k_aS*c_PSO2H*c_TSS - k_aS*c_PSOH*c_TSS - k_aS*c_PSS*c_TSS
-(k_R1*c_P1SS*c_TrxSH) - k_R*c_PSS*c_TrxSH + (c_TrxSS*V_MaxTrxR)/(K_MTrxR + c_TrxSS)
k_R1*c_P1SS*c_TrxSH + k_R*c_PSS*c_TrxSH - (c_TrxSS*V_MaxTrxR)/(K_MTrxR + c_TrxSS)
-(k_aS*K_DTSH*c_PSH_TSH) - c_H*k_Se*c_PSH_TSH + k_aS*c_PSH*c_TSH
-(k_aS*K_DTSS*c_PSH_TSS) - (k_ITDE*c_PSH_TSS)/K_ITDE - c_H*k_Se*c_PSH_TSS - (k_TDE*c_PSH_TSS)/K_TDE + k_TDE*c_PSST + k_ITDE*c_PSS_TSH + k_aS*c_PSH*c_TSS
c_H*k_Se*c_PSH_TSH + k_Sr*c_PSO2H_TSH - k_C*c_PSOH_TSH - k_aS*K_DTSH*c_PSOH_TSH - k_HC*c_PSOH_TSH - c_H*k_Si*c_PSOH_TSH + k_aS*c_PSOH*c_TSH
c_H*k_Se*c_PSH_TSS + k_Sr*c_PSO2H_TSS - k_C*c_PSOH_TSS - k_aS*K_DTSS*c_PSOH_TSS - c_H*k_Si*c_PSOH_TSS + k_aS*c_PSOH*c_TSS
-(k_aS*K_DTSH*c_PSO2H_TSH) - k_Sr*c_PSO2H_TSH + c_H*k_Si*c_PSOH_TSH + k_aS*c_PSO2H*c_TSH
-(k_aS*K_DTSS*c_PSO2H_TSS) - k_Sr*c_PSO2H_TSS + c_H*k_Si*c_PSOH_TSS + k_aS*c_PSO2H*c_TSS
(k_ITDE*c_PSH_TSS)/K_ITDE + k_C*c_PSOH_TSH - k_aS*K_DTSH*c_PSS_TSH - k_ITDE*c_PSS_TSH + k_aS*c_PSS*c_TSH
k_C*c_PSOH_TSS - k_aS*K_DTSS*c_PSS_TSS + k_aS*c_PSS*c_TSS