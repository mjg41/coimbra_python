-(c_H*k_Alt) - c_B_PSH*c_H*k_Se - c_B_PSH_TSH*c_H*k_Se - c_B_PSH_TSS*c_H*k_Se - c_B_PSOH*c_H*k_Si_P2 - c_B_PSOH_TSH*c_H*k_Si_P2 - c_B_PSOH_TSS*c_H*k_Si_P2 - c_H*k_Se*c_P1SH - c_H*k_Si_P1*c_P1SOH - c_H*k_PbOx*c_PbSH - c_H*k_Se*c_PSH - c_H*k_Se*c_PSH_TSH - c_H*k_Se*c_PSH_TSS - c_H*k_Si_P2*c_PSOH - c_H*k_Si_P2*c_PSOH_TSH - c_H*k_Si_P2*c_PSOH_TSS
-(c_H*k_Se*c_P1SH) + k_I*c_P1SHa
c_H*k_Se*c_P1SH + k_Sr*c_P1SO2H - k_C_P1*c_P1SOH - c_H*k_Si_P1*c_P1SOH
-(k_Sr*c_P1SO2H) + c_H*k_Si_P1*c_P1SOH
k_C_P1*c_P1SOH - k_R_P1*c_P1SS*c_TrxSH
-(k_I*c_P1SHa) + k_R_P1*c_P1SS*c_TrxSH
c_B_PSH*k_aBP*K_DB - c_B*k_aBP*c_PSH - c_H*k_Se*c_PSH + k_aS*K_DTSH*c_PSH_TSH + k_aS*K_DTSS*c_PSH_TSS + k_R_P2*c_PSS*c_TrxSH - k_aS*c_PSH*c_TSH - k_aS*c_PSH*c_TSS
c_B_PSOH*k_aBP*K_DB + c_H*k_Se*c_PSH + k_Sr*c_PSO2H - c_B*k_aBP*c_PSOH - k_C_P2*c_PSOH - c_H*k_Si_P2*c_PSOH + k_aS*K_DTSH*c_PSOH_TSH + k_aS*K_DTSS*c_PSOH_TSS - k_aS*c_PSOH*c_TSH - k_aS*c_PSOH*c_TSS
c_B_PSO2H*k_aBP*K_DB - c_B*k_aBP*c_PSO2H - k_Sr*c_PSO2H + k_aS*K_DTSH*c_PSO2H_TSH + k_aS*K_DTSS*c_PSO2H_TSS + c_H*k_Si_P2*c_PSOH - k_aS*c_PSO2H*c_TSH - k_aS*c_PSO2H*c_TSS
c_B_PSS*k_aBP*K_DB + k_C_P2*c_PSOH - c_B*k_aBP*c_PSS + k_aS*K_DTSH*c_PSS_TSH + k_aS*K_DTSS*c_PSS_TSS - k_R_P2*c_PSS*c_TrxSH - k_aS*c_PSS*c_TSH - k_aS*c_PSS*c_TSS
c_B_PSST*k_aB*K_DB + (k_TDE*c_PSH_TSS)/K_TDE + k_HC*c_PSOH_TSH - c_B*k_aB*c_PSST - (k_ITDE*c_PSST)/K_ITDE - k_TDE*c_PSST + k_ITDE*c_PSS_TSH
c_B_PSH_TSH*k_aBC*K_DTSH + c_B_PSO2H_TSH*k_aBC*K_DTSH + c_B_PSOH_TSH*k_aBC*K_DTSH + c_B_PSS_TSH*k_aBC*K_DTSH + k_aS*K_DTSH*c_PSH_TSH + k_aS*K_DTSH*c_PSO2H_TSH + k_aS*K_DTSH*c_PSOH_TSH + k_aS*K_DTSH*c_PSS_TSH - c_B_PSH*k_aBC*c_TSH - c_B_PSO2H*k_aBC*c_TSH - c_B_PSOH*k_aBC*c_TSH - c_B_PSS*k_aBC*c_TSH - k_aS*c_PSH*c_TSH - k_aS*c_PSO2H*c_TSH - k_aS*c_PSOH*c_TSH - k_aS*c_PSS*c_TSH + k_TSSR*c_TSS
c_B_PSH_TSS*k_aBC*K_DTSS + c_B_PSO2H_TSS*k_aBC*K_DTSS + c_B_PSOH_TSS*k_aBC*K_DTSS + c_B_PSS_TSS*k_aBC*K_DTSS + k_aS*K_DTSS*c_PSH_TSS + k_aS*K_DTSS*c_PSO2H_TSS + k_aS*K_DTSS*c_PSOH_TSS + k_aS*K_DTSS*c_PSS_TSS - c_B_PSH*k_aBC*c_TSS - c_B_PSO2H*k_aBC*c_TSS - c_B_PSOH*k_aBC*c_TSS - c_B_PSS*k_aBC*c_TSS - k_TSSR*c_TSS - k_aS*c_PSH*c_TSS - k_aS*c_PSO2H*c_TSS - k_aS*c_PSOH*c_TSS - k_aS*c_PSS*c_TSS
-(c_B_PSS*k_R_P2*c_TrxSH) - k_R_P1*c_P1SS*c_TrxSH - k_PbR*c_PbSS*c_TrxSH - k_R_P2*c_PSS*c_TrxSH + (c_TrxSS*V_MaxTrxR)/(K_MTrxR + c_TrxSS)
c_B_PSS*k_R_P2*c_TrxSH + k_R_P1*c_P1SS*c_TrxSH + k_PbR*c_PbSS*c_TrxSH + k_R_P2*c_PSS*c_TrxSH - (c_TrxSS*V_MaxTrxR)/(K_MTrxR + c_TrxSS)
c_B_PSH_TSH*k_aB*K_DB - c_B*k_aB*c_PSH_TSH - k_aS*K_DTSH*c_PSH_TSH - c_H*k_Se*c_PSH_TSH + k_aS*c_PSH*c_TSH
c_B_PSH_TSS*k_aB*K_DB - c_B*k_aB*c_PSH_TSS - k_aS*K_DTSS*c_PSH_TSS - c_H*k_Se*c_PSH_TSS - (k_TDE*c_PSH_TSS)/K_TDE + k_TDE*c_PSST + k_aS*c_PSH*c_TSS
c_B_PSOH_TSH*k_aB*K_DB + c_H*k_Se*c_PSH_TSH + k_Sr*c_PSO2H_TSH - c_B*k_aB*c_PSOH_TSH - k_C_P2*c_PSOH_TSH - k_aS*K_DTSH*c_PSOH_TSH - k_HC*c_PSOH_TSH - c_H*k_Si_P2*c_PSOH_TSH + k_aS*c_PSOH*c_TSH
c_B_PSOH_TSS*k_aB*K_DB + c_H*k_Se*c_PSH_TSS + k_Sr*c_PSO2H_TSS - c_B*k_aB*c_PSOH_TSS - k_C_P2*c_PSOH_TSS - k_aS*K_DTSS*c_PSOH_TSS - c_H*k_Si_P2*c_PSOH_TSS + k_aS*c_PSOH*c_TSS
c_B_PSO2H_TSH*k_aB*K_DB - c_B*k_aB*c_PSO2H_TSH - k_aS*K_DTSH*c_PSO2H_TSH - k_Sr*c_PSO2H_TSH + c_H*k_Si_P2*c_PSOH_TSH + k_aS*c_PSO2H*c_TSH
c_B_PSO2H_TSS*k_aB*K_DB - c_B*k_aB*c_PSO2H_TSS - k_aS*K_DTSS*c_PSO2H_TSS - k_Sr*c_PSO2H_TSS + c_H*k_Si_P2*c_PSOH_TSS + k_aS*c_PSO2H*c_TSS
c_B_PSS_TSH*k_aB*K_DB + k_C_P2*c_PSOH_TSH + (k_ITDE*c_PSST)/K_ITDE - c_B*k_aB*c_PSS_TSH - k_aS*K_DTSH*c_PSS_TSH - k_ITDE*c_PSS_TSH + k_aS*c_PSS*c_TSH
c_B_PSS_TSS*k_aB*K_DB + k_C_P2*c_PSOH_TSS - c_B*k_aB*c_PSS_TSS - k_aS*K_DTSS*c_PSS_TSS + k_aS*c_PSS*c_TSS
-(c_H*k_PbOx*c_PbSH) + k_PbR*c_PbSS*c_TrxSH
c_H*k_PbOx*c_PbSH - k_PbR*c_PbSS*c_TrxSH
c_B_PSH_TSH*k_aB*K_DB + c_B_PSH_TSS*k_aB*K_DB + c_B_PSO2H_TSH*k_aB*K_DB + c_B_PSO2H_TSS*k_aB*K_DB + c_B_PSOH_TSH*k_aB*K_DB + c_B_PSOH_TSS*k_aB*K_DB + c_B_PSST*k_aB*K_DB + c_B_PSS_TSH*k_aB*K_DB + c_B_PSS_TSS*k_aB*K_DB + c_B_PSH*k_aBP*K_DB + c_B_PSO2H*k_aBP*K_DB + c_B_PSOH*k_aBP*K_DB + c_B_PSS*k_aBP*K_DB - c_B*k_aBP*c_PSH - c_B*k_aB*c_PSH_TSH - c_B*k_aB*c_PSH_TSS - c_B*k_aBP*c_PSO2H - c_B*k_aB*c_PSO2H_TSH - c_B*k_aB*c_PSO2H_TSS - c_B*k_aBP*c_PSOH - c_B*k_aB*c_PSOH_TSH - c_B*k_aB*c_PSOH_TSS - c_B*k_aBP*c_PSS - c_B*k_aB*c_PSST - c_B*k_aB*c_PSS_TSH - c_B*k_aB*c_PSS_TSS
-(c_B_PSH*k_aBP*K_DB) + c_B_PSH_TSH*k_aBC*K_DTSH + c_B_PSH_TSS*k_aBC*K_DTSS - c_B_PSH*c_H*k_Se + c_B*k_aBP*c_PSH + c_B_PSS*k_R_P2*c_TrxSH - c_B_PSH*k_aBC*c_TSH - c_B_PSH*k_aBC*c_TSS
-(c_B_PSH_TSH*k_aB*K_DB) - c_B_PSH_TSH*k_aBC*K_DTSH - c_B_PSH_TSH*c_H*k_Se + c_B*k_aB*c_PSH_TSH + c_B_PSH*k_aBC*c_TSH
-(c_B_PSH_TSS*k_aB*K_DB) - c_B_PSH_TSS*k_aBC*K_DTSS - c_B_PSH_TSS*c_H*k_Se + c_B*k_aB*c_PSH_TSS + c_B_PSH*k_aBC*c_TSS
-(c_B_PSO2H*k_aBP*K_DB) + c_B_PSO2H_TSH*k_aBC*K_DTSH + c_B_PSO2H_TSS*k_aBC*K_DTSS + c_B_PSOH*c_H*k_Si_P2 - c_B_PSO2H*k_Sr + c_B*k_aBP*c_PSO2H - c_B_PSO2H*k_aBC*c_TSH - c_B_PSO2H*k_aBC*c_TSS
-(c_B_PSO2H_TSH*k_aB*K_DB) - c_B_PSO2H_TSH*k_aBC*K_DTSH + c_B_PSOH_TSH*c_H*k_Si_P2 - c_B_PSO2H_TSH*k_Sr + c_B*k_aB*c_PSO2H_TSH + c_B_PSO2H*k_aBC*c_TSH
-(c_B_PSO2H_TSS*k_aB*K_DB) - c_B_PSO2H_TSS*k_aBC*K_DTSS + c_B_PSOH_TSS*c_H*k_Si_P2 - c_B_PSO2H_TSS*k_Sr + c_B*k_aB*c_PSO2H_TSS + c_B_PSO2H*k_aBC*c_TSS
-(c_B_PSOH*k_C_P2) - c_B_PSOH*k_aBP*K_DB + c_B_PSOH_TSH*k_aBC*K_DTSH + c_B_PSOH_TSS*k_aBC*K_DTSS + c_B_PSH*c_H*k_Se - c_B_PSOH*c_H*k_Si_P2 + c_B_PSO2H*k_Sr + c_B*k_aBP*c_PSOH - c_B_PSOH*k_aBC*c_TSH - c_B_PSOH*k_aBC*c_TSS
-(c_B_PSOH_TSH*k_C_P2) - c_B_PSOH_TSH*k_aB*K_DB - c_B_PSOH_TSH*k_aBC*K_DTSH - c_B_PSOH_TSH*k_HC + c_B_PSH_TSH*c_H*k_Se - c_B_PSOH_TSH*c_H*k_Si_P2 + c_B_PSO2H_TSH*k_Sr + c_B*k_aB*c_PSOH_TSH + c_B_PSOH*k_aBC*c_TSH
-(c_B_PSOH_TSS*k_C_P2) - c_B_PSOH_TSS*k_aB*K_DB - c_B_PSOH_TSS*k_aBC*K_DTSS + c_B_PSH_TSS*c_H*k_Se - c_B_PSOH_TSS*c_H*k_Si_P2 + c_B_PSO2H_TSS*k_Sr + c_B*k_aB*c_PSOH_TSS + c_B_PSOH*k_aBC*c_TSS
c_B_PSOH*k_C_P2 - c_B_PSS*k_aBP*K_DB + c_B_PSS_TSH*k_aBC*K_DTSH + c_B_PSS_TSS*k_aBC*K_DTSS + c_B*k_aBP*c_PSS - c_B_PSS*k_R_P2*c_TrxSH - c_B_PSS*k_aBC*c_TSH - c_B_PSS*k_aBC*c_TSS
-(c_B_PSST*k_aB*K_DB) + c_B_PSOH_TSH*k_HC + c_B_PSS_TSH*k_ITDE - (c_B_PSST*k_ITDE)/K_ITDE + c_B*k_aB*c_PSST
c_B_PSOH_TSH*k_C_P2 - c_B_PSS_TSH*k_aB*K_DB - c_B_PSS_TSH*k_aBC*K_DTSH - c_B_PSS_TSH*k_ITDE + (c_B_PSST*k_ITDE)/K_ITDE + c_B*k_aB*c_PSS_TSH + c_B_PSS*k_aBC*c_TSH
c_B_PSOH_TSS*k_C_P2 - c_B_PSS_TSS*k_aB*K_DB - c_B_PSS_TSS*k_aBC*K_DTSS + c_B*k_aB*c_PSS_TSS + c_B_PSS*k_aBC*c_TSS