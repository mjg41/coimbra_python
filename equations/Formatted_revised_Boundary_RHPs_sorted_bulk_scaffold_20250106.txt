-(c_H[0]*k_MPbOx*c_mPbSH) - c_H[0]*k_Se*c_m_PSH - c_H[0]*k_Se*c_m_PSH_TSH - c_H[0]*k_Se*c_m_PSH_TSS - c_H[0]*k_Si_P2*c_m_PSOH - c_H[0]*k_Si_P2*c_m_PSOH_TSH - c_H[0]*k_Si_P2*c_m_PSOH_TSS + r_VS*v_sup
0
0
0
0
0
k_aMP*c_m_PSH*r_DM - k_aMP*c_PSH[0]*r_VS
k_aMP*c_m_PSOH*r_DM - k_aMP*c_PSOH[0]*r_VS
k_aMP*c_m_PSO2H*r_DM - k_aMP*c_PSO2H[0]*r_VS
k_aMP*c_m_PSS*r_DM - k_aMP*c_PSS[0]*r_VS
k_aM*c_m_PSST*r_DM - k_aM*c_PSST[0]*r_VS
k_aMC*K_DTSH*c_m_PSH_TSH + k_aMC*K_DTSH*c_m_PSO2H_TSH + k_aMC*K_DTSH*c_m_PSOH_TSH + k_aMC*K_DTSH*c_m_PSS_TSH - k_aMC*c_m_PSH*c_TSH[0] - k_aMC*c_m_PSO2H*c_TSH[0] - k_aMC*c_m_PSOH*c_TSH[0] - k_aMC*c_m_PSS*c_TSH[0]
k_aMC*K_DTSS*c_m_PSH_TSS + k_aMC*K_DTSS*c_m_PSO2H_TSS + k_aMC*K_DTSS*c_m_PSOH_TSS + k_aMC*K_DTSS*c_m_PSS_TSS - k_aMC*c_m_PSH*c_TSS[0] - k_aMC*c_m_PSO2H*c_TSS[0] - k_aMC*c_m_PSOH*c_TSS[0] - k_aMC*c_m_PSS*c_TSS[0]
-(k_MPbR*c_mPbSS*c_TrxSH[0]) - k_R_P2*c_m_PSS*c_TrxSH[0]
k_MPbR*c_mPbSS*c_TrxSH[0] + k_R_P2*c_m_PSS*c_TrxSH[0]
k_aM*c_m_PSH_TSH*r_DM - k_aM*c_PSH_TSH[0]*r_VS
k_aM*c_m_PSH_TSS*r_DM - k_aM*c_PSH_TSS[0]*r_VS
k_aM*c_m_PSOH_TSH*r_DM - k_aM*c_PSOH_TSH[0]*r_VS
k_aM*c_m_PSOH_TSS*r_DM - k_aM*c_PSOH_TSS[0]*r_VS
k_aM*c_m_PSO2H_TSH*r_DM - k_aM*c_PSO2H_TSH[0]*r_VS
k_aM*c_m_PSO2H_TSS*r_DM - k_aM*c_PSO2H_TSS[0]*r_VS
k_aM*c_m_PSS_TSH*r_DM - k_aM*c_PSS_TSH[0]*r_VS
k_aM*c_m_PSS_TSS*r_DM - k_aM*c_PSS_TSS[0]*r_VS
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
-(c_H[0]*k_Se*c_m_PSH) + k_aMC*K_DTSH*c_m_PSH_TSH + k_aMC*K_DTSS*c_m_PSH_TSS - k_aMP*c_m_PSH*r_DM + k_aMP*c_PSH[0]*r_VS + k_R_P2*c_m_PSS*c_TrxSH[0] - k_aMC*c_m_PSH*c_TSH[0] - k_aMC*c_m_PSH*c_TSS[0]
c_H[0]*k_Se*c_m_PSH + k_Sr*c_m_PSO2H - k_C_P2*c_m_PSOH - c_H[0]*k_Si_P2*c_m_PSOH + k_aMC*K_DTSH*c_m_PSOH_TSH + k_aMC*K_DTSS*c_m_PSOH_TSS - k_aMP*c_m_PSOH*r_DM + k_aMP*c_PSOH[0]*r_VS - k_aMC*c_m_PSOH*c_TSH[0] - k_aMC*c_m_PSOH*c_TSS[0]
-(k_Sr*c_m_PSO2H) + k_aMC*K_DTSH*c_m_PSO2H_TSH + k_aMC*K_DTSS*c_m_PSO2H_TSS + c_H[0]*k_Si_P2*c_m_PSOH - k_aMP*c_m_PSO2H*r_DM + k_aMP*c_PSO2H[0]*r_VS - k_aMC*c_m_PSO2H*c_TSH[0] - k_aMC*c_m_PSO2H*c_TSS[0]
k_C_P2*c_m_PSOH + k_aMC*K_DTSH*c_m_PSS_TSH + k_aMC*K_DTSS*c_m_PSS_TSS - k_aMP*c_m_PSS*r_DM + k_aMP*c_PSS[0]*r_VS - k_R_P2*c_m_PSS*c_TrxSH[0] - k_aMC*c_m_PSS*c_TSH[0] - k_aMC*c_m_PSS*c_TSS[0]
(k_TDE*c_m_PSH_TSS)/K_TDE + k_HC*c_m_PSOH_TSH - (k_ITDE*c_m_PSST)/K_ITDE - k_TDE*c_m_PSST + k_ITDE*c_m_PSS_TSH - k_aM*c_m_PSST*r_DM + k_aM*c_PSST[0]*r_VS
-(k_aMC*K_DTSH*c_m_PSH_TSH) - c_H[0]*k_Se*c_m_PSH_TSH - k_aM*c_m_PSH_TSH*r_DM + k_aM*c_PSH_TSH[0]*r_VS + k_aMC*c_m_PSH*c_TSH[0]
-(k_aMC*K_DTSS*c_m_PSH_TSS) - c_H[0]*k_Se*c_m_PSH_TSS - (k_TDE*c_m_PSH_TSS)/K_TDE + k_TDE*c_m_PSST - k_aM*c_m_PSH_TSS*r_DM + k_aM*c_PSH_TSS[0]*r_VS + k_aMC*c_m_PSH*c_TSS[0]
c_H[0]*k_Se*c_m_PSH_TSH + k_Sr*c_m_PSO2H_TSH - k_C_P2*c_m_PSOH_TSH - k_aMC*K_DTSH*c_m_PSOH_TSH - k_HC*c_m_PSOH_TSH - c_H[0]*k_Si_P2*c_m_PSOH_TSH - k_aM*c_m_PSOH_TSH*r_DM + k_aM*c_PSOH_TSH[0]*r_VS + k_aMC*c_m_PSOH*c_TSH[0]
c_H[0]*k_Se*c_m_PSH_TSS + k_Sr*c_m_PSO2H_TSS - k_C_P2*c_m_PSOH_TSS - k_aMC*K_DTSS*c_m_PSOH_TSS - c_H[0]*k_Si_P2*c_m_PSOH_TSS - k_aM*c_m_PSOH_TSS*r_DM + k_aM*c_PSOH_TSS[0]*r_VS + k_aMC*c_m_PSOH*c_TSS[0]
-(k_aMC*K_DTSH*c_m_PSO2H_TSH) - k_Sr*c_m_PSO2H_TSH + c_H[0]*k_Si_P2*c_m_PSOH_TSH - k_aM*c_m_PSO2H_TSH*r_DM + k_aM*c_PSO2H_TSH[0]*r_VS + k_aMC*c_m_PSO2H*c_TSH[0]
-(k_aMC*K_DTSS*c_m_PSO2H_TSS) - k_Sr*c_m_PSO2H_TSS + c_H[0]*k_Si_P2*c_m_PSOH_TSS - k_aM*c_m_PSO2H_TSS*r_DM + k_aM*c_PSO2H_TSS[0]*r_VS + k_aMC*c_m_PSO2H*c_TSS[0]
k_C_P2*c_m_PSOH_TSH + (k_ITDE*c_m_PSST)/K_ITDE - k_aMC*K_DTSH*c_m_PSS_TSH - k_ITDE*c_m_PSS_TSH - k_aM*c_m_PSS_TSH*r_DM + k_aM*c_PSS_TSH[0]*r_VS + k_aMC*c_m_PSS*c_TSH[0]
k_C_P2*c_m_PSOH_TSS - k_aMC*K_DTSS*c_m_PSS_TSS - k_aM*c_m_PSS_TSS*r_DM + k_aM*c_PSS_TSS[0]*r_VS + k_aMC*c_m_PSS*c_TSS[0]
-(c_H[0]*k_MPbOx*c_mPbSH) + k_MPbR*c_mPbSS*c_TrxSH[0]
c_H[0]*k_MPbOx*c_mPbSH - k_MPbR*c_mPbSS*c_TrxSH[0]