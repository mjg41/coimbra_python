# Solve numerically the system described in Eqns (3) of Travasso et al.
# (2017). The PDEs are discretised spatially and the resulting ODE
# system is solved using the built-in bdf method in scipy.

import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
from scipy.sparse import diags, csr_matrix
from scipy.sparse.linalg import spsolve
from scipy.integrate import simps, LSODA, BDF, RK45
from timeit import default_timer as timer
import sys

# Create class to do dynamic printing on one line

class Printer():
    """Print things to stdout on one line dynamically"""
    def __init__(self,data):
        sys.stdout.write("\r\x1b[K"+data.__str__())
        sys.stdout.flush()

#####################################

# Set up constants

c_PSH_init_value = 50
k_minus = 20

k_1 = 1e2
k_2 = 0.012
k_3 = 1e-4
k_4 = 1.7
k_5 = 0.21
#~ k_p = 78
k_p = 7.8e-4
c_trx = 10
boundary_frac = 0.01
#~ D_H = 1.8e3
#~ D = 5.2e-1
D_H = 1.8e-7
D = 5.2e-11

# Set up ICs

#~ dx = 0.01
#~ length = 5
#~ Nx = int(length/dx) + 1
dx=1e-6
length = 5e-5
Nx = int(length/dx) + 1

c_H_init = np.zeros(Nx)
c_PSH_init = np.zeros(Nx)
c_PSOH_init = np.zeros(Nx)
c_PSO2H_init = np.zeros(Nx)
c_PSS_init = np.zeros(Nx)

c_PSH_init[:] = c_PSH_init_value

c_m_init = np.zeros(4)

#####################################

def plot_concs(c_H_out, t_end, c_H, c_PSH, c_PSOH, c_PSO2H, c_PSS, c_m):
    
    fig, axes = plt.subplots(2,3,figsize=[15,9])
    
    # Plot title
            
    fig.suptitle('k_minus = ' + str(k_minus) + '   c_H_out = ' +
                 str(c_H_out) + '   Model at ' + 
                 str('{:.2f}'.format(t_end)) + 's.')
    
    # Domain
    
    x = np.linspace(0,length,Nx)
    
    # Plot bulk concentrations
    
    axes[0,0].plot(x,c_H_init)
    axes[0,0].plot(x,c_H)
    axes[0,0].set_title('$c_{\mathrm{H}}$')
    axes[0,1].plot(x,c_PSH_init)
    axes[0,1].plot(x,c_PSH)
    axes[0,1].set_title('$c_{\mathrm{PSH}}$')
    axes[0,2].plot(x,c_PSOH_init)
    axes[0,2].plot(x,c_PSOH)
    axes[0,2].set_title('$c_{\mathrm{PSOH}}$')
    axes[1,0].plot(x,c_PSO2H_init)
    axes[1,0].plot(x,c_PSO2H)
    axes[1,0].set_title('$c_{\mathrm{PSO2H}}$')
    axes[1,1].plot(x,c_PSS_init)
    axes[1,1].plot(x,c_PSS)
    axes[1,1].set_title('$c_{\mathrm{PSS}}$')
    
    # Display membrane concentrations using text
    
    axes[1,2].text(0.5, 0.8, '$c_{\mathrm{PSH}}^{m} = $' +
                                    str('{:.4e}'.format(c_m[0])),
                                    horizontalalignment='center')
    axes[1,2].text(0.5, 0.6, '$c_{\mathrm{PSOH}}^{m} = $' +
                                    str('{:.4e}'.format(c_m[1])),
                                    horizontalalignment='center')
    axes[1,2].text(0.5, 0.4, '$c_{\mathrm{PSO2H}}^{m} = $' +
                                    str('{:.4e}'.format(c_m[2])),
                                    horizontalalignment='center')
    axes[1,2].text(0.5, 0.2, '$c_{\mathrm{PSS}}^{m} = $' +
                                    str('{:.4e}'.format(c_m[3])),
                                    horizontalalignment='center')
    plt.show()

def hpsbml(c_H_out, run_length_max = 100000, dx = dx, length = length, 
          c_H = c_H_init, c_PSH = c_PSH_init, c_PSOH = c_PSOH_init,
          c_PSO2H = c_PSO2H_init, c_PSS = c_PSS_init, c_m = c_m_init,
          plotting = False):
    
    ######################### Initial setup ##########################
    
    # Timer
    
    start = timer()
    
    # Space discretisation
    
    Nx = int(length/dx) + 1
    x = np.linspace(0,length,Nx)

    # Calculate intermediate constants

    k_5star = k_5*c_trx
    k_plus = boundary_frac*length*k_minus/(1 - boundary_frac)
    kp_over_dx = k_p/dx
    kminus_over_dx = k_minus/dx
    kplus_over_dx = k_plus/dx
    r_H = D_H/dx**2
    r = D/dx**2    
    
    # Initialise condition

    t_0 = 0.0
    Y_0 = np.concatenate((c_H, c_PSH, c_PSOH, c_PSO2H, c_PSS, c_m))
    
    # Descriptive string of IC for saving

    IC= '(c_PSH=' + str(c_PSH_init_value) + '_c_H_out=' + str(c_H_out) + ')'
                                                        
    # Initialise figure if plotting

    if plotting:
        fig, axes = plt.subplots(2,3,figsize=[15,9])
        
        axes[0,0].plot(x,c_H_init)
        axes[0,0].set_title('$c_{\mathrm{H}}$')
            
        axes[0,1].plot(x,c_PSH_init)
        axes[0,1].set_title('$c_{\mathrm{PSH}}$')

        axes[0,2].plot(x,c_PSOH_init)
        axes[0,2].set_title('$c_{\mathrm{PSOH}}$')

        axes[1,0].plot(x,c_PSO2H_init)
        axes[1,0].set_title('$c_{\mathrm{PSO2H}}$')

        axes[1,1].plot(x,c_PSS_init)
        axes[1,1].set_title('$c_{\mathrm{PSS}}$')
        
        plt.pause(0.01)
    
    ##################### Build Diffusion Operator #####################

    # Initialise diagonals
    
    L_diag = np.zeros(5*Nx + 4)
    
    L_updiag = np.zeros(5*Nx + 3)
    
    L_lowdiag = np.zeros(5*Nx + 3)
    
    # Fill in diagonal
    
    L_diag[:Nx] = -2*r_H*np.ones(Nx)
    
    L_diag[Nx:5*Nx] = np.tile(-2*r*np.ones(Nx),4)
    
    L_diag[5*Nx:] = np.array(4*[-k_minus])
    
    # Fill in upper diagonal
    
    L_updiag[:(Nx - 1)] = r_H*np.append(2, np.ones(Nx - 2))
    
    L_updiag[Nx:(2*Nx - 1)] = r*np.append(2, np.ones(Nx - 2))
    L_updiag[2*Nx:(3*Nx - 1)] = r*np.append(2, np.ones(Nx - 2))
    L_updiag[3*Nx:(4*Nx - 1)] = r*np.append(2, np.ones(Nx - 2))
    L_updiag[4*Nx:(5*Nx - 1)] = r*np.append(2, np.ones(Nx - 2))
    
    # Fill in lower diagonal
    
    L_lowdiag[:(Nx - 1)] = r_H*np.append(np.ones(Nx - 2), 2)
    
    L_lowdiag[Nx:(2*Nx - 1)] = r*np.append(np.ones(Nx - 2), 2)
    L_lowdiag[2*Nx:(3*Nx - 1)] = r*np.append(np.ones(Nx - 2), 2)
    L_lowdiag[3*Nx:(4*Nx - 1)] = r*np.append(np.ones(Nx - 2), 2)
    L_lowdiag[4*Nx:(5*Nx - 1)] = r*np.append(np.ones(Nx - 2), 2)
    
    # Construct matrix
    
    L = diags([L_lowdiag, L_diag, L_updiag],[-1,0,1])
    
    ########################## Build ODE RHS ###########################

    def f(t, Y):
        
        # Initialise output
        
        dYdt = np.zeros(5*Nx+4)
        
        # Extract variables from input
        
        c_H = Y[:Nx]
        c_PSH = Y[Nx:2*Nx]
        c_PSOH = Y[2*Nx:3*Nx]
        c_PSO2H = Y[3*Nx:4*Nx]
        c_PSS = Y[4*Nx:5*Nx]
        
        c_m_PSH = Y[-4]
        c_m_PSOH = Y[-3]
        c_m_PSO2H = Y[-2]
        c_m_PSS = Y[-1]
        
        # Diffusion
        
        dYdt = L*Y
        
        # Reaction terms
        
        dYdt[:Nx] += -k_1*c_H*c_PSH - k_2*c_H*c_PSOH
        
        dYdt[Nx:2*Nx] += -k_1*c_H*c_PSH + k_5star*c_PSS
        
        dYdt[2*Nx:3*Nx] += (-k_4*c_PSOH + k_1*c_H*c_PSH
                                               + k_3*c_PSO2H - k_2*c_H*c_PSOH)
        
        dYdt[3*Nx:4*Nx] += -k_3*c_PSO2H + k_2*c_H*c_PSOH
        
        dYdt[4*Nx:5*Nx] += -k_5star*c_PSS + k_4*c_PSOH
        
        # Boundary conditions
        
        dYdt[0] += 2*kp_over_dx*c_H_out - 2*kp_over_dx*c_H[0]
        dYdt[Nx] += 2*kminus_over_dx*c_m_PSH - 2*kplus_over_dx*c_PSH[0]
        dYdt[2*Nx] += 2*kminus_over_dx*c_m_PSOH - 2*kplus_over_dx*c_PSOH[0]
        dYdt[3*Nx] += 2*kminus_over_dx*c_m_PSO2H - 2*kplus_over_dx*c_PSO2H[0]
        dYdt[4*Nx] += 2*kminus_over_dx*c_m_PSS - 2*kplus_over_dx*c_PSS[0]
        
        # Membrane ODEs
        
        dYdt[-4] += -k_1*c_H[0]*c_m_PSH + k_5star*c_m_PSS + k_plus*c_PSH[0]
        
        dYdt[-3] += (-(k_4 + k_2*c_H[0])*c_m_PSOH 
                        + k_1*c_H[0]*c_m_PSH + k_3*c_m_PSO2H + k_plus*c_PSOH[0])
        
        dYdt[-2] += -k_3*c_m_PSO2H + k_2*c_H[0]*c_m_PSOH + k_plus*c_PSO2H[0]
        
        dYdt[-1] += -k_5star*c_m_PSS + k_4*c_m_PSOH + k_plus*c_PSS[0]
            
        return dYdt
    
    ############################# Solve ODE ############################
    
    # Set up ODE integrator
    
    solver = LSODA(f, t_0, Y_0, run_length_max)
    
    # Integrate ODE forward
    
    while solver.t < run_length_max:
        
        # Step solver forward
        
        solver.step()      
    
        output = 'Run time = %.6f s.' % solver.t
        
        # Check conservation
    
        conc = (simps(solver.y[Nx:2*Nx],x)
                + simps(solver.y[2*Nx:3*Nx],x)
                + simps(solver.y[3*Nx:4*Nx],x)
                + simps(solver.y[4*Nx:5*Nx],x)
                + np.sum(solver.y[-4:]))/length/c_PSH_init_value
    
        output += '    ' + 'Conservation: %.8f %%' % (100*conc)
    
        Printer(output)
        
        # If plotting is enabled do plotting every timestep
        
        if plotting:
            
            # Plot title
            
            fig.suptitle('k_minus = ' + str(k_minus) + '   c_H_out = ' +
                         str(c_H_out) + '   Model at ' + 
                         str('{:.2f}'.format(solver.t)) + 's.')
            
            # Plot concentrations on 5 different axes
            
            axes[0,0].clear()
            axes[0,0].plot(x,c_H_init)
            axes[0,0].plot(x,solver.y[:Nx])
            axes[0,0].set_title('$c_{\mathrm{H}}$')
            
            axes[0,1].clear()
            axes[0,1].plot(x,c_PSH_init)           
            axes[0,1].plot(x,solver.y[Nx:2*Nx])
            axes[0,1].set_title('$c_{\mathrm{PSH}}$')
            
            axes[0,2].clear()
            axes[0,2].plot(x,c_PSOH_init)
            axes[0,2].plot(x,solver.y[2*Nx:3*Nx])
            axes[0,2].set_title('$c_{\mathrm{PSOH}}$')
            
            axes[1,0].clear()
            axes[1,0].plot(x,c_PSO2H_init)
            axes[1,0].plot(x,solver.y[3*Nx:4*Nx])
            axes[1,0].set_title('$c_{\mathrm{PSO2H}}$')
            
            axes[1,1].clear()
            axes[1,1].plot(x,c_PSS_init)
            axes[1,1].plot(x,solver.y[4*Nx:5*Nx])
            axes[1,1].set_title('$c_{\mathrm{PSS}}$')
            
            # Plot membrane concs
            
            axes[1,2].clear()
            axes[1,2].text(0.5, 0.8, '$c_{\mathrm{PSH}}^{m} = $' +
                                    str('{:.4e}'.format(solver.y[-4])),
                                          horizontalalignment='center')
            axes[1,2].text(0.5, 0.6, '$c_{\mathrm{PSOH}}^{m} = $' +
                                    str('{:.4e}'.format(solver.y[-3])),
                                          horizontalalignment='center')
            axes[1,2].text(0.5, 0.4, '$c_{\mathrm{PSO2H}}^{m} = $' +
                                    str('{:.4e}'.format(solver.y[-2])),
                                          horizontalalignment='center')
            axes[1,2].text(0.5, 0.2, '$c_{\mathrm{PSS}}^{m} = $' +
                                    str('{:.4e}'.format(solver.y[-1])),
                                          horizontalalignment='center')
            plt.pause(0.01)
        
    
    ############################# Timing ###############################

    end = timer()
    print('\nElapsed time: ',end - start, 's')
    
    ############################## Output ##############################
    
    # Save final and initial concentrations in array

    out_array = np.zeros([Nx,11])

    out_array[:,0] = c_H_init
    out_array[:,1] = solver.y[:Nx]
    out_array[:,2] = c_PSH_init
    out_array[:,3] = solver.y[Nx:2*Nx]
    out_array[:,4] = c_PSOH_init
    out_array[:,5] = solver.y[2*Nx:3*Nx]
    out_array[:,6] = c_PSO2H_init
    out_array[:,7] = solver.y[3*Nx:4*Nx]
    out_array[:,8] = c_PSS_init
    out_array[:,9] = solver.y[4*Nx:5*Nx]
    out_array[0:4,10] = solver.y[-4:]
    np.save('output/binding/concs_binding_for_IC_' + IC + '_at_t_0_and_T=' 
                                + str(run_length_max) + 's', out_array)
    
    # Interactive output
    
    c_H = solver.y[:Nx]
    c_PSH = solver.y[Nx:2*Nx]
    c_PSOH = solver.y[2*Nx:3*Nx]
    c_PSO2H = solver.y[3*Nx:4*Nx]
    c_PSS = solver.y[4*Nx:5*Nx]
    c_m = solver.y[-4:]
    
    return c_H, c_PSH, c_PSOH, c_PSO2H, c_PSS, c_m
