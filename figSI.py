# Template for doing multiple simulation runs

from hpsignal_complete_binding_method_of_lines import *

v_sup_values = [0.1, 0.1259, 0.1585, 0.1995, 0.2512, 0.3162, 0.3981, 0.5012, 0.631, 0.7943, 1., 1.2589, 1.5849, 1.9953, 2.5119, 3.1623, 3.9811, 5.0119, 6.3096, 7.9433, 10., 12.5893, 15.8489, 19.9526, 25.1189, 31.6228, 39.8107, 50.1187, 63.0957, 79.4328, 100., 102.92, 105.925, 109.018, 112.202, 115.478, 118.85, 122.321, 125.893, 129.569, 133.352, 137.246, 141.254, 145.378, 149.624, 153.993, 158.489, 163.117, 167.88, 172.783, 177.828, 183.021, 188.365, 193.865, 199.526, 251.189, 316.228, 398.107, 501.187]

# Loop over parameters

for v_sup in v_sup_values:

        parameters = Parameters(v_sup = v_sup, k_aM = 0.0, k_aMP = 0.0, k_aMC = 0.0, c_P1_init_value = 30.0, c_P_init_value = 110.0)
        modelsetup = ModelSetup(parameters)
        modelsetup.ic = '(v_sup={:.4f})'.format(parameters.v_sup)

        _,_ = hpscbml(parameters = parameters, modelsetup = modelsetup, t_end= 100000.0, saving=True, output_dir = 'output/figSI')
