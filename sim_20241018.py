# Template for doing multiple simulation runs
import sys
from hpsignal_complete_binding_method_of_lines import Parameters, ModelSetup, hpscbml

# Moved the below variables into sbatch array to parallelise
# v_sup_values=[0.1,0.1585,0.2512,0.3981,0.631,1.,1.5849,2.5119,3.9811,
#               6.3096,10.,15.8489,25.1189,39.8107,63.0957,100.]
# K_DTSS_values = [0.01, 0.018, 0.032, 0.056, 0.1, 0.178, 0.316,
#                  0.562, 1.0, 1.778, 3.162, 5.623, 10.0]
t_end_values = [0.5,1.,5.,10.,15.,20.,25.,30.,35.,40.,45.,50.,55.,60.,70.,80.,
                90.,100.,110.,120.,150.,180.,240.,300.,3600.,21600.,43200.,86400.]

parameters = Parameters(v_sup = float(sys.argv[1]), k_aMP = 0.0, k_aMC = 0.0, k_aS = 1.0, k_HC=0.5,
                        K_DTSH = 0.1, K_DTSS = float(sys.argv[2]), c_Pb_init_value = 0.001)
modelsetup = ModelSetup(parameters)
modelsetup.ic = f'(v_sup={parameters.v_sup:.4f}_K_DTSS={parameters.K_DTSS:.4f})'

for t_end in t_end_values:

    _,_ = hpscbml(parameters = parameters, modelsetup = modelsetup, t_end= t_end, saving=True, output_dir = 'output/20241018_K_DTSS_scan')
