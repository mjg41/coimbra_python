# Template for doing multiple simulation runs

from hpsignal_complete_binding_method_of_lines import *

v_sup_values=[0.1,0.1585,0.2512,0.3981,0.631,1.,1.5849,2.5119,3.9811,
              6.3096,10.,15.8489,25.1189,39.8107,63.0957,100.]
k_aS_values = [0.1, 1.0, 10.0]
K_DTSH_values = [0.01, 0.018, 0.032, 0.056, 0.1, 0.178, 0.316,
                 0.562, 1.0, 1.778, 3.162, 5.623, 10.0]

# Loop over parameters

for v_sup in v_sup_values:
    for k_aS in k_aS_values:
        for K_DTSH in K_DTSH_values:

            parameters = Parameters(v_sup = v_sup, k_aMP = 0.0, k_aMC = 0.0, k_aS = k_aS,
                                    K_DTSH = K_DTSH, K_DTSS = 10000.0, c_Pb_init_value = 0.001)
            modelsetup = ModelSetup(parameters)
            modelsetup.ic = '(v_sup={:.4f}_k_aS={:.2f}_K_DTSH={:.4f})'.format(parameters.v_sup, parameters.k_aS, parameters.K_DTSH)

            _,_ = hpscbml(parameters = parameters, modelsetup = modelsetup, t_end= 100000.0, saving=True, output_dir = 'output/20240802_K_DTSH_no_scaffold_scan')
