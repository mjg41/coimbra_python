# Template for doing multiple simulation runs

from hpsignal_complete_binding_method_of_lines import *

k_aS_values = [0.001,0.0017,0.0028,0.0046,0.0077,0.0129,0.0215,0.0359,0.0599,0.1]
k_TSSR_values = [0.015,0.0225,0.03,0.0375,0.045]
t_end_values = [0.5,1.,5.,10.,20.,30.,45.,60.,90.,120.,150.,180.,240.,300.]

# Loop over parameters

for k_aS in k_aS_values:
    for k_TSSR in k_TSSR_values:
        for t_end in t_end_values:

            parameters = Parameters(v_sup = 1.0, k_aM = 0.0, k_aMP = 0.0, k_aMC = 0.0, k_aS = k_aS, k_TSSR = k_TSSR, c_Pb_init_value = 0.001)
            modelsetup = ModelSetup(parameters)
            modelsetup.ic = '(k_aS={:.4f}_k_TSSR={:.4f})'.format(parameters.k_aS, parameters.k_TSSR)

            _,_ = hpscbml(parameters = parameters, modelsetup = modelsetup, t_end= t_end, saving=True, output_dir = 'output/20240614_k_aS_k_TSSR_parameter_scan')
