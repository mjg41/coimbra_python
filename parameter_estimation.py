import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import glob
import os

# Load in all concs. Folder structure:
# 
# concs_binding_for_IC_(v_sup=***)_at_t_0_and_T=**s
#
# for values of T=[0.5, 1, 5, 10, 30, 60, 720, 7200]
#
# and with files within of the format:
#
# concs_binding_for_IC_(*******)_at_t_0_and_T=**s.csv
#
# for varying IC_(*******) strings.

root = '/home/hpsignalling/hpsignalling-coimbra/output/complete_binding_prx1'

t_end_vals = ['0.5','1.0','5.0','10.0','30.0','60.0','720.0','7200.0']

n_t_end_vals = len(t_end_vals)

conc_list = [[] for i in range(n_t_end_vals)]

for path, subdirs, files in os.walk(root):
    
    for ii, t_end_val in enumerate(t_end_vals):
        
        if ('T=' + t_end_val + 's') in path:
            
            for filename in files:
                
                conc_list[ii].append(os.path.join(path,filename))


concs = [np.zeros((len(conc_list[0]),51,43)) for i in range(n_t_end_vals)]

for ii, conc_list_t_end in enumerate(conc_list):
    
    for jj, cfile in enumerate(conc_list_t_end):
        
        concs[ii][jj,:,:] = np.loadtxt(cfile,delimiter=',')

def plot_concs(c_bulk, label = None, title = None):
    '''Plot concentrations outputted by pde routine.
    
    Positional arguments:
    c_bulk are the bulk concentrations from the pde routine
    
    Optional keyword arguments:
    label is the line label for each plot
    '''
    
    # List of subplot titles
    
    title_list = ['$c_{\mathrm{H}}$',
                  '$c_{\mathrm{P1SH}}$',
                  '$c_{\mathrm{P1SOH}}$',
                  '$c_{\mathrm{P1SS}}$',
                  '$c_{\mathrm{PSH}}$',
                  '$c_{\mathrm{PSOH}}$',
                  '$c_{\mathrm{PSO2H}}$',
                  '$c_{\mathrm{PSS}}$',
                  '$c_{\mathrm{PSST}}$',
                  '$c_{\mathrm{TSH}}$',
                  '$c_{\mathrm{TSS}}$',
                  '$c_{\mathrm{TrxSH}}$',
                  '$c_{\mathrm{TrxSS}}$',
                  '$c_{\mathrm{PSH\$TSH}}$',
                  '$c_{\mathrm{PSH\$TSS}}$',
                  '$c_{\mathrm{PSOH\$TSH}}$',
                  '$c_{\mathrm{PSOH\$TSS}}$',
                  '$c_{\mathrm{PSO2H\$TSH}}$',
                  '$c_{\mathrm{PSO2H\$TSS}}$',
                  '$c_{\mathrm{PSS\$TSH}}$',
                  '$c_{\mathrm{PSS\$TSS}}$'
                  ]
    
    # Plot bulk concentrations
    
    for conc, ax, title in zip(c_bulk.T,
                               axes.ravel(),
                               title_list):
        ax.plot(x,conc, label = label, linewidth=0.4)
        ax.set_title(title)
    
    handles, labels = ax.get_legend_handles_labels()
    
    # Remove axis ticks and labels from last three axes
    
    for ax in axes.ravel()[-3:]:
        ax.axis('off')
    
    return handles,labels
    

### Make plots ###

x = np.linspace(0,5,51)

titles = ['v_sup = 65exp(-5e-3t)    Model at 0.5s.',
          'v_sup = 65exp(-5e-3t)    Model at 1.0s.',
          'v_sup = 65exp(-5e-3t)    Model at 5.0s.',
          'v_sup = 65exp(-5e-3t)    Model at 10.0s.',
          'v_sup = 65exp(-5e-3t)    Model at 30.0s.',
          'v_sup = 65exp(-5e-3t)    Model at 60.0s.',
          'v_sup = 65exp(-5e-3t)    Model at 720.0s.',
          'v_sup = 65exp(-5e-3t)    Model at 7200.0s.']

for title, conc, conc_list_t_end, t_end_val in zip(titles, concs, conc_list, t_end_vals):
    
    # Initialise figure and axes
    
    fig, axes = plt.subplots(6,4,figsize=[10,11],dpi=450)
    
    # Nicely space figures leaving room for title
    
    fig.tight_layout(pad=3.75, h_pad=None, w_pad=None)
    
    NUM_COLORS = 12
    cm = plt.get_cmap('Paired')
    newcolors = cm(np.linspace(0,1,NUM_COLORS))
    for ax in axes.ravel():
        ax.set_prop_cycle(color = newcolors)
    
    # Plot title
            
    fig.suptitle(title)
    
    for conc_ic, conc_list_t_end_ic in zip(conc[:,:,1::2], conc_list_t_end):
        
        IC_left_index = conc_list_t_end_ic.replace('(', 'X', 1).find('(') + len('(v_sup=65exp-5e-3t_')
        IC_right_index = conc_list_t_end_ic.replace(')','X',1).find(')') + 1
        label = conc_list_t_end_ic[IC_left_index:IC_right_index]
        
        if ('k_aM=0.1' in label) or ('k_aM=10.0' in label):
            
            if ('k_aMC=0.01' in label) or ('k_aMC=1.0' in label):
                
                if ('k_TDE=0.01' in label) or ('k_TDE=1)' in label) or ('k_TDE=100)' in label):
        
                    handles, labels = plot_concs(conc_ic[:,:], label = label[:-1])

    fig.legend(handles, labels, loc='lower center',prop={'size': 8})
    fig.savefig('figures/' + 'v_sup=65exp-5e-3t_T=' + t_end_val + 's.png')
