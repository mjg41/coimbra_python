#!/bin/bash
#SBATCH --job-name=sim-20241018
#SBATCH --time=15:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --array=0-51  # Total number of combinations minus 1

module load python3

# Define lists
# v_sup_values=(0.1 0.1585 0.2512 0.3981 0.631 1.0 1.5849 2.5119 3.9811 6.3096 10.0 15.8489 \
            #   25.1189 39.8107 63.0957 100.0) # 16 values total
v_sup_values=(25.1189 39.8107 63.0957 100.0) # do 4 values at a time
K_DTSS_values=(0.01 0.018 0.032 0.056 0.1 0.178 0.316 0.562 1.0 1.778 3.162 5.623 10.0) # 13 values total
# Then we have 52 values as per array (up to system quota)

# Calculate the total number of combinations
len1=${#v_sup_values[@]}
len2=${#K_DTSS_values[@]}
total_combinations=$((len1 * len2))

# Get the index of the current job from SLURM
index=$SLURM_ARRAY_TASK_ID

# Calculate the indices for each list
i=$((index / len2 % len1))
j=$((index % len2))

# Access the values from each list
value1=${v_sup_values[$i]}
value2=${K_DTSS_values[$j]}

echo "Running with values: $value1, $value2"

# Run your Python script with the values
python3 sim_20241018.py $value1 $value2