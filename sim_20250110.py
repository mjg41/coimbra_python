# Template for doing multiple simulation runs

from hpsignal_complete_binding_method_of_lines import Parameters, ModelSetup, hpscbml

v_sup_values = [0.1, 0.1585, 0.2512, 0.3981, 0.631, 1., 1.5849, 2.5119, 3.9811, 6.3096, 10., 15.8489, 25.1189, 39.8107, 63.0957, 100.]
k_aBC_values = [0.01, 0.018, 0.032, 0.056, 0.1, 0.178, 0.316, 0.562, 1., 1.778, 3.162, 5.623, 10.]
K_DTSH_values = [0.01, 0.018, 0.032, 0.056, 0.1, 0.178, 0.316, 0.562, 1., 1.778, 3.162, 5.623, 10.]
t_end_values = [0.5, 1., 5., 10., 20., 30., 45., 60., 90., 120., 150., 180., 240., 300., 3600., 21600., 43200., 86400.]

# Loop over parameters

for v_sup in v_sup_values:

    for k_aBC in k_aBC_values:

        for K_DTSH in K_DTSH_values:

            for t_end in t_end_values:

                parameters = Parameters(v_sup = v_sup, k_aMP = 0.0, k_aMC = 0.0, k_aS = 0.0, k_aBC = k_aBC,
                                        K_DTSH = K_DTSH, K_DTSS = 10000.0, c_Pb_init_value = 0.001)
                modelsetup = ModelSetup(parameters)
                modelsetup.ic = f'(v_sup={parameters.v_sup:.4f})'

                _,_ = hpscbml(parameters = parameters, modelsetup = modelsetup, t_end= t_end, saving=True, output_dir = 'output/20250106_bulk_scaffold_test')
