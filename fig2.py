# Template for doing multiple simulation runs

from hpsignal_complete_binding_method_of_lines import *

t_end_values = [0.001 , 0.0016, 0.0025, 0.004 , 0.0063, 0.01, 0.0158, 0.0251, 0.0398, 0.0631,
                0.1, 0.1585, 0.2512, 0.3981, 0.631, 1., 1.5849, 2.5119, 3.9811, 6.3096,
                10., 15.8489, 25.1189, 39.8107, 63.0957, 100., 158.4893, 251.1886, 398.1072, 630.9573,
                1000., 1584.8932, 2511.8864, 3981.0717, 6309.5734, 10000., 15848.9319, 25118.8643, 39810.7171, 63095.7344, 100000.]

# Loop over parameters

for t_end in t_end_values:

    parameters = Parameters(v_sup = 10.0, k_aM = 0.0, k_aMP = 0.0, k_aMC = 0.0)
    modelsetup = ModelSetup(parameters)
    modelsetup.ic = '(v_sup={:.4f})'.format(parameters.v_sup)

    _,_ = hpscbml(parameters = parameters, modelsetup = modelsetup, t_end= t_end, saving=True, output_dir = 'output/fig2_complex_Prdx1SS_reduction')
