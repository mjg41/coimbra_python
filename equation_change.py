# Code to take equations output by matematica into a txt file, and
# replace the equations appropriately in the pde solving code.
#
# The PDEs text file should contain the 39 reaction-diffusion equations
# for the bulk concentrations in the following order:
# 
# c_H
# c_P1SH
# c_P1SOH
# c_P1SO2H
# c_P1SS
# c_P1SHa
# c_PSH
# c_PSOH
# c_PSO2H
# c_PSS
# c_PSST
# c_TSH
# c_TSS
# c_TrxSH
# c_TrxSS
# c_PSH_TSH
# c_PSH_TSS
# c_PSOH_TSH
# c_PSOH_TSS
# c_PSO2H_TSH
# c_PSO2H_TSS
# c_PSS_TSH
# c_PSS_TSS
# c_PbSH
# c_PbSS
# c_B
# c_B_PSH
# c_B_PSH_TSH
# c_B_PSH_TSS
# c_B_PSO2H
# c_B_PSO2H_TSH
# c_B_PSO2H_TSS
# c_B_PSOH
# c_B_PSOH_TSH
# c_B_PSOH_TSS
# c_B_PSS
# c_B_PSST
# c_B_PSS_TSH
# c_B_PSS_TSS
#
# The boundary text file should contain the 18 boundary terms related to
# the above bulk concentrations followed by the 15 membrane
# concentrations in the following order:
#
# c_m_PSH
# c_m_PSOH
# c_m_PSO2H
# c_m_PSS
# c_m_PSST
# c_m_PSH_TSH
# c_m_PSH_TSS
# c_m_PSOH_TSH
# c_m_PSOH_TSS
# c_m_PSO2H_TSH
# c_m_PSO2H_TSS
# c_m_PSS_TSH
# c_m_PSS_TSS
# c_m_PbSH
# c_m_PbSS
#
# Thus the PDE text file should have 18 lines and the boundary file
# should have 39 + 15 = 54 lines. This condition is checked in the code.

import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--pdes", help="Path to file containing PDE RHSs")
parser.add_argument("-b", "--boundary", help="Path to file containing Boundary RHSs")
args = parser.parse_args()

# Read in PDEs

with open(os.path.join('equations', args.pdes), 'r') as f:
    pdes = f.readlines()

# Read in Boundary conditions

with open(os.path.join('equations', args.boundary), 'r') as f:
    boundary = f.readlines()

# Read in code to be updated

with open('hpsignal_complete_binding_method_of_lines.py', 'r') as f:
    code = f.readlines()

# Simple error checks

n_pdes = 39
n_odes = 15

if len(pdes)!= n_pdes:
    raise Exception(f"PDE file doesnt have {n_pdes} lines - please double check!")
    
if len(boundary)!= n_pdes + n_odes:
    raise Exception(f"Boundary file doesnt have {n_pdes + n_odes} lines - please double check!")

# Define function to generate starting part of line for pdes
def pde_line_start(ii):
    """
    ii: Equation number
    """
    # Error checks
    if ii <= 0:
        raise Exception("Number of equation must be greater than 0.")
    
    if not isinstance(ii, int):
        raise Exception("Number of equation must be an integer.")
    
    if ii == 1:

        return "        dYdt[:modelsetup.Nx] += "
    
    if ii == 2:

        return "        dYdt[modelsetup.Nx:2*modelsetup.Nx] += "
    
    else:

        return f"        dYdt[{ii-1}*modelsetup.Nx:{ii}*modelsetup.Nx] += "

# Define function to generate starting part of line for pdes
def pde_line_start(ii):
    """
    ii: Equation number
    """
    # Error checks
    if ii <= 0:
        raise Exception("Number of equation must be greater than 0.")
    
    if not isinstance(ii, int):
        raise Exception("Number of equation must be an integer.")
    
    if ii == 1:

        return "        dYdt[:modelsetup.Nx] += "
    
    if ii == 2:

        return "        dYdt[modelsetup.Nx:2*modelsetup.Nx] += "
    
    else:

        return f"        dYdt[{ii-1}*modelsetup.Nx:{ii}*modelsetup.Nx] += "

# Define function to generate starting part of line for bcs
def bc_line_start(ii):
    """
    ii: Equation number
    """
    # Error checks
    if ii <= 0:
        raise Exception("Number of equation must be greater than 0.")
    
    if not isinstance(ii, int):
        raise Exception("Number of equation must be an integer.")
    
    if ii == 1:

        return "        dYdt[0] += "
    
    if ii == 2:

        return "        dYdt[modelsetup.Nx] += "
    
    else:

        return f"        dYdt[{ii-1}*modelsetup.Nx] += "

# Define function to generate starting part of line for odes
def ode_line_start(ii):
    """
    ii: Equation number
    """
    # Error checks
    if ii <= 0:
        raise Exception("Number of equation must be greater than 0.")
    
    if not isinstance(ii, int):
        raise Exception("Number of equation must be an integer.")
    
    if ii == 1:

        return "        dYdt[modelsetup.n_bulk_species*modelsetup.Nx] += "

    else:

        return f"        dYdt[modelsetup.n_bulk_species*modelsetup.Nx + {ii-1}] += "

# Initialise new code & pde, bc and ode counter

newcode = []
pdes_done = 1
bcs_done = 1
odes_done = 1

# Update equations

for line in code:
    
    # Update PDEs

    pde_to_check = pde_line_start(pdes_done)
    bc_to_check = bc_line_start(bcs_done)
    ode_to_check = ode_line_start(odes_done)


    if pde_to_check in line:
        newcode.append(pde_to_check + pdes[pdes_done - 1].strip("\n") + "\n")
        pdes_done += 1

    elif bc_to_check in line:
        newcode.append(bc_to_check + "2*( " + boundary[bcs_done - 1].strip("\n") + " )/parameters.dx\n")
        bcs_done += 1

    elif pde_to_check in line:
        newcode.append(pde_to_check + boundary[n_pdes + odes_done - 1].strip("\n") + "\n")
        odes_done += 1
    
    else:
        newcode.append(line)

# Write out new code

with open('hpsignal_complete_binding_method_of_lines_new.py', 'w') as f:
    for line in newcode:
        f.write(line)
