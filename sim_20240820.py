# Template for doing multiple simulation runs

from hpsignal_complete_binding_method_of_lines import Parameters, ModelSetup, hpscbml

v_sup_values=[0.1,1.,10.,100.]

# Loop over parameters

for v_sup in v_sup_values:

    parameters = Parameters(v_sup = v_sup)
    modelsetup = ModelSetup(parameters)
    modelsetup.ic = f'(v_sup={parameters.v_sup:.4f})'

    _,_ = hpscbml(parameters = parameters, modelsetup = modelsetup, saving=True, output_dir = 'output/20240820_test')
